import { Button, ButtonGroup } from "@blueprintjs/core";
import React, { useState } from "react"
import { Icon } from "react-pe-scalars";
import { __ } from "react-pe-utilities" ;
import {MediaChooser} from "react-pe-useful" 
import { CARD_SEPARATOR } from "../../views/LandingState/Card";
import { getCurrencies } from "../../views/LandingState/card/CardFieldPrice";

const CardSingleAllFields = props => {
    const [value, setValue] = useState(
        props.value && Array.isArray(props.value)
            ?
            props.value
            :
            []
    )

    const on = evt => {
        const __field = evt.currentTarget.value
        console.log(__field)
    }
    const btn = (field, i) => {
        const fieldData = props.origin.card_fields[i]
        const __field = field.field
        if (!fieldData) return ""
        //console.log(fieldData)
        //console.log(__field)

        switch (fieldData.type) {
            case "check":
                return check(__field, on, fieldData)
            case "icon":
                return icon(__field, on, fieldData)
            case "cf":
            case "navlink":
            case "outerlink":
                return outerlink(__field, on, fieldData)
            case "media":
                return media(__field, on, fieldData)
            case "personal_links":
                return personal_links(__field, on, fieldData)
            case "price":
                return price(__field, on, fieldData)
            case "section":
                return section(__field, on, fieldData)
            case "empty":
                return ""
            case "divider":
                return ""
            case "string":
            default:
                return string(__field, on, fieldData)
        }
    }
    const fields = props.origin.card_fields.map((field, i) => {
        return ["empty", "divider"].filter(e => e === field.type).length > 0
            ?
            null
            :
            <div className="row mt-3" key={i}>
                <div className="col-12 title">
                    {field.metafor}:
                </div>
                <div className="col-12">
                    {btn(value[i], i)}
                </div>
            </div>
    })
    
    return <div className="w-100">
        
        {fields}
    </div>
}
export default CardSingleAllFields

const string = (__field, on, fieldData) => {
    return <div className="w-100 card p-3">
        <input
            type="text"
            className="dark input form-control"
            value={__field}
            onChange={on}
        />
    </div>
}

const check = (__field, on, fieldData) => {
    let datas = []
    try {
        datas = JSON.parse(__field.replaceAll("!~!~", '"'))
        datas = Array.isArray(datas) ? datas : []
    }
    catch (e) {
        datas = []
    }
    return <div className="w-100 card p-3">
        {
            datas.map((dt, index) => {
                return <div className="w-100 d-flex" key={index}>
                    <label className="_check_">
                        <input
                            type="checkbox"
                            className=" "
                            checked={dt.icon || dt.icon == 1 ? true : false}
                            onChange={evt => on(evt, "icon", index)}
                        />
                    </label>
                    <input
                        type="text"
                        className="dark input form-control"
                        value={
                            dt.label
                                ?
                                dt.label
                                :
                                ""
                        }
                        onChange={evt => on(evt, "label", index)}
                    />
                    <Button minimal icon="minus" onClick={evt => on(evt, "remove_check", index)} minimal />
                </div>
            })
        }
        <Button minimal icon="plus" className="mt-2" onClick={evt => on(evt, "add_check")} >{__("add  feature")}</Button>
    </div>
}

const icon = (__field, on, fieldData) => {
    console.log(fieldData)
    const datas = __field
        ?
        __field.split(CARD_SEPARATOR())
        :
        ["", 40]
    return (
        <div className="w-100 card p-3">
            <Icon
                editable
                value={datas[0]}
                on={(data) => on(data, 0)}
            />
        </div>
    )
}
const outerlink = (__field, on, fieldData) => {
    // const { card_fields, i } = this.props.origin.object
    const datas = __field
        ?
        __field.split(CARD_SEPARATOR())
        :
        ["", ""]
    // console.log( datas );
    return <div className="w-100 card p-3">
        <div className="row dat ">
            <div className="col-4 layout-label">
                {__("Button's outer URL")}
            </div>
            <div className="col-8 layout-data">
                <input
                    type="text"
                    className="dark input form-control"
                    value={datas[0]}
                    onChange={(evt) => on(evt, 0)}
                />
            </div>
            <div className="col-4 layout-label">
                {__("Button label")}
            </div>
            <div className="col-8 layout-data">
                <input
                    type="text"
                    className="dark input form-control"
                    value={datas[1]}
                    onChange={(evt) => on(evt, 1)}
                />
            </div>
        </div>
    </div>
}
const media = (__field, on, fieldData) => {
    return <div className="my-2">
        <MediaChooser
            url={__field}
            id="media"
            padding={5}
            height={140}
            onChange={on}
        />
    </div>

}
const personal_links = (__field, on, fieldData) => {
    // console.log( __field);
    const datas = __field
        ? (__field).split(CARD_SEPARATOR())
        : []
    const btns = datas.map((ee, ii) => (
        <div className="d-flex" key={ii}>
            <input
                type="text"
                className="dark input form-control"
                value={ee}
                onChange={(evt) => on(evt, ii)}
                placeholder={__("put the link")}
            />
            <Button
                icon="minus"
                minimal
                className="my-2"
                onClick={() => onRemoveLink(ii)}
            />
            <Button
                icon="chevron-down"
                minimal
                className="my-2"
                disabled={ii == datas.length - 1}
                onClick={() => on("down", ii)}
            />
            <Button
                icon="chevron-up"
                minimal
                className="my-2"
                disabled={ii == 0}
                onClick={() => on("up", ii)}
            />
        </div>
    ))
    const onRemoveLink = i => {
        
    }
    return (
        <div className="container card p-3">
            <div className="row p-2 my-2 bg-light border ">
                <div className="col-12 flex-column">
                    {btns}
                </div>
                <div className="offset-2 col-10">
                    <Button
                        icon="plus"
                        className="my-2"
                        onClick={() => on("addLink")}
                    />
                </div>
            </div>
        </div>
    )
}
const price = (__field, on, fieldData) => {

    const datas = __field
        ? (__field).split(CARD_SEPARATOR())
        : ["", "", "", ""]
    console.log(fieldData.variant_param_1);
    console.log(fieldData.variant_param_2);

    const currencySelector = selected => {
        return getCurrencies().map((e, i) => {
            return <option value={e.id} selected={selected} key={i}>
                {__(e.title)}
            </option>
        })
    }

    let currency = getCurrencies().filter(e => {
        // console.log(e, e.id == datas[1])
        return e.id == datas[1]
    })[0]

    currency = currency ? currency : getCurrencies()[0]
    return (
        <div className="container card p-3">
            <div className="row dat ">
                <div className="col-12 layout-label">
                    {__("Nominal")}
                </div>
                <div className="col-12 layout-data">
                    <input
                        type="text"
                        className="dark input form-control"
                        value={datas[0]}
                        onChange={(evt) => on(evt, 0)}
                    />
                </div>
                <div className="col-12 layout-label">
                    {__("Mini nominal")}
                </div>
                <div className="col-12 layout-data">
                    <input
                        type="text"
                        className="dark input form-control"
                        value={datas[3]}
                        onChange={(evt) => on(evt, 3)}
                    />
                </div>
                <div className="col-12 lead title opacity_75">
                    {__(fieldData.variant_param_1)}
                </div>
                {/* <div className="col-12 layout-label">
                {__("Carrency label")}
            </div>
            <div className="col-12 layout-data flex-column">

                <select
                    className="form-control input dark"
                    value={datas[1]}
                    onChange={evt => on(evt, 1)}
                >
                    {currencySelector(datas[1])}
                </select>
                <ButtonGroup fill large>
                    <Button onClick={evt => this.on(evt, "id", 2)} minimal={datas[2] !== "id"} large>
                        {currency.id}
                    </Button>
                    <Button onClick={evt => this.on(evt, "title", 2)} minimal={datas[2] !== "title"} large>
                        {__(currency.title)}
                    </Button>
                    <Button onClick={evt => this.on(evt, "abbreviation", 2)} minimal={datas[2] !== "abbreviation"} large>
                        {currency.abbreviation}
                    </Button>
                    <Button onClick={evt => this.on("icon", 2)} minimal={datas[2] !== "icon"} large>
                        <span className={currency.icon} />
                    </Button>
                </ButtonGroup>
            </div> */}
            </div>
        </div>
    )
}
const section = (__field, on, fieldData) => {
    return (
        <input
            type="text"
            className="dark input form-control"
            value={__field}
            onChange={on}
        />
    )

}