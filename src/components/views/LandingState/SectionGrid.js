import React from "react"

const SectionGrid = props => {
    const composition = props.composition
        ?
        Array(props.composition.columns).fill(1)
            .map((e, i) => {
                return i == 0 ? null :
                    <div
                        key={i}
                        className="landing-section-grid-left"
                        style={{
                            position: "absolute",
                            left: `${(100 / props.composition.columns * i)}%`
                        }}
                    />
            })
        :
        null;
    return <div className="position-absolute w-100 h-100 untouchble">
        <div className="landing-section-grid">
            <div className="container mx-auto">
                <div className="landing-section-grid-vertical w-100 h-100">
                    {props.composition.is_blocked ? composition : null}
                </div>
                {!props.composition.is_blocked ? composition : null}
            </div>
        </div>
    </div>
}
export default SectionGrid