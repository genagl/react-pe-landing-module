import React, { Component } from "react"
import { __ } from "react-pe-utilities" 
import {LayoutIcon} from 'react-pe-useful'
import { components } from "./data/components"
import { Button, Dialog, Intent } from "@blueprintjs/core"

class Image extends Component {
	state = {
	  ...this.props,
	}

	componentDidUpdate(nextProps) {
	  let isUpdate = false
	  const state = { };
	  ["is_edit", "data", "class_name", "type", "title"]
	    .forEach((e, i) => {
	      if (nextProps[e] !== this.state[e]) {
	        isUpdate = true
	        state[e] = nextProps[e]
	      }
	    })
	  if (isUpdate) {
	    // console.log(state);
	    this.setState(state)
	  }
	}
	onClick = evt =>
	{
		const {is_full_open} = this.state.data;
		console.log(is_full_open)
		if(is_full_open)
		{
			this.setState({is_full_open: !this.state.is_full_open})
		}
	}
	render() {
	  const { class_name, title } = this.props
	  const {
	    src, 
		src_type, 
		style, 
		height, 
		original_size, 
		background_position_x, 
		width_fixed,
		description,
		description_title,
		description_style,
		description_class_name, 
		image_vertical, 
		is_contrast_muar, 
		exact_value 
	  } = this.props.data
	  const {is_full_open} = this.state
	  const st = { backgroundImage: `url(${src})`, height }
	  if (original_size) {
	    st.backgroundSize = "auto"
	    st.backgroundRepeat = "no-repeat"
	  }
	  if (background_position_x) {
	    st.backgroundPositionX = background_position_x
	  }
	  	const muar = is_contrast_muar ? " muar" : ""
		let thumb_image_vert = { backgroundPosition:"center"}
		switch(image_vertical)
		{
			case "top":
			case "bottom":
			case "center":
				thumb_image_vert = { backgroundPositionY:image_vertical}
			break
			case "exact_value":
			default:
				thumb_image_vert = { backgroundPositionY:`${exact_value}%`}
			break
		}


	  // console.log( style );
	  const descr = description_title || description
	  	?
		<div className={"landing-image-descr-container " + description_class_name} style={description_style}>
			<div 
				className="landing-image-descr-title"
				dangerouslySetInnerHTML={{__html:__(description_title)}}
			/>
			<div 
				className="landing-image-descr"
				dangerouslySetInnerHTML={{__html:__(description)}}
			/>
		</div>
		: 
		null
	  return <>
		{
			src
				? 
				src_type === "div"
					? 
					<div
						className={`landing-image ${class_name} ${muar}` }
						style={{ ...style, ...st, cursor: is_full_open ? "pointer": null, ...thumb_image_vert }}
						onClick={this.onClick}
					> 
						{descr}
					</div>
					: 
					<div className={`landing-image ${class_name}`}>
						<img
						src={src}
						alt={title ? title.text : ""}
						style={{
							height: height || (width_fixed
							? "auto"
							: "100%"),
							margin: "0 auto",
							...style,
							width: width_fixed ? "100%" : "auto",
						}}
						/>
						{descr}
					</div>
				: 
				<div
					className={` landing-empty ${class_name}`}
					style={{
								...style,
								minHeight: height || 400,
					}}
				>
					<LayoutIcon
					src={components()[this.props.type].icon}
					className=" layout-icon white "
					/>
					<div className="lead text-white">
					{ __(components()[this.props.type].title) }
					</div>
				</div>
		} 
		{
			is_full_open
				?
				<Dialog 
					isOpen={this.state.is_full_open}
					onClose={this.onClick}
					className="landing-outer-container m-4"
				>
					<img src={src} alt={title ? title.text : ""}  style={{height : "auto", width : "100%"}} />
					<Button 
						large intent={Intent.DANGER} 
						className=" position-absolute right m-2"
						icon="cross"
						onClick={this.onClick}
					/>
				</Dialog>
				:
				null
		}
		

		</>
	}
}
export default Image
