import React from "react"
import VK, {Poll} from "react-vk"

export default ({ api_key, poll_id }) => {
    return <>
        <div className="landing-vp-poll" id="vp_pol">
        </div>
        <VK apiId={api_key} onApiAvailable={() => console.log("VK ready")}>
            <Poll elementId="vp_pol" pollId={poll_id} />
        </VK>
    </>
}