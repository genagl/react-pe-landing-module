import React, { Component } from "react" 
import Style from "style-it"

class Testanomial extends Component {
  render() {
    const { class_name, style } = this.props
    const {
      text, name, avatar, description, palette
    } = this.props
    return (
      <div className={`landing-testanomial ${class_name}`} style={style}>
        {
          Style.it(
            `.text::before {
              content: "";
              position: absolute;
              left: 120px;
              bottom: -20px;
              border: 20px solid transparent;
              border-left: 20px solid ${palette.card.backgroundColor};
            }`,
            <div
              className="text"
              style={{
                ...palette.card
              }}
            >
              <div dangerouslySetInnerHTML={{ __html: text }} />
            </div>
          )

        }

        <div className="d-flex">
          <div className="avatar" style={{ backgroundImage: `url(${avatar})` }} />
          <div
            className="ltest-cont"
            style={{
              color: palette.main_text_color
            }}
          >
            <div className="name">
              {name}
            </div>
            <div
              className=""
              dangerouslySetInnerHTML={{ __html: description }}
            />
          </div>
        </div>
      </div>
    )
  }
}
export default Testanomial
