import React from "react" 
import SectionContent from "./SectionContent"
import Style from "style-it"
import { getFontNameByID } from "./data/PalettePresets"

class Quote extends SectionContent {
  is() {
    return this.props.data.text
  }

  renderContent() {
    const { palette } = this.props
    const {
      class_name, style, text, name, description, thumbnail
    } = this.props.data
    const media = thumbnail
      ?
      Style.it(
        `.landing-quote-thumbnail 
        {
          background-image:url(${thumbnail});
        }`,
        <div className="landing-quote-thumbnail" />
      )

      : null
    return (
      <div
        className={`landing-quote ${class_name}`}
        style={{
          color: palette.main_text_color,
          ...style
        }}
      >
        { media }
        <div 
          className="text" 
          style={{ 
            fontFamily: getFontNameByID( palette.card.title.fontFamilyID ),
            fontSize: palette.card.title.fontSize
          }}          
        >
          <span dangerouslySetInnerHTML={{ __html: text }} />
        </div>
        <div className="name">
          <span>
            {name}
          </span>
        </div>
        <div className="description">
          <span>
            {description}
          </span>
        </div>
      </div>
    )
  }
}
export default Quote
