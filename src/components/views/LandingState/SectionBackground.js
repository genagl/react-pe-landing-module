import React, { Component, Fragment } from "react"
import { getColor } from "./data/getColor"
import PalettePresets from "./data/PalettePresets"
import { getStyle } from "./Section"

class SectionBackground extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ...props,
      scroll: 0,
    }
    this.bgRef = React.createRef()
  }

  componentDidMount() {
    // if(!this.props.background.effect != "none") return;
    window.bgs.push(this)
  }

  componentWillUnmount() {
    if (!this.props.background.effect != "none") return
  }
  componentDidUpdate(nextProps, nexState) {
    if (nextProps.background !== this.state.background) {
      //console.log(nextProps.background)
      this.setState({ background: nextProps.background })
    }
    if (nextProps.palette !== this.state.palette) {
      //console.log(nextProps.palette.id, this.state.palette.id)
      this.setState({ palette: nextProps.palette })
    }

  }

  render() {
    const { background, palette, level } = this.state 
    const {
      effect,
      parallax_speed,
      parallax_y_offset,
      style,
      stack,
      image,
      color
    } = background
    
    //если секция вложенная и специальных свойств не указано - игнорировать
    if( level > 0 && palette.id === PalettePresets()[0].id && !color && !image )
      return null
    const img = image ? image : palette.background.tile
    const clr = color ? getColor(color, palette) : palette.background_color 
    const opacity = background.opacity
    const bgStyle = getStyle(style)

    let poffset, backgroundAttachment
    switch (effect) {
      case "vparallax":
        backgroundAttachment = "fixed"
        poffset = this.state.scroll * parallax_speed - (parallax_y_offset || 0)
        break
      case "fixed":
        backgroundAttachment = "fixed"
        poffset = this.state.scroll - (parallax_y_offset || 0)
        break
      default:
        backgroundAttachment = null
        poffset = 0
    }
    const size = background.size
      ?
      background.size
      :
      typeof palette.background.size == "string"
        ?
        "cover"
        :
        background.tile_size_px
          ?
          `${background.tile_size_px}px`
          :
          palette.background.size
            ?
            `${palette.background.size}px`
            :
            "200px"
    const tile_opacity = background.tile_opacity ? background.tile_opacity : palette.background.tileOpacity
    const tile_size_px = background.tile_size_px

    const st = {
      backgroundAttachment,
      backgroundImage: `url(${img})`,
      backgroundSize: size,
      backgroundRepeat: palette.repeat,
      backgroundPositionY: poffset,
      opacity: tile_opacity || 1,
      ...bgStyle,
    }
    if (tile_size_px) {
      st.backgroundSize = tile_size_px
    }

    const bck = <div
      className="landing-section__bg"
      style={{
        background: clr,
        opacity: opacity || 1,
      }}
    />
    const tile = <div
      ref={this.bgRef}
      className="landing-section_img_bg"
      style={st}
    />
    return stack == "image"
      ?
      <>
        {bck}
        {tile}
        <div className="display-1 text-danger position-absolute hidden">{size}</div>
      </>
      :
      <>
        {tile}
        {bck}
        <div className="display-1 text-danger position-absolute hidden">{size}</div>
      </>
  }


  render2() {
    if (!this.props.background) return null
    const {
      image,
      tile_opacity,
      size,
      tile_size_px,
      color,
      opacity,
      effect,
      parallax_speed,
      parallax_y_offset,
      style,
      stack,
      palette
    } = this.props.background
    const bgStyle = getStyle(style)
    let poffset, backgroundAttachment="unset"
    switch (effect) {
      case "vparallax":
        poffset = this.state.scroll * parallax_speed - (parallax_y_offset || 0)
        backgroundAttachment="fixed"
        break
      case "fixed":
        poffset = this.state.scroll - (parallax_y_offset || 0)
        backgroundAttachment="fixed"
        break
      default:
        poffset = 0
    }
    console.log(palette)
    const st = {
      backgroundImage: `url(${palette.tile})`,
      backgroundSize: palette.size,
      backgroundRepeat: palette.repeat,
      backgroundPositionY: poffset,
      opacity: tile_opacity || 1,
      backgroundAttachment,
      ...bgStyle,
    }
    if (tile_size_px) {
      //st.backgroundSize = tile_size_px
    }
    // console.log( poffset );
    if (palette.tile || image || color) {
      return stack == "image"
        ? (
          <>
            <div
              className="landing-section__bg"
              style={{
                background: getColor(color, palette) || "transparent",
                opacity: opacity || 1,
              }}
            />
            <div
              ref={this.bgRef}
              className="landing-section_img_bg"
              style={st}
            />
            <div className="display-1 text-danger position-absolute hidden">{size}</div>
          </>
        )
        : (
          <>
            <div
              ref={this.bgRef}
              className="landing-section_img_bg"
              style={st}
            />
            <div
              className="landing-section__bg"
              style={{
                backgroundColor: getColor(color, palette) || "transparent",
                opacity: opacity || 1,
              }}
            />
            <div className="display-1 text-danger position-absolute hidden">{size}</div>
          </>
        )
    }
    return null
  }
}
export default SectionBackground
