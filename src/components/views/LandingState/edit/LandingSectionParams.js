import {
  ButtonGroup, Button, Drawer, Position,
} from "@blueprintjs/core"
import React, { Component } from "react"
import { __ } from "react-pe-utilities"
import matrix from "../data/matrix.json"

import CardTemplateEditor from "./sectionParamsEditors/CardTemplateEditor" 

class LandingSectionParams extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ...this.props,
      matrixSrc: {}
    }
  }

  render() {
    // console.log(matrix[this.state.type].sparams.sparam_id);
    const btns = (
      <ButtonGroup>
        {
          Array.isArray(matrix[this.state.type].sparams.sparam_id)
            ? matrix[this.state.type].sparams.sparam_id.map((e, i) => (
              <Button
                key={i}
                onClick={(evt) => this.onClick(evt, e)}
                className="py-1 hint hint--left"
                data-hint={__(e.hint)}
              >
                <span dangerouslySetInnerHTML={{ __html: __(e.title) }} />
              </Button>
            ))
            : null
        }
      </ButtonGroup>
    )
    let content
    switch (this.state.matrixSrc.editor) {
      case "CardTemplateEditor":
        content = <CardTemplateEditor
            {...this.state }
            onEdit={this.props.onEdit}
            onApply={this.props.onApply}
            onTry={this.props.onTry}
            onUpdate={this.props.onUpdate}
        />
        break;

    }
    return <>
      <div className="landing-sector__params mr-5" style={{top: this.state.dopEditTop}}>
        {btns}
      </div>
      <Drawer
        isOpen={this.state.isDialogOpen}
        onClose={this.onDialogClose}
        title={<span dangerouslySetInnerHTML={{ __html: this.state.DialogTitle  }} />}
        position={Position.RIGHT}
        usePortal
        backdropClassName="landing-drawer-bg"
        size={700}
      >
        <div className="pt-0 px-0 overflow-y-auto flex-grow-100 bg-tripp" >
          {content}
        </div>
      </Drawer>
    </>
  }

  onClick = (evt, data) => {
    this.setState({
      isDialogOpen: true,
      DialogTitle: __(data.title) + "  " + __(data.hint),
      matrixSrc: data
    })
  }

  onDialogClose = () => {
    this.setState({ isDialogOpen: false })
  }
}
export default LandingSectionParams
