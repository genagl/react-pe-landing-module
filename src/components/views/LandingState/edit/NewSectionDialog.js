import React, { Component } from "react"
import {
  Button, ButtonGroup, Intent, Popover, Position, Drawer, Dialog
} from "@blueprintjs/core"
import { __ } from "react-pe-utilities"
import {LayoutIcon} from 'react-pe-useful'
import InputForm from "./InputForm"
import section_groups from "../data/section_groups.json"
import matrix from "../data/matrix" 
import { components } from "../data/components"

class NewSectionDialog extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ...props,
      current_type: "html",
      currentTypeGroup: matrix.html.sparams.group,
    }
  }

  componentWillUpdate(nextProps) {
    const state = {}
    Object.keys(nextProps).forEach((e) => {
      if (this.state[e] != nextProps[e]) state[e] = nextProps[e]
    })
    if (Object.keys(state).length > 0) {
      this.setState(state)
    }
  }
  onType = (evt) => {
    const id = evt.currentTarget.getAttribute("id")
    //console.log(id)
    this.setState({ currentTypeGroup: id })
  }
  onTypeSwitch = (evt) => {
    const current_type = evt.currentTarget.getAttribute("type")
    let data = {}
    //console.log(current_type)
    if (matrix[current_type].default) {
      data = { ...matrix[current_type].default, sectionType: current_type }
      delete data.hidden
    }
    // console.log( data, current_type );
    this.setState({ data, current_type, is_change_type_enbl: current_type != this.state.type })
  }

  tabs() {
    const btns = []
    const tabs = Object.entries(section_groups).map((e, i) => {
      const element = e[1]
      if (element.hidden) return
      return (
        <Button
          key={i}
          minimal
          small
          id={element.id}
          text={__(element.title)}
          active={this.state.currentTypeGroup == element.id}
          onClick={this.onType}
          rightIcon={matrix[this.state.current_type].sparams.group == element.id ? "dot" : "empty"}
        />
      )
    })
    for (const c in components()) {
      if (matrix[c].sparams && matrix[c].sparams.group != this.state.currentTypeGroup) {
        continue
      }
      const cl = c == this.state.current_type ? "active " : " "
      const ccl = c == this.state.type ? " text-danger " : " "
      btns.push(<div
        key={c}
        type={c}
        className={`l-icon ${cl}`}
        onClick={this.onTypeSwitch}
      >
        <div>
          <LayoutIcon
            src={components()[c].icon}
            className="layout-icon"
          />
          <div className={ccl}>
            {__(components()[c].title)}
          </div>
        </div>
      </div>)
    }
    return (
      <div className="p-4">
        <div className="d-flex">
          <div className="landing-type-menu">
            <ButtonGroup className="  text-right" vertical>
              {tabs}
            </ButtonGroup>
          </div>
          <div className="landing-type-menu-cont">
            {btns}
          </div>
        </div>

      </div>
    )

  }


  render() {
    //console.log( this.state.data );
    return (
      <Dialog
        isOpen={this.state.isOpen}
        onClose={this.props.onClose}
        title={__("Inserting new. Choose type")}
        className="landing-outer-container"
      >
        <div className="pt-0 px-0 overflow-y-auto flex-grow-100 bg-tripp">
          <div className="m-4">
            {this.tabs()}
          </div>
        </div>
        <ButtonGroup className="mx-auto py-3">
          <Button
            onClick={this.onEdit}
          >
            {__("Insert")}
          </Button>
        </ButtonGroup>
      </Dialog>
    )
  }

  onField(value, field) {
    const { data } = this.state // { ...this.state.data}
    data[field] = value
    //console.log(data)
    // this.setState( { data } );
  }

  onEdit = () => {
    //console.log(this.state.data)
    this.props.onEdit( this.state.data, this.state.current_type )
  }
}

export default NewSectionDialog
