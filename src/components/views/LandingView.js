import React from "react"
import ReactDOM from "react-dom"
import $ from "jquery"
import { withRouter } from "react-router"
import { compose } from "recompose"
import { withApollo } from "react-apollo"
import axios from "axios"
import {
	Button, ButtonGroup, Intent, Dialog, Icon, Tooltip, Position, Callout,
} from "@blueprintjs/core"
import gql from "graphql-tag"
import WebFont from "webfontloader"
import { __ } from "react-pe-utilities"
import BasicState from "react-pe-basic-view"
import matrix from "./LandingState/data/matrix.json"

import { isRole } from "react-pe-utilities"
import {Loading} from 'react-pe-useful'
import LandingEditDialog from "./LandingState/edit/LandingEditDialog"
import PalettePresets from "./LandingState/data/PalettePresets"
import Section, { getDefault } from "./LandingState/Section"
import DataContext from "./LandingState/DataContext"
import "./LandingState/assets/css/style.css"
import "./LandingState/assets/css/animate.css"

import { AppToaster } from 'react-pe-useful'
import NewSectionDialog from "./LandingState/edit/NewSectionDialog"
import ChoosePaletteDialog from "./LandingState/edit/ChoosePaletteDialog" 
import {PEHelp} from "react-pe-useful"

import ToUp from "./LandingState/ToUp"

class LandingView extends BasicState {
	local = this.props.extend_params && this.props.extend_params.local;

	getId() {
		// console.log( this.props.extend_params );
		return this.props.extend_params && this.props.extend_params.id ? this.props.extend_params.id : "landing_page"
	}

	setID(lid) {
		// console.log(lid)
		this.setState(
			{ lid },
			this.loadLanding(lid),
		)
	}

	localLoad(lid) {
		axios.get( "/assets/data/" + this.props.extend_params.id + ".json" )
		.then(
			(response) => {
				const parsed2 = response.data
				//console.log( parsed2 );	
				DataContext.upd(parsed2)
				//console.log( DataContext.data );
				// console.log( ["Open Sans", ...DataContext.data.landing.fonts.map(e => e.title )] )
				WebFont.load({
					google: {
						families: ["Open Sans", ...DataContext.data.landing.fonts.map((e) => e.title)],
					},
				})
				this.setState({ loading: false, is_edit: DataContext.data.sections ? DataContext.data.sections.length === 0 : false })
				if (!parsed2 || !parsed2.sections || parsed2.sections.length === 0) {
					this.onLandingClear()
				}
			},
			error => console.log(error)
		)





		
	}

	serverLoad(lid) {
		// console.log('Дебютировал наш герой, когда "Реалом" руководил Хорхе Вальдано – человек, отдавший клубу лишь три года как футболист, но немало поработавший в Мадриде после завершения карьеры. Установка Вальдано молодому игроку состояла всего из двух слов: "Твори, парень!".'.replace(/"([^"]+)"/g, '«$1»') )
		const query = gql`
			query
			{
				getPE_Landing( id : "${lid}" )
				{
					json
				}
			}
		`
		this.props.client.query({ query })
			.then((result) => {
				const { json } = result.data.getPE_Landing
				const replaced_json = json.replace(/'/gi, "\"")
				let replaced2_json = replaced_json.replace(/~~/gi, "'")
				//replaced2_json = replaced2_json.replace(/\s/gi, "")
				replaced2_json = replaced2_json.replace(/\t/gi, "")
				replaced2_json = replaced2_json.replace(/\n/gi, "")
				//console.log( replaced2_json);

				const parsed = replaced2_json ? JSON.parse(replaced2_json) : { sections: [] }
				DataContext.upd(parsed)
				// console.log(parsed );
				if (parsed && parsed.landing) {
					if (parsed.landing.css) {
						$("head").append(`<style id='landing-css'>${parsed.landing.css}</style>`)
					}
					if (parsed.landing.is_hide_header) {
						$(".layout-header").hide()
					}
				}
				// console.log( parsed );
				// console.log( DataContext.data.sections.length );
				if (!parsed.sections || parsed.sections.length === 0) {
					this.onLandingClear()
				}

				// console.log( ["Open Sans", ...DataContext.data.landing.fonts.map(e => e.title )] )
				const fonts = DataContext.data.landing.fonts
					? DataContext.data.landing.fonts.map((e) => e ? e.title : "Open Sans")
					: []
				WebFont.load({
					google: {
						families: ["Open Sans", ...fonts],
					},
				})
				this.setState({
					loading: false, 
					is_edit: DataContext.data.sections.length === 0
				})
			})
	}

	loadLanding(lid) {
		this.local ? this.localLoad(lid) : this.serverLoad(lid)
	}

	basic_state_data() {
		const el = document.createElement("script")
		el.type = "text/javascript"
		el.src = "https://cdn.jsdelivr.net/npm/social-likes/dist/social-likes.min.js"
		el.async = true
		el.id = "landing"

		const stl = document.createElement("link")
		stl.type = "text/css"
		stl.src = "https://cdn.jsdelivr.net/npm/social-likes/dist/social-likes_birman.css"
		stl.async = true

		return {
			loading: true,
			is_edit: false,
			isHelpOpen: false,
			s: false,
			isLocalMessageOpen : true,
			fixedChildren: [],
			newSectionType: "html"
		}
	}

	stateDidMount() {
		window.bgs = []
		window.pe_landing = { section: [] }
		// $(".layout-header").hide();
		document.body.addEventListener("mousemove", this.onMove)
		window.addEventListener("scroll", this.onscrollHandler)
		window.addEventListener("resize", this.updateWindowDimensions)
		this.updateWindowDimensions()
		const lid = this.getId()
		// console.log(lid)
		const query = gql`
			query
			{
				getPE_LandingID( id : "${lid}" ) 
			}
		`
		this.props.client.query({ query })
			.then((result) => {
				const json = result.data.getPE_LandingID
				// console.log( json );
				this.setID(json)
			})
		/**/
	}
	componentDidUpdate()
	{
		if(this.state.is_not_update)
		{
			window.onbeforeunload = function() {
				return "NO! NO! NO!";
			}
		}
		else
		{
			window.onbeforeunload = function() {
				 
			}
		}
	}

	componentWillMount() {

	}

	componentWillUnmount() {
		$(".layout-header").show()
		document.body.removeEventListener("mousemove", this.onMove)
		window.removeEventListener("scroll", this.onscrollHandler)
		window.removeEventListener("resize", this.updateWindowDimensions)
		delete window.bgs
		delete window.pe_landing
	}

	onscrollHandler(evt) {
		if (!window.bgs) return
		window.bgs.forEach((e) => {
			try {
				const node = ReactDOM.findDOMNode(e)
				e.setState({ scroll: window.scrollY - $(node).offset().top })
			} catch (e) {

			}
		})
	}

	updateWindowDimensions = () => {
		if (Array.isArray(window.pe_landing.section) && window.pe_landing.section.length > 0) {
			window.pe_landing.section.forEach((e, i) => {
				// console.log( e.state );
				const {
					visible_lg, visible_sm, visible_ms, visible_xs,
				} = e.state
				const w = document.body.clientWidth

				const state = {
					section_width: w,
					section_height: document.body.clientHeight,
				}
				// const old = e.is_visible
				// hide all = show all
				const visible_all = !visible_lg && !visible_sm && !visible_ms && !visible_xs
				if (w > 1200) {
					e.is_visible = visible_lg || visible_all
				} else if (w > 740) {
					e.is_visible = visible_sm || visible_all
				} else if (w > 540) {
					e.is_visible = visible_ms || visible_all
				} else {
					e.is_visible = visible_xs || visible_all
				}
				// if( old !== e.is_visible )
				e.setState(state)
				e.updateWidth(w)
			})
		}
	}

	onMove = (evt) => {
		window.mouseX = evt.offsetX
		window.mouseY = evt.offsetY
	}

	render() {
		if (this.state.loading) {
			return (
				<div className="layout-state layout-center">
					<Loading />
				</div>
			)
		}
		//console.log(DataContext.data.sections)
		const landings = Array.isArray(DataContext.data.sections) && DataContext.data.sections.length > 0
			?
			DataContext.data.sections.map((e, i) => {
				/* TEMPLATE 
				let palette = DataContext.data.landing.palette
					?
					DataContext.data.landing.palette.filter((ee) => ee.id === e.current_template_id)[0]
					:
					null
				palette = palette || PalettePresets()[0]
				/* END TEMPLATE */
				// console.log( e )	
				return (
					<Section
						{...e}
						section_id={e.id}
						key={i}
						i={i}
						user={this.props.user}
						is_admin={this.is_admin()}
						is_edit={this.state.is_edit}
						level={0}
						onEdit={this.onEdit}
						onUp={this.onUp}
						onDn={this.onDn}
						onAdd={this.onAdd}
						onDouble={this.onDouble}
						onRnv={this.onRnv}
						onHide={this.onHide}
						onRemoveFloat={this.onRemoveFloat}
						onUpdateFloat={this.onUpdateFloat}
						onClipboardCopy={this.onClipboardCopy}
						onClipboardPaste={this.onClipboardPaste}
						onWaypointEnter={this.onWaypointEnter}
						onWaypointLeave={this.onWaypointLeave}
						onFixedAdd={this.onFixedAdd}
						getHelp={this.getHelp}
					/>
				)
			})
			:
			<div className="p-5">
				<Callout className="p-5 text-center">
					{__("No landing data")}
					{
						this.state.is_edit
							? <>
								<div className="pt-4 d-flex">
									<ButtonGroup
										className="mx-auto drawer"
										vertical={true} fill={true}
										large={true}
									>
										<Button onClick={this.onChoosePresetsOpen} icon="snowflake">
											{__("Choose presets")}
										</Button>
										<Button onClick={this.onAdd} icon="plus">
											{__("Add first section")}
										</Button>
									</ButtonGroup>
									<ChoosePaletteDialog
										isOpen={this.state.isPaletteOpen}
										onEdit={this.onChoosePresets}
										onSet={this.onSetPresets}
										onClose={this.onChoosePresetsOpen}
										value={
											DataContext.data.landing && DataContext.data.landing.palette
												?
												DataContext.data.landing.palette
												:
												[]
										}
									/>
								</div>
							</>
							: null
					}
				</Callout>
			</div>
		let c, row
		if (DataContext.data.landing && DataContext.data.landing.is_blocked) {
			c = (
				<>
					<div className="landing-container-left-field">
						<div
							style={{ backgroundImage: `url(${DataContext.data.landing.left_image})` }}
						/>
						<div
							style={{
								backgroundColor: DataContext.data.landing.left_color,
								opacity: DataContext.data.landing.left_color_opacity,
							}}
						/>
					</div>
					<div className="container">
						{landings}
					</div>
					<div className="landing-container-right-field">
						<div
							style={{ backgroundImage: `url(${DataContext.data.landing.right_image})` }}
						/>
						<div
							style={{
								backgroundColor: DataContext.data.landing.right_color,
								opacity: DataContext.data.landing.right_color_opacity,
							}}
						/>
					</div>
				</>
			)
			row = " flex-row "
		} else {
			c = landings
			row = " flex-column "
		}

		const toTop = DataContext.data && DataContext.data.landing && DataContext.data.landing.is_up_to_top_button
			? 
			<ToUp 
				{
					...DataContext.data.landing
				}
			/>
			: 
			null
		
		const fixedChildren = this.state.fixedChildren.map((e, ii) => ({ ...e, key: ii }))
		return (
			<div
				className={`landing-container layout-state p-0 ${row}`}
			>
				<div
					className="landing-cfIcon-conttainer"
					id="landing-main-float"
					children={[
						...fixedChildren,
						
					]}
				/>
				{ toTop }
				{
					this.is_admin()
						? 
						!this.state.is_edit
							? (
								<div className="landind-edit-cont">
									<Tooltip
										position={Position.LEFT}
										content={__("Start edit Landing")}
									>
										<div
											className="l-inline-edit-btn"
											onClick={this.onEditHandler}
											title={__("Start edit Landing")}
											style={{ position: "relative", top: "auto", right: 0 }}
										>
											<Icon icon="annotation" />
										</div>
									</Tooltip>
								</div>
							)
							: 
							<>
							{ this.getEditPanel() }
							
							<div className="landind-edit-cont">
								<Button
									intent={Intent.NONE}
									onClick={this.onSettings}
									large={false}
									icon="cog"
									className="mr-1"
									title={__("Landing settings")}
								/>
								<Button
									intent={Intent.NONE}
									onClick={this.onHelp}
									large={false}
									icon="help"
									className="mr-1"
									title={__("Help")}
								/>
								<div style={{ width: this.state.is_not_update ? 270 : 0 }} className="landing-upd-cont mr-1">
									<Button
										intent={Intent.SUCCESS}
										onClick={this.onUpdateHandler}
										large={false}
										icon="floppy-disk"
										title={__("Update Landing")}
										style={{ width: 270 }}
									>
										{__("Update Landing")}
									</Button>
								</div>
								<Button
									intent={Intent.DANGER}
									onClick={this.onEditHandler}
									large={false}
									className="mr-1"
									title={__("Finish edit Landing")}
									icon="cross"
								/>
							</div>
							<Dialog
								isOpen={this.state.isClearOpen}
								onClose={this.onClearOpen}
								title={__("Clear Landing?")}
								className="little"
							>
								<div className="p-4 layout-centered">
									<div>
										<div className="pb-3">
											{__("Realy clear all content?")}
										</div>
										<ButtonGroup>
											<Button
												intent={Intent.NONE}
												onClick={this.onLandingClear}
												text={__("Yes, clear")}
											/>
											<Button
												intent={Intent.DANGER}
												icon="cross"
												onClick={this.onClearOpen}
											/>
										</ButtonGroup>
									</div>
								</div>
							</Dialog>
							<PEHelp
								isOpen={this.state.isHelpOpen}
								onClose={this.onHelp}
								url={this.state.help_url}
							/>
							<LandingEditDialog
								isOpen={this.state.isLandingEditOpen}
								onClose={this.onSettings}
								data={DataContext.data.landing}
								onEdit={this.onLandingEdit}
								onAdd={this.onAdd}
								onDownload={this.onDownload}
								onLoadChange={this.onLoadChange}
								onClearOpen={this.onClearOpen}
							/>
							<NewSectionDialog
								isOpen={this.state.isNewSectionOpen}
								onClose={this.onNewSection}
								onEdit={this.onAddStart}
							/>
							</>	
						: 
						null
				}
				{c}
			</div>
		)
	}
	getEditPanel = () =>
	{
		return this.local
			?
			this.getLocalEditPanel()
			:
			this.getServerEditPanel()
	}
	getLocalEditPanel = () =>
	{
		return <>
			<Dialog
				isOpen={this.state.isLocalMessageOpen}
				onClose={this.onLocalMessageOpen}
				title={__("Attention")}
				className="little"
			>
				<div className="px-5 pt-5 pb-4 layout-centered flex-column align-items-center">
					<div dangerouslySetInnerHTML={{ __html :__( "This landing is localy edited" ) }} />	
					<div className="mt-4">
						<a 
							className="btn btn-light" 
							href="http://ux.protopia-home.ru/developers/react-client/compile" 
							target="_blank" 
							rel="noreferrer"
						>
							{__("More info")}
						</a>
					</div>
				</div>
			</Dialog>
			<Dialog
				isOpen={this.state.realLocalUpdateOpen}
				onClose={ this.onLocalUpdate }
				title={__("Attention")}
				className="little"
			>
				<div className="px-5 pt-5 pb-4 layout-centered flex-column align-items-center">
					<div dangerouslySetInnerHTML={{ __html :__( "This landing is localy edited" ) }} />		
					<div className="mt-2 btn-group">
						<a 
							className="btn btn-light" 
							href="http://ux.protopia-home.ru/developers/react-client/compile" 
							target="_blank" 
							rel="noreferrer"
						>
							{__("More info")}
						</a>
						<div className="btn btn-light" onClick={this.onFinishLocalUpdate}>
							{ __("Download source json") }
						</div>
						<div className="btn btn-danger" onClick={this.onLocalUpdate}>
							<i className="fas fa-times"/>
						</div>
					</div>
				</div>
			</Dialog>
		</>
	}
	onLocalMessageOpen = evt =>
	{
		this.setState({ isLocalMessageOpen : !this.state.isLocalMessageOpen })
	}
	getServerEditPanel = () =>
	{
		return <>			
		</>
	}	


	getHelp = (help_url) =>
	{
		this.setState({ isHelpOpen: true, help_url:help_url })
	}
	onHelp = () => {
		this.setState({ isHelpOpen: !this.state.isHelpOpen })
	}
	onLandingEdit = (data) => {
		DataContext.setLanding(data)
		this.setState({ isLandingEditOpen: false, is_not_update: true })
		$("#landing-css")
			.empty()
			.append(data.css)
	}

	onSettings = () => {
		if (this.state.isLandingEditOpen)
			$("body").css({ overflowY: "auto" })
		this.setState({ isLandingEditOpen: !this.state.isLandingEditOpen })
	}
	onNewSection = data => {
		this.setState({ isNewSectionOpen: !this.state.isNewSectionOpen })
	}

	onLoadChange = (evt) => {
		// console.log(evt.target.files);
		if (evt.target.files[0].type === "application/json") {
			const context = this
			const reader = new FileReader()
			reader.readAsText(evt.target.files[0])
			reader.onload = () => {
				const data = JSON.parse(reader.result)
				//console.log(data)
				if (
					typeof data.sections != "undefined"
					&& typeof data.maxSectionID != "undefined"
					&& typeof data.maxFloatID != "undefined"
				) {
					DataContext.upd(data)
					context.setState({ s: !context.state.s })
				} else {
					AppToaster.show({
						intent: Intent.DANGER,
						icon: "tick",
						duration: 10000,
						message: __("File is not Landing format"),
					})
				}
			}
		} else {
			AppToaster.show({
				intent: Intent.DANGER,
				icon: "tick",
				duration: 10000,
				message: __("Choose JSON file."),
			})
		}
	}

	is_admin() {
		return isRole("administrator", this.props.user)
	}

	onEditHandler = () => {
		this.setState({ is_edit: !this.state.is_edit })
	}

	onLocalUpdate = () =>
	{ 
		this.setState({ realLocalUpdateOpen: !this.state.realLocalUpdateOpen })
	}
	onFinishLocalUpdate = () =>
	{
		this.onDownload()
		this.setState({
			is_edit: !this.state.is_edit,
			loading: false,
			realLocalUpdateOpen: false,
			is_not_update: false,
		})
	}

	onServerUpdate = () =>
	{
		// console.log( JSON.stringify(DataContext.data).replace(/"([^"]+)"/g, '«$1»') );

		const data = JSON.stringify(DataContext.data).replace(/'/g, "~~").replace(/"/g, "'")
		//console.log(data);
		this.setState({ loading: true })
		const lid = this.getId()
		const chagePE_Landing = gql`
			mutation chagePE_Landing
			{
				chagePE_Landing( id : "${lid}", input:"${data}")
			}
		`
		this.props.client.mutate({
			mutation: chagePE_Landing,
			update: (store, { data: { chagePE_Landing } }) => {
				this.setState({
					is_edit: !this.state.is_edit,
					loading: false,
					is_not_update: false,
				})
			},
		})
	}

	onUpdateHandler = () => {
		window.bgs = []
		this.state.fixedChildren = []
		this.local ? this.onLocalUpdate() : this.onServerUpdate()
	}

	onEdit = (data) => {
		delete data.user;
		delete data.section_id
		delete data.tab
		delete data.navbarTabId
		delete data.myX
		delete data.section_height
		delete data.section_width
		//console.log( DataContext.data );
		//console.log( data.id );
		DataContext.updateSection(data.id, data)
		//console.log(data);
		//console.log(DataContext.data);
		this.setState({ s: !this.state.s, is_not_update: true })
	}

	onUp = (data) => {
		//console.log("UP", data)
		const sections = [...DataContext.data.sections]
		const sec = { ...sections[data] }
		sections.splice(data, 1)
		sections.splice(data - 1, 0, sec)
		DataContext.upd({ ...DataContext.data, sections })
		this.setState({ s: !this.state.s, is_not_update: true })
	}

	onDn = (data) => {
		//console.log("DN", data)
		const sections = [...DataContext.data.sections]
		const sec = { ...sections[data] }
		sections.splice(data, 1)
		sections.splice(data + 1, 0, sec)
		DataContext.upd({ ...DataContext.data, sections })
		this.setState({ s: !this.state.s, is_not_update: true })
	}

	onRnv = (data) => {
		//console.log("RMV", data)
		const sections = [...DataContext.data.sections]
		sections.splice(data, 1)
		DataContext.upd({ ...DataContext.data, sections })
		this.setState({ s: !this.state.s, is_not_update: true })
	}
	onChoosePresetsOpen = () => {
		this.setState({ isPaletteOpen: !this.state.isPaletteOpen })
	}
	onChoosePresets = () => {

	}
	onSetPresets = presets => {
		//console.log(presets);
		const data = {
			landing: {
				...DataContext.data.landing,
				palette: presets
			}
		}
		//console.log(data)
		DataContext.upd(data)
		//console.log(DataContext.data.landing)
		this.setState({ isPaletteOpen: !this.state.isPaletteOpen })
	}
	onAdd = data => {
		//console.log(data)
		this.setState({ isNewSectionOpen: true, newSectionOrder: data })
	}
	onAddStart = (data, type) => {
		const sections = [...DataContext.data.sections || []]
		let order = typeof this.state.newSectionOrder === "number" 
				?
				this.state.newSectionOrder 
				: 
				typeof this.state.newSectionOrder === "string"
					?
					PerformancePaintTiming( this.state.newSectionOrder )
					:
					0
		let data1 = { data: {} }
		if (matrix[type].default) {
			//console.log(DataContext.data.landing.palette[0].id)
			const ifPal = Array.isArray(DataContext.data.landing.palette) && DataContext.data.landing.palette[0]
			const defPal = PalettePresets()[0].id
			const pal = ifPal
				?
				DataContext.data.landing.palette[0].id
				:
				defPal
			//console.log(pal)
			//console.log(DataContext.data.sections, order)
			data1 = {
				data: { ...matrix[type].default },
				composition: matrix[type].default.composition,
				current_template_id: DataContext.data.sections && DataContext.data.sections[order]
					?
					DataContext.data.sections[order].current_template_id
					:
					pal
			}
			delete data1.data.hidden
			delete data1.data.composition
		}
		//console.log( order )
		const sec = getDefault(type, data1)
		//console.log("ADD", type, matrix[type].default);
		//console.log("ADD", data1);
		sections.splice(order + 1, 0, sec)
		//console.log(sections)
		DataContext.upd({ ...DataContext.data, sections })
		this.setState({ isNewSectionOpen: false, is_not_update: true })
	}

	onDouble = (data) => {
		const sections = [...DataContext.data.sections]
		//console.log("DOUBLE", data, sections[data])
		const newID = DataContext.getMaxSectionID(true)
		const sec = {
			...sections[data],
			section_id: newID,
			id: newID,
			menu: { ...sections[data].menu, id: `section-${newID}` },
		}
		delete sec.visible_lg
		delete sec.visible_sm
		delete sec.visible_sm
		delete sec.visible_ms
		delete sec.visible_xs
		sections.splice(data + 1, 0, sec)
		//console.log(sections[data + 1])
		DataContext.upd({ ...DataContext.data, sections })
		this.setState({ s: !this.state.s, is_not_update: true })
	}

	onHide = (id, is_hide) => {
		//console.log("HIDE", id, is_hide)
		DataContext.hideSection(id, is_hide)
		//console.log(DataContext.data)
		this.setState({ s: !this.state.s, is_not_update: true })
	}

	onClearOpen = () => {
		this.setState({ isClearOpen: !this.state.isClearOpen })
	}

	onLandingClear = () => {
		window.bgs = []
		DataContext.clear()
		this.setState({
			loading: false,
			s: !this.state.s,
			isClearOpen: false,
			is_not_update: true,
			isLandingEditOpen: true
		})
	}

	onRemoveFloat = (float_id) => {
		DataContext.deleteFloatId(float_id)
		this.setState({ s: !this.state.s, is_not_update: true })
	}

	onUpdateFloat = (data, float_id, section_id) => {
		//console.log(data, float_id, section_id)
		DataContext.updateFloat(data, float_id, section_id)
		//console.log(DataContext.data)
		this.setState({ s: !this.state.s, is_not_update: true })
	}

	onClipboardCopy = (i, data) => 
	{
		//console.log( data.id) 
		//console.log(DataContext.getSectionJSON(data.id))
		
		$("body").append(`<div style='position:absolute; z-index:-100; width:100%; top:0; left:0;'><textarea style='width:100%;' id='myInput'>${DataContext.getSectionJSON(data.id)}</textarea></div>`)
		const copyText = document.getElementById("myInput")
		copyText.select()
		copyText.setSelectionRange(0, 99999999999999999999)
		document.execCommand("copy")
		$("#myInput").remove()
		AppToaster.show({
			intent: Intent.SUCCESS,
			icon: "tick",
			duration: 10000,
			message: __("Section copy to clipbord"),
		})
	}

	onClipboardPaste = (i) => {
		navigator.clipboard.readText()
			.then((clipText) => {
				try 
				{
					DataContext.data.sections.splice(i, 0, DataContext.setSectionJSON(clipText))
					this.setState({ is_not_update: true })
				} 
				catch (e) 
				{
					AppToaster.show({
						intent: Intent.DANGER,
						icon: "tick",
						duration: 10000,
						message: __("Error read clipboard data"),
					})
				}
			})
	}

	onDownload = () => {
		// const downloadFile = async () =>
		// {
		const myData = DataContext.data 	// I am assuming that "this.state.myData"
		// is an object and I wrote it to file as
		// json
		const fileName = "file"
		const json = JSON.stringify(myData)
		const blob = new Blob([json], { type: "application/json" })
		const href = URL.createObjectURL(blob)
		const link = document.createElement("a")
		link.classList.add("lead")
		link.classList.add("bg-light")
		link.classList.add("p-5")
		link.href = href
		link.download = `${fileName}.json`
		document.body.appendChild(link)
		link.click()
		// document.body.removeChild(link);
		// }
	}

	onWaypointEnter = (section_id) => {
		// console.log("WaypointEnter", section_id)
	}

	onWaypointLeave = (section_id) => {
		// console.log("WaypointLeave", section_id);
	}

	onFixedAdd = (data) => {
		this.setState({ fixedChildren: [...this.state.fixedChildren, data] })
	}
}
export default compose(
	withRouter,
	withApollo,
)(LandingView)
