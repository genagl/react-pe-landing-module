function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, Intent, Dialog, Tag, Slider, MultiSlider, ButtonGroup, Callout } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import matrix from "../views/LandingState/data/matrix";
import Columns from "./landingObject/Columns";
import Padding from "./landingObject/Padding";
import Margin from "./landingObject/Margin";
import Border from "./landingObject/Border";
import SVGLabrary from "./landingObject/SVGLabrary";
import SectionButton from "./landingObject/SectionButton";
import PaletteForm from "./landingObject/PaletteForm";
import ContanctFormFieldButton from "./landingObject/ContanctFormFieldButton";
import CardSingleFieldType from "./landingObject/CardSingleFieldType";
import YandexMapPlaceBottom from "./landingObject/YandexMapPlaceBottom";
import CardButton from "./landingObject/CardButton";
import CardField from "./landingObject/CardField";
import GoogleFonts from "./landingObject/GoogleFonts"; //import CardSingleAllFields from "./landingObject/CardSingleAllFields"

import CardSingleField from "./landingObject/CardSingleField";
import VideoButton from "./landingObject/VideoButton";
import PlaceTypeButton from "./landingObject/PlaceTypeButton";
import Composition from "./landingObject/Composition";
import LasyLoadType from "./landingObject/LasyLoadType";
import TextFormatForm from "./landingObject/TextFormatForm";
import Style from "./landingObject/Style";
import Object_Position from "./landingObject/Object_Position";
import { getVisibleValue } from "react-pe-layouts";
import { getTypeSelector } from "react-pe-layouts";
import TypeDialog from "../views/LandingState/edit/TypeDialog";
import InputForm from "../views/LandingState/edit/InputForm";
import DataContext from "../views/LandingState/DataContext";
import { getDefault } from "../views/LandingState/Section";
import CSSColor from "./utilities/CSSColor";
import LandingParameters from "./landingObject/LandingParameters";
import CardTemplateLib from "./landingObject/CardTemplatesLib";
import ExternalLandingColor from "./landingObject/ExternalLandingColor";
import CardFieldMetaphorEdit from "./landingObject/CardFieldMetaphorEdit";
import NewSectionDialog from "../views/LandingState/edit/NewSectionDialog";
import { AppToaster } from 'react-pe-useful';
import { addEmpty, SubMenu } from "react-pe-scalars";
import $ from "jquery";

class LandingObject extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", { ...this.props,
      isDialog: false,
      object: {}
    });

    _defineProperty(this, "dialogContent", () => {
      let value;

      switch (this.state.object.landing_type) {
        case "section":
          return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(TypeDialog, _extends({}, this.state.object, {
            onChange: this.onSection,
            onRnv: this.onRnv,
            onClose: this.onDialog,
            onClipboardPaste: this.onClipboardPaste,
            onClipboardCopy: this.onClipboardCopy
          })));

        case "Card":
          value = typeof this.state.value != "undefined" ? this.state.value : [];
          return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
            className: "py-4 dialog-content overflow-y-auto"
          }, /*#__PURE__*/React.createElement(InputForm, _extends({}, value[this.state.object.i], {
            source: this.state.object.landing_type,
            id: this.state.object.i,
            data: value[this.state.object.i],
            card_fields: this.state.object.object.card_fields,
            sourceType: this.state.object.landing_type,
            on: this.onDialogEdit
          }))), /*#__PURE__*/React.createElement("div", {
            className: "d-flex justify-content-center"
          }, /*#__PURE__*/React.createElement(ButtonGroup, {
            className: "p-0",
            fill: true,
            large: true
          }, /*#__PURE__*/React.createElement(Button, {
            icon: "cog",
            intent: Intent.SUCCESS,
            onClick: this.onDialogUpdate,
            className: "flex-grow-100"
          }, __("Edit")), /*#__PURE__*/React.createElement(Button, {
            icon: "cross",
            intent: Intent.DANGER,
            onClick: this.onRemoveElement,
            className: "flex-grow-1"
          }, __("Delete")))));

        case "TestanomialMember":
        case "MotivatonElement":
        default:
          if (Array.isArray(this.state.value) && typeof this.state.value !== "undefined") {
            value = this.state.value; // console.log( this.state )
            // console.log( this.state.object, value, typeof this.state.value )

            return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
              className: "p-4 dialog-content overflow-y-auto"
            }, /*#__PURE__*/React.createElement(InputForm, _extends({}, value[this.state.object.i], {
              source: this.state.object.landing_type,
              id: this.state.object.i,
              data: Array.isArray(value) ? value[this.state.object.i] : value,
              sourceType: this.state.object.landing_type,
              object: this.state.object.object,
              on: this.onDialogEdit,
              onClipboardCopy: this.onClipboardCopy
            }))), /*#__PURE__*/React.createElement("div", {
              className: "d-flex justify-content-center"
            }, /*#__PURE__*/React.createElement(ButtonGroup, {
              className: "p-2"
            }, /*#__PURE__*/React.createElement(Button, {
              icon: "cog",
              intent: Intent.NONE,
              onClick: this.onDialogUpdate
            }, __("Edit")), /*#__PURE__*/React.createElement(Button, {
              icon: "cross",
              intent: Intent.DANGER,
              onClick: this.onRemoveElement
            }, __("Delete")))));
          } else {
            value = this.state.value; //console.log( this.state.object, value, typeof this.state.value )

            return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
              className: "p-4 dialog-content overflow-y-auto"
            }, /*#__PURE__*/React.createElement(InputForm, _extends({}, value, {
              source: this.state.object.landing_type,
              id: this.state.object.i,
              data: value,
              sourceType: this.state.object.landing_type,
              object: this.state.object.object,
              on: this.onDialogEdit
            }))), /*#__PURE__*/React.createElement("div", {
              className: "d-flex justify-content-center"
            }, /*#__PURE__*/React.createElement(ButtonGroup, {
              className: "p-2"
            }, /*#__PURE__*/React.createElement(Button, {
              icon: "cog",
              intent: Intent.NONE,
              onClick: this.onDialogUpdate
            }, __("Edit")))));
          }

      }
    });

    _defineProperty(this, "onDialogEdit", (value, field) => {
      //console.log(value, field)
      let v;

      if (Array.isArray(this.state.value)) {
        v = [...this.state.value];
        const object = v[this.state.object.i]; // console.log( value, field, v );

        object[field] = value;
        v[this.state.object.i] = { ...v[this.state.object.i],
          field: value
        };
        this.setState({
          value: v
        });
      } else {
        v = { ...this.state.value
        };
        v[field] = value;
        this.setState({
          value: v
        });
      }
    });

    _defineProperty(this, "onDialogUpdate", evt => {
      this.on(this.state.field, this.state.value);
    });

    _defineProperty(this, "onRemoveElement", evt => {
      const value = [...this.state.value];
      value.splice(this.state.object.i, 1);
      this.setState({
        value
      });
      this.on(this.state.field, value);
    });

    _defineProperty(this, "onAddArray", evt => {
      // console.log( this.state.value,  this.state.field, this.state.landing_object, this.state );
      const mt = matrix[this.state.landing_object];
      const dflt = mt.default ? mt.default : {};
      let obj = {};
      Object.keys(mt).filter(e => !mt[e].hidden).forEach((e, i) => {
        // console.log( e, mt[e] );
        let el;

        switch (mt[e].type) {
          case "int":
          case "number":
          case "slider":
            el = dflt[e] ? dflt[e] : 0;
            break;

          case "boolean":
            el = dflt[e] ? dflt[e] : false;
            break;

          case "geo":
          case "array":
            el = dflt[e] ? dflt[e] : [];
            break;

          case "checkbox":
          case "string":
          case "media":
          case "color":
          case "url":
          case "text":
          case "date":
            el = typeof dflt[e] !== "undefined" ? dflt[e] : "";

            if (e === "id" && this.state.landing_object === "section") {
              el = DataContext.getMaxSectionID(true);
            }

            break;

          case "radio":
            break;

          default:
          case "landing_object":
            const mtt = matrix[mt[e].landing_object];
            el = dflt[e] ? dflt[e] : {}; // console.log( mtt, e, el, mt[e] );

            if (!mtt || dflt[e]) break;
            Object.keys(mtt).filter(ee => !mtt[ee].hidden).forEach((ee, i) => {
              // console.log( ee, mtt[ee] );
              let ell;

              switch (mtt[ee].type) {
                case "int":
                case "number":
                case "slider":
                  ell = 0;
                  break;

                case "boolean":
                  ell = false;
                  break;

                case "checkbox":
                case "array":
                case "string":
                case "media":
                case "url":
                case "text":
                case "date":
                  ell = "";
                  break;

                case "color":
                  ell = null;
                  break;

                case "radio":
                  break;

                default:
                case "landing_object":
                  // console.log( ee, mtt[ee].landing_object, matrix[ mtt[ee].landing_object ] );
                  ell = mtt[ee].kind === "array" ? [] : {};
                  break;
              }

              el[ee] = ell;
            });
            break;
        }

        obj[e] = el;
      }); //console.log( this.state.landing_object )

      const value = Array.isArray(this.state.value) ? this.state.value : [];

      if (this.state.landing_object === "Card" && value.length > 0) {
        obj = value[value.length - 1];
      }

      if (this.state.landing_object === "section") {
        //obj = getDefault()
        //obj.id = DataContext.getMaxSectionID(true)
        this.onNewSectionOpen();
        return;
      }

      if (this.state.landing_object === "CardField") {
        obj.type = "string"; //console.log(obj)
      }

      value.push({ ...obj
      }); // console.log( value );

      this.setState({
        value
      });
      this.on(this.state.field, value);
    });

    _defineProperty(this, "onMultiInput", evt => {});

    _defineProperty(this, "onMulti", data => {
      // console.log( data );
      const value = [];
      let vv = 0;
      data.forEach((e, i) => {
        if (e !== 0) {
          const v = e - vv;
          vv += v;
          value.push(v);
        }
      }); // value.push(100 - vv);
      // console.log( vv, value );

      this.setState({
        value
      });
      this.on(this.state.field, value);
    });

    _defineProperty(this, "onSwitchButton", evt => {
      const i = parseInt(evt.currentTarget.getAttribute("i"));
      const value = Array.isArray(this.state.value) ? this.state.value : [];
      const next = value.splice(i - 1, 1);
      value.splice(i, 0, next[0]);
      this.setState({
        value
      });
      this.on(this.state.field, value);
    });

    _defineProperty(this, "onCardFieldSwitchButton", evt => {
      const i = parseInt(evt.currentTarget.getAttribute("i")); //

      const section = DataContext.getSection(this.props.section_id);
      const dopol = section.data.cards.map((e, ii) => {
        // console.log( e.fields );
        const card = Array.isArray(e.fields) ? [...e.fields] : [];

        const _next = card.splice(i - 1, 1);

        card.splice(i, 0, _next[0]); // console.log(card)

        return { ...e,
          fields: card
        };
      }); //

      const value = Array.isArray(this.state.value) ? this.state.value : [];
      const next = value.splice(i - 1, 1);
      value.splice(i, 0, next[0]);
      this.setState({
        value
      });
      this.on(this.state.field, value, dopol);
    });

    _defineProperty(this, "on", (data, field, dopol) => {
      //console.log(data, field, dopol);
      this.props.on(field, data, dopol);
      this.setState({
        isDialog: false
      });
    });

    _defineProperty(this, "onDialog", evt => {
      this.setState({
        isDialog: !this.state.isDialog
      });
    });

    _defineProperty(this, "onCFForm", (data, i) => {
      const value = [...this.state.value];
      value[i] = data;
      this.setState({
        value
      }); //console.log(value)

      this.on(this.state.field, value);
    });

    _defineProperty(this, "onCFBClose", eid => {
      const value = [...this.state.value]; // console.log( eid, value );

      value.splice(eid, 1); // this.on( value, this.state.field );
      // console.log( "onCFBClose", value );

      this.setState({
        value
      });
      this.props.on(value, this.state.field);
    });

    _defineProperty(this, "onSubDialog", (object, type, i) => {
      // console.log(object)
      this.setState({
        object,
        isDialog: true,
        current_element: i
      });
    });

    _defineProperty(this, "onCardDialog", (object, type, i) => {
      // console.log( object );
      this.setState({
        object,
        isDialog: true,
        current_element: i
      });
    });

    _defineProperty(this, "onClipboardCopy", data => {
      if (this.props.onClipboardCopy) {
        console.log("LandingObject.onClipboardCopy", data);
        this.props.onClipboardCopy(data);
      }
    });

    _defineProperty(this, "onClipboardPaste", data => {
      if (this.props.onClipboardPaste) {
        console.log("LandingObject.onClipboardPaste");
        this.props.onClipboardPaste(data);
      }
    });

    _defineProperty(this, "onSection", (type, data) => {
      console.log("on Section Edit", type, data);
      let value = [...this.state.value]; // console.log( value );

      value = Array.isArray(value) ? value : [];
      value.forEach((e, i) => {
        console.log(e.id, data.id);

        if (e.id === data.id) {
          const dt = { ...data,
            type
          };
          delete dt.current_type;
          delete dt.is_change_type_enbl;
          delete dt.navbarTabId;
          delete dt.tab;
          delete dt.onChange;
          value[i] = dt;
        }
      });

      if (value.length === 0) {
        value[0] = { ...data,
          type
        };
      } //console.log("on Section Edit", value)


      this.props.on(value);
      this.setState({
        isDialog: false
      });
    });

    _defineProperty(this, "onRnv", id => {
      let value = [...this.state.value]; // console.log( value );

      value = Array.isArray(value) ? value : [];
      value.forEach((e, i) => {
        // console.log( e.id, data.id );
        if (e.id === id) {
          value.splice(i, 1);
        }
      }); //console.log("on Section Delete", value)

      this.props.on(value);
      this.setState({
        isDialog: false
      });
    });

    _defineProperty(this, "onStyle", val => {
      this.setState({
        value: val
      });
      this.props.on(val, this.state.field);
    });

    _defineProperty(this, "onCoordinate", (evt, field) => {
      const value = { ...this.state.value
      };
      value[field] = evt.currentTarget.value; // console.log(value);

      this.setState({
        value
      });
      this.props.on(value);
    });

    _defineProperty(this, "onComposition", (value, field) => {// let value = { ...this.state.value };
      // value[field] = evt.currentTarget.value;
      //console.log(value, field)
      // this.setState( { value });
      // this.props.on(value);
    });

    _defineProperty(this, "onPlaceType", (evt, field) => {
      this.setState({
        value: evt.currentTarget.value
      });
      this.props.on(evt.currentTarget.value, field);
    });

    _defineProperty(this, "onVideoGroup", (evt, field) => {
      this.props.on(evt.currentTarget.value, field);
    });

    _defineProperty(this, "onPaletteForm", value => {
      this.props.on(value, this.props.field);
    });

    _defineProperty(this, "onNewSectionOpen", () => {
      this.setState({
        isNewSectionOpen: !this.state.isNewSectionOpen
      });
    });

    _defineProperty(this, "onAddSection", (data, type) => {
      const value = Array.isArray(this.state.value) ? this.state.value : [];
      let sec;

      if (matrix[type].default) {
        const data1 = {
          data: { ...matrix[type].default
          },
          composition: {
            columns: 1,
            is_blocked: 0,
            is_expand: 0,
            is_vertical_center: 0
          }
        };
        delete data1.data.hidden;
        delete data1.data.composition;
        sec = getDefault(type, data1);
      } else {
        sec = getDefault(type, {
          composition: {
            columns: 1,
            is_blocked: 0,
            is_expand: 0,
            is_vertical_center: 0
          },
          data: {}
        });
      }

      value.push({ ...sec
      });
      this.setState({
        value,
        isNewSectionOpen: false
      });
      this.on(this.state.field, value);
    });

    _defineProperty(this, "onClipboardCardFieldCopy", () => {
      const data = {
        data: {
          back_image_vertical: this.state.origin.data.back_image_vertical,
          box_shadow: [...this.state.origin.data.box_shadow],
          card_inner_style: { ...this.state.origin.data.card_inner_style
          },
          cards: [...this.state.origin.data.cards],
          cliping: this.state.origin.data.cliping,
          color: this.state.origin.data.color,
          decoration: this.state.origin.data.decoration,
          fields: [...this.state.origin.data.fields],
          is_contrast_muar: this.state.origin.data.is_contrast_muar,
          padding: this.state.origin.data.padding,
          style: { ...this.state.origin.data.style
          },
          tension: this.state.origin.data.tension,
          title: this.state.origin.data.title,
          vertical_align: this.state.origin.data.vertical_align
        },
        current_template_id: this.state.origin.current_template_id
      }; //console.log(data)

      const copy = JSON.stringify(data); //console.log(copy) 

      $("body").append(`<div style='position:absolute; z-index:-100; width:100%; top:0; left:0;'><textarea style='width:100%;' id='StyleClipboard'>${copy}</textarea></div>`);
      const copyText = document.getElementById("StyleClipboard");
      copyText.select();
      copyText.setSelectionRange(0, 99999999999999999999);
      document.execCommand("copy");
      $("#StyleClipboard").remove();
      AppToaster.show({
        intent: Intent.SUCCESS,
        icon: "tick",
        duration: 10000,
        message: __("Section copy to clipbord")
      });
    });

    _defineProperty(this, "onClipboardConstructordPaste", () => {
      navigator.clipboard.readText().then(clipText => {
        try {
          const clip = JSON.parse(clipText);
          console.log(clip, this.props.onClipboardPaste);
          this.setState({
            value: clip.data.fields
          });
          this.on(this.state.field, clip.data.fields); // if(this.props.onClipboardPaste)
          // {
          // 	this.props.onClipboardPaste(clip)
          // }
        } catch (e) {
          AppToaster.show({
            intent: Intent.DANGER,
            icon: "tick",
            duration: 10000,
            message: __("Error read clipboard data")
          });
        }
      });
    });

    _defineProperty(this, "onClipboardCardFieldPaste", () => {
      navigator.clipboard.readText().then(clipText => {
        try {
          const clip = JSON.parse(clipText);
          console.log(clip, this.props.onClipboardPaste);
          this.setState({
            value: clip.data.fields
          });
          this.on(this.state.field, clip.data.fields);

          if (this.props.onClipboardPaste) {
            this.props.onClipboardPaste(clip);
          }
        } catch (e) {
          AppToaster.show({
            intent: Intent.DANGER,
            icon: "tick",
            duration: 10000,
            message: __("Error read clipboard data")
          });
        }
      });
    });

    _defineProperty(this, "onClipboardCardContentCopy", () => {
      // const value = Array.isArray(this.state.value)
      // 	? this.state.value
      // 	: []
      console.log(this.state.origin);
      const copy = JSON.stringify(this.state.origin); //console.log(copy) 

      $("body").append(`<div style='position:absolute; z-index:-100; width:100%; top:0; left:0;'><textarea style='width:100%;' id='StyleClipboard'>${copy}</textarea></div>`);
      const copyText = document.getElementById("StyleClipboard");
      copyText.select();
      copyText.setSelectionRange(0, 99999999999999999999);
      document.execCommand("copy");
      $("#StyleClipboard").remove();
      AppToaster.show({
        intent: Intent.SUCCESS,
        icon: "tick",
        duration: 10000,
        message: __("Card content copy to clipbord")
      });
    });

    _defineProperty(this, "onClipboardCardContentPaste", () => {
      navigator.clipboard.readText().then(clipText => {
        try {
          const clip = JSON.parse(clipText);
          console.log(clip);
          this.setState({
            value: clip.fields
          });
          this.on(this.state.field, clip.fields); // 
          // if(this.props.onClipboardPaste)
          // {
          // 	this.props.onClipboardPaste(clip)
          // }
        } catch (e) {
          AppToaster.show({
            intent: Intent.DANGER,
            icon: "tick",
            duration: 10000,
            message: __("Error read clipboard data")
          });
        }
      });
    });

    _defineProperty(this, "onSubMenu", (val, field, title, prefix) => {
      console.log(val, field, title, prefix);
      let state = { ...this.state
      };
      state[field] = val;
      console.log(state);
      this.setState(state);
      this.on(field, val);
    });
  }

  render() {
    // console.log( this.props );
    const {
      field,
      title,
      description
    } = this.props;
    const col1 = title || description ? this.props.vertical ? "col-12 layout-label-vert" : "col-md-3  layout-label" : " hidden ";
    const col2 = title || description ? this.props.vertical ? "col-12 layout-data-vert" : "col-md-9 layout-data" : " col-12 layout-data-vert ";
    const o = this.props.type === "landing_object" ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "row dat strob01",
      row_data: field
    }, /*#__PURE__*/React.createElement("div", {
      className: col1
    }, __(this.props.title)), /*#__PURE__*/React.createElement("div", {
      className: col2,
      style: {
        position: "relative"
      }
    }, this.formInput())), /*#__PURE__*/React.createElement(NewSectionDialog, {
      title: __("Add new sub Section"),
      isOpen: this.state.isNewSectionOpen,
      onClose: this.onNewSectionOpen,
      onEdit: this.onAddSection
    })) : null;
    return /*#__PURE__*/React.createElement(React.Fragment, null, o, /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isDialog,
      onClose: this.onDialog,
      title: __("Parameters"),
      className: "little2"
    }, this.dialogContent()));
  }

  formInput() {
    const {
      value
    } = this.state;
    let groups,
        btns = "",
        after = ""; // console.log( value, this.props );
    // console.log( this.props.kind );

    switch (this.props.kind) {
      case "array":
        switch (this.props.landing_object) {
          case "google_font":
            return /*#__PURE__*/React.createElement(GoogleFonts, _extends({}, this.state, {
              on: val => this.on(this.state.field, val)
            }));

          case "__palette":
            return /*#__PURE__*/React.createElement(PaletteForm, {
              value: value,
              on: this.onPaletteForm
            });

          case "ColumnsProportia":
            // console.log( Array.isArray( value ) );
            // console.log(this.state);
            let _value = value;

            if (!Array.isArray(_value)) {
              _value = [];
              const columnsC = this.state.origin && this.state.origin.composition ? this.state.origin.composition.columns : 1;

              for (let __i = 0; __i < columnsC; __i++) {
                _value.push(100 / columnsC);
              }
            }

            let pr = 0;
            const fields = [];

            const handles = _value.map((e, i) => {
              if (i === _value.length - 1) return null;
              const m = /*#__PURE__*/React.createElement(MultiSlider.Handle, {
                key: i,
                value: pr + e,
                type: "end2",
                intentAfter: Intent.PRIMARY
              });
              pr += e;
              fields.push( /*#__PURE__*/React.createElement("input", {
                type: "number",
                key: i,
                className: "input dark m-1 py-2 w_45",
                value: parseInt(e),
                onChange: this.onMultiInput
              }));
              return m;
            });

            return /*#__PURE__*/React.createElement("div", {
              className: "w-100"
            }, /*#__PURE__*/React.createElement(MultiSlider, {
              min: 0,
              max: 100,
              stepSize: 1,
              labelStepSize: 10,
              onChange: this.onMulti,
              vertical: false
            }, /*#__PURE__*/React.createElement(MultiSlider.Handle, {
              value: 0,
              type: "start",
              intentAfter: Intent.DANGER
            }), handles, /*#__PURE__*/React.createElement(MultiSlider.Handle, {
              value: 100,
              type: "end",
              intentAfter: Intent.PRIMARY
            })), /*#__PURE__*/React.createElement("div", {
              className: "d-flex"
            }, fields));

          case "ProportiaInt":
            return /*#__PURE__*/React.createElement("div", {
              className: "w-100"
            }, /*#__PURE__*/React.createElement("div", {
              className: "d-flex justify-content-between"
            }, /*#__PURE__*/React.createElement(Tag, null, " ", __("Title"), " "), /*#__PURE__*/React.createElement(Tag, null, " ", __("Content"), " ")), /*#__PURE__*/React.createElement(Slider, {
              min: 0,
              max: 100,
              stepSize: 1,
              labelStepSize: 1,
              onChange: this.onSlider,
              value: Array.isArray(value) ? value[0] : value,
              vertical: false
            }));

          case "card_template":
            return /*#__PURE__*/React.createElement("div", {
              className: "p-4 w-100"
            }, /*#__PURE__*/React.createElement(CardTemplateLib, _extends({}, this.props, {
              value: value,
              on: val => this.on(this.state.field, val),
              onApply: this.props.onApply,
              onTry: this.props.onTry,
              onSingleRemove: this.props.onSingleRemove,
              onUpdate: this.props.onUpdate
            })));

          case "cards":
            after = "<div className='right'>A</div>";
            break;

          default:
            let btnContClasses = "d-flex flex-wrap w-100 my-3";
            btns = Array.isArray(value) ? value.map((e, i) => {
              let label;

              if (this.state.visibled_value) {
                const tt = getVisibleValue(e, this.state.visibled_value); // console.log(e, this.state, tt);

                label = tt && tt.length > 0 ? tt : i;
              } else {
                label = i;
              }

              switch (this.props.landing_object) {
                case "section":
                  return /*#__PURE__*/React.createElement("div", {
                    key: i,
                    className: "p-0 d-flex flex-wrap"
                  }, i === 0 // || i == value.length - 1
                  ? null : /*#__PURE__*/React.createElement(Button, {
                    i: i,
                    eid: e.id,
                    onClick: this.onSwitchButton,
                    className: "btn-switch"
                  }, /*#__PURE__*/React.createElement("i", {
                    className: "fas fa-exchange-alt"
                  })), /*#__PURE__*/React.createElement(SectionButton, {
                    key: i,
                    eid: e.id,
                    object: e,
                    onClick: object => this.onSubDialog({ ...object,
                      landing_type: this.props.landing_object
                    })
                  }));

                case "ContanctFormField":
                  console.log(this.state);
                  return /*#__PURE__*/React.createElement("div", {
                    key: i,
                    className: "w-50 "
                  }, i === 0 // || i == value.length - 1
                  ? null : /*#__PURE__*/React.createElement(Button, {
                    i: i,
                    eid: e.id,
                    onClick: this.onSwitchButton,
                    className: "btn-switch"
                  }, /*#__PURE__*/React.createElement("i", {
                    className: "fas fa-exchange-alt"
                  })), /*#__PURE__*/React.createElement(ContanctFormFieldButton, {
                    key: i,
                    i: i,
                    object: e,
                    onClose: this.onCFBClose,
                    onChange: this.onCFForm
                  }));

                case "Card":
                  // console.log( this.props.origin.data.fields );
                  // console.log(e, i);
                  return /*#__PURE__*/React.createElement("div", {
                    key: i,
                    className: "p-0 d-flex flex-wrap"
                  }, i === 0 // || i === value.length - 1
                  ? null : /*#__PURE__*/React.createElement(Button, {
                    i: i,
                    eid: e.id,
                    onClick: this.onSwitchButton,
                    className: "btn-switch"
                  }, /*#__PURE__*/React.createElement("i", {
                    className: "fas fa-exchange-alt"
                  })), /*#__PURE__*/React.createElement(CardButton, {
                    key: i,
                    i: i,
                    object: { ...e,
                      card_fields: this.props.origin.data.fields,
                      fields: e.fields.map((fld, index) => ({ ...fld,
                        card_fields: this.props.origin.data.fields
                      }))
                    },
                    onClick: object => this.onCardDialog({
                      object,
                      landing_type: this.props.landing_object,
                      i
                    })
                  }));

                case "CardField":
                  //console.log(this.state)
                  after = /*#__PURE__*/React.createElement("div", {
                    className: "m-4 d-flex"
                  }, /*#__PURE__*/React.createElement("div", {
                    className: "ml-auto"
                  }, /*#__PURE__*/React.createElement(ButtonGroup, {
                    minimal: true
                  }, /*#__PURE__*/React.createElement(Button, {
                    minimal: true,
                    className: "hint hint--top",
                    "data-hint": __("Copy cards data"),
                    onClick: this.onClipboardCardFieldCopy
                  }, /*#__PURE__*/React.createElement("i", {
                    className: "fas fa-file-import mr-2"
                  })), /*#__PURE__*/React.createElement(Button, {
                    minimal: true,
                    className: "hint hint--top",
                    "data-hint": __("Paste all Cards"),
                    onClick: this.onClipboardCardFieldPaste
                  }, /*#__PURE__*/React.createElement("i", {
                    className: "fas fa-file-export mr-2"
                  })), /*#__PURE__*/React.createElement(Button, {
                    minimal: true,
                    className: "hint hint--top",
                    "data-hint": __("Paste constructor only"),
                    onClick: this.onClipboardConstructordPaste
                  }, /*#__PURE__*/React.createElement("i", {
                    className: "fas fa-file-export mr-2"
                  })))));
                  return /*#__PURE__*/React.createElement("div", {
                    key: i,
                    className: "p-0 d-flex flex-wrap position-relative w-100"
                  }, i === 0 // || i === value.length - 1
                  ? null : /*#__PURE__*/React.createElement(Button, {
                    i: i,
                    key: `btn${i}`,
                    eid: e.id,
                    onClick: this.onCardFieldSwitchButton,
                    className: "btn-card-fields-switch"
                  }, /*#__PURE__*/React.createElement("i", {
                    className: "fas fa-exchange-alt"
                  })), /*#__PURE__*/React.createElement(CardField, {
                    origin: this.state.origin,
                    key: i,
                    i: i,
                    object: e,
                    onClose: this.onCFBClose,
                    onChange: this.onCFForm
                  }));

                case "video":
                  return /*#__PURE__*/React.createElement("div", {
                    key: i,
                    className: "p-0 position-relative"
                  }, i === 0 ? null : /*#__PURE__*/React.createElement(Button, {
                    i: i,
                    key: `btn${i}`,
                    eid: e.id,
                    onClick: this.onSwitchButton,
                    className: "bp3-button btn-switch"
                  }, /*#__PURE__*/React.createElement("i", {
                    className: "fas fa-exchange-alt"
                  })), /*#__PURE__*/React.createElement(VideoButton, {
                    key: i,
                    i: i,
                    eid: e.id,
                    object: e,
                    onClick: object => this.onSubDialog({
                      object,
                      landing_type: this.props.landing_object,
                      i
                    })
                  }));

                case "place_type":
                  //console.log(e)
                  return /*#__PURE__*/React.createElement(PlaceTypeButton, {
                    key: i,
                    i: i,
                    eid: e._id,
                    object: e,
                    onClick: object => this.onSubDialog({
                      object,
                      landing_type: this.props.landing_object,
                      i
                    })
                  });

                case "YandexMapPlace":
                  return /*#__PURE__*/React.createElement(YandexMapPlaceBottom, {
                    key: i,
                    i: i,
                    eid: e._id,
                    object: e,
                    onClick: object => this.onSubDialog({
                      object,
                      landing_type: this.props.landing_object,
                      i
                    })
                  });

                case "CardSingleField":
                  btnContClasses = " d-flex flex-column w-100 my-3"; // console.log( this.props.origin  )

                  after = /*#__PURE__*/React.createElement("div", {
                    className: "m-4 d-flex"
                  }, /*#__PURE__*/React.createElement("div", {
                    className: "ml-auto"
                  }, /*#__PURE__*/React.createElement(ButtonGroup, {
                    minimal: true
                  }, /*#__PURE__*/React.createElement(Button, {
                    minimal: true,
                    className: "hint hint--top",
                    "data-hint": __("Copy Card content"),
                    onClick: this.onClipboardCardContentCopy
                  }, /*#__PURE__*/React.createElement("i", {
                    className: "fas fa-file-import mr-2"
                  }), __("Copy Card content")), /*#__PURE__*/React.createElement(Button, {
                    minimal: true,
                    className: "hint hint--top",
                    "data-hint": __("Paste Card content"),
                    onClick: this.onClipboardCardContentPaste
                  }, /*#__PURE__*/React.createElement("i", {
                    className: "fas fa-file-export mr-2"
                  }), __("Paste Card content")))));
                  return /*#__PURE__*/React.createElement("div", {
                    key: i,
                    className: "p-0 d-flex flex-wrap"
                  }, i === 0 ? null : /*#__PURE__*/React.createElement(Button, {
                    i: i,
                    eid: e.id,
                    onClick: this.onSwitchButton,
                    className: "btn-switch btn-switch-vert"
                  }, /*#__PURE__*/React.createElement("i", {
                    className: "fas fa-arrows-alt-v"
                  })), /*#__PURE__*/React.createElement(CardSingleField, {
                    key: i,
                    object: { ...e,
                      i,
                      card_fields: this.props.origin.card_fields
                    },
                    on: this.on,
                    onClick: object => this.onSubDialog({
                      object,
                      landing_type: this.props.landing_object,
                      i
                    })
                  }));

                case "MotivatonElement":
                default:
                  return /*#__PURE__*/React.createElement("div", {
                    key: i,
                    className: "p-0 d-flex flex-wrap"
                  }, i === 0 // || i === value.length - 1
                  ? null : /*#__PURE__*/React.createElement(Button, {
                    i: i,
                    eid: e.id,
                    onClick: this.onSwitchButton,
                    className: "btn-switch"
                  }, /*#__PURE__*/React.createElement("i", {
                    className: "fas fa-exchange-alt"
                  })), /*#__PURE__*/React.createElement(Button, {
                    i: i,
                    eid: e.id,
                    onClick: evt => this.onSubDialog({ ...e,
                      landing_type: this.props.landing_object,
                      i: parseInt(evt.currentTarget.getAttribute("i"))
                    }),
                    className: "edit-button"
                  }, label));
              }
            }) : null;
            return /*#__PURE__*/React.createElement("div", {
              className: "w-100"
            }, /*#__PURE__*/React.createElement("div", {
              className: btnContClasses
            }, btns, /*#__PURE__*/React.createElement(Button, {
              icon: "plus",
              className: "  ",
              onClick: this.onAddArray
            })), after, /*#__PURE__*/React.createElement("div", {
              className: "right"
            }, /*#__PURE__*/React.createElement(SubMenu, {
              sub_menus: this.props.sub_menus,
              on: this.onSubMenu
            })));
        }

      case "mask_src":
        return /*#__PURE__*/React.createElement(SVGLabrary, _extends({}, this.state, {
          on: this.on
        }));

      default:
        switch (this.props.landing_object) {
          case "CardFieldMetaphors":
            return /*#__PURE__*/React.createElement("div", {
              className: "py-4 w-100"
            }, /*#__PURE__*/React.createElement(CardFieldMetaphorEdit, _extends({}, this.props, {
              value: value,
              on: val => this.on(this.state.field, val)
            })));

          case "ExternalLandingColor":
            // console.log(this.props );
            return /*#__PURE__*/React.createElement(ExternalLandingColor, _extends({}, this.state, {
              origin: this.props.origin
            }));

          case "LandingParameters":
            return /*#__PURE__*/React.createElement(LandingParameters, {
              onAdd: this.props.onAdd,
              onDownload: this.props.onDownload,
              onLoadChange: this.props.onLoadChange,
              onClearOpen: this.props.onClearOpen
            });

          case "css_color":
            return /*#__PURE__*/React.createElement(CSSColor, _extends({}, this.state, {
              on: this.on
            }));

          case "text_format":
            return /*#__PURE__*/React.createElement(TextFormatForm, _extends({}, this.state, {
              example: __(this.props.example),
              on: this.on
            }));

          case "Object_Position":
            return /*#__PURE__*/React.createElement(Object_Position, _extends({}, this.state, {
              example: __(this.props.example),
              on: this.on
            }));
          // 	=====================================================
          //	поле редактирования параметров единичной секции Карточки (Card).
          // 	Вызывается - в CardSingleField
          //	Параметр - значение секции (типы, варианты и данные)
          // 	=====================================================

          case "CardSingleFieldType":
            return /*#__PURE__*/React.createElement(CardSingleFieldType, _extends({}, this.state, {
              on: this.on
            }));

          case "place_type":
            groups = Array.isArray(DataContext.data.landing.place_type) ? /*#__PURE__*/React.createElement("div", {
              className: "my-1"
            }, DataContext.data.landing.place_type.map((e, i) => /*#__PURE__*/React.createElement("div", {
              className: "my-1"
            }, /*#__PURE__*/React.createElement("label", {
              className: "_check_",
              key: i
            }, /*#__PURE__*/React.createElement("input", {
              type: "radio",
              value: e._id,
              onChange: evt => this.onPlaceType(evt, this.state.field),
              name: `place_type${this.state.field}`,
              checked: e._id === value
            }), e.title)))) : /*#__PURE__*/React.createElement(Callout, {
              title: __("There are no one Video Group")
            }, __("Go to Landing global settings to charter «Globals»"));
            return /*#__PURE__*/React.createElement("div", {
              className: "dd-video-group w-100"
            }, groups);

          case "video_group":
            groups = Array.isArray(DataContext.data.landing.video_group) ? /*#__PURE__*/React.createElement("select", {
              onChange: evt => this.onVideoGroup(evt, this.state.field),
              className: "form-control my-2",
              value: this.state.value
            }, /*#__PURE__*/React.createElement("option", {
              value: ""
            }, " "), DataContext.data.landing.video_group.map((e, i) => /*#__PURE__*/React.createElement("option", {
              value: e.unique,
              key: i
            }, e.title))) : /*#__PURE__*/React.createElement(Callout, {
              title: __("There are no one Video Group")
            }, __("Go to Landing global settings to charter «Globals»"));
            return /*#__PURE__*/React.createElement("div", {
              className: "dd-video-group w-100"
            }, groups);

          case "Coordinate":
            // console.log( this.state.field,  this.state );
            let dst;

            switch (this.state.field) {
              case "x":
                dst = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("label", {
                  className: "_check_ mr-2 w_80",
                  htmlFor: `dst_x1_${this.state.id}`
                }, /*#__PURE__*/React.createElement("input", {
                  type: "radio",
                  name: `dst_x_${this.state.id}`,
                  id: `dst_x1_${this.state.id}`,
                  checked: this.state.value.dst === "L",
                  value: "L",
                  onClick: evt => this.onCoordinate(evt, "dst")
                }), __("from left")), /*#__PURE__*/React.createElement("label", {
                  className: "_check_ mr-2 w_80",
                  htmlFor: `dst_x2_${this.state.id}`
                }, /*#__PURE__*/React.createElement("input", {
                  type: "radio",
                  name: `dst_x_${this.state.id}`,
                  id: `dst_x2_${this.state.id}`,
                  checked: this.state.value.dst === "R",
                  value: "R",
                  onClick: evt => this.onCoordinate(evt, "dst")
                }), __("from right")));
                break;

              case "y":
                dst = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("label", {
                  className: "_check_ mr-2 w_80",
                  htmlFor: `dst_yt_${this.state.id}`
                }, /*#__PURE__*/React.createElement("input", {
                  type: "radio",
                  name: `dst_y_${this.state.id}`,
                  id: `dst_yt_${this.state.id}`,
                  className: "",
                  checked: this.state.value.dst === "T",
                  value: "T",
                  onClick: evt => this.onCoordinate(evt, "dst")
                }), __("from top")), /*#__PURE__*/React.createElement("label", {
                  className: "_check_ mr-2 w_80",
                  htmlFor: `dst_yb_${this.state.id}`
                }, /*#__PURE__*/React.createElement("input", {
                  type: "radio",
                  name: `dst_y_${this.state.id}`,
                  id: `dst_yb_${this.state.id}`,
                  className: "",
                  checked: this.state.value.dst === "B",
                  value: "B",
                  onClick: evt => this.onCoordinate(evt, "dst")
                }), __("from bottom")));
                break;

              default:
                break;
            }

            return /*#__PURE__*/React.createElement("div", {
              className: "d-flex align-items-center"
            }, /*#__PURE__*/React.createElement("input", {
              type: "number",
              step: ".2",
              value: this.state.value.value,
              field: "value",
              className: "input dark m-1 text-right w_80",
              onChange: evt => this.onCoordinate(evt, "value")
            }), /*#__PURE__*/React.createElement("select", {
              className: "input dark m-1 mr-2",
              value: this.state.value.ei,
              onChange: evt => this.onCoordinate(evt, "ei")
            }, /*#__PURE__*/React.createElement("option", {
              value: "px"
            }, "px"), /*#__PURE__*/React.createElement("option", {
              value: "%"
            }, "%")), dst);

          case "ColumnsCount":
            return /*#__PURE__*/React.createElement(Columns, _extends({}, this.state, {
              on: this.on
            }));

          case "Padding":
            return /*#__PURE__*/React.createElement(Padding, _extends({}, this.state, {
              on: this.on
            }));

          case "Margin":
            return /*#__PURE__*/React.createElement(Margin, _extends({}, this.state, {
              on: this.on
            }));

          case "Border":
            return /*#__PURE__*/React.createElement(Border, _extends({}, this.state, {
              on: this.on
            }));

          case "Style":
            // console.log( this.state );
            return /*#__PURE__*/React.createElement(Style, _extends({}, this.state, {
              on: this.onStyle
            }));

          case "SchemaType":
            return getTypeSelector({
              onChange: evt => this.on(this.props.field, evt.currentTarget.value),
              className: " input dark ",
              selected: this.state.value
            });

          case "Composition":
            return /*#__PURE__*/React.createElement(Composition, _extends({}, this.state, {
              on: this.onComposition
            }));

          case "lasy_load_type":
            return /*#__PURE__*/React.createElement(LasyLoadType, _extends({}, this.state, {
              on: this.on
            }));

          case "Columns":
            return getTypeSelector({
              onChange: evt => this.on(this.props.field, evt.currentTarget.value),
              className: " input dark ",
              selected: this.state.value
            });

          /*
          	  case "contact_form":
          		  console.log(this.state, value);
          		  return "contact_form";
          	  */

          case "Section":
          default:
            let label; //console.log(this.state, value);

            if (this.state.visibled_value) {
              const tt = getVisibleValue(value, this.state.visibled_value);
              label = tt && tt.length > 0 ? tt : this.props.landing_object;
            } else {
              label = this.props.landing_object;
            }

            if (label.length > 30) {
              label = label.substring(0, 30) + "...";
            }

            return /*#__PURE__*/React.createElement("div", {
              className: "square2 bg-secondary text-light mr-1 my-1 btn-item",
              onClick: evt => this.onSubDialog({ ...value,
                landing_type: this.props.landing_object,
                i: 0
              })
            }, label);
        }

    }
  }

}

export default LandingObject;
export function getStyle(style) {}