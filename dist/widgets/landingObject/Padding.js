function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";

class Padding extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onTop", () => {});

    _defineProperty(this, "onLeft", () => {});

    _defineProperty(this, "onRight", () => {});

    _defineProperty(this, "onBottom", () => {});

    this.state = {
      paddingTop: parseInt(this.props.value.paddingTop),
      paddingRight: parseInt(this.props.value.paddingRight),
      paddingBottom: parseInt(this.props.value.paddingBottom),
      paddingLeft: parseInt(this.props.value.paddingLeft)
    };
  }

  render() {
    const {
      paddingTop,
      paddingRight,
      paddingBottom,
      paddingLeft
    } = this.state;
    console.log(this.props);
    return /*#__PURE__*/React.createElement("div", {
      className: "d-flex flex-column mb-3"
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex w-100"
    }, /*#__PURE__*/React.createElement("div", {
      className: " w-100"
    }), /*#__PURE__*/React.createElement("div", {
      className: "d-flex justify-content-center w-100"
    }, /*#__PURE__*/React.createElement("div", null, "top:"), /*#__PURE__*/React.createElement("input", {
      type: "number",
      className: "input dark w_45 ml-2 p-0",
      value: paddingTop,
      onChange: this.onTop
    })), /*#__PURE__*/React.createElement("div", {
      className: " w-100"
    })), /*#__PURE__*/React.createElement("div", {
      className: "d-flex w-100"
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex align-items-end justify-content-center flex-column w-100"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text-right"
    }, "left:"), /*#__PURE__*/React.createElement("input", {
      type: "number",
      className: "input dark w_45 p-0",
      value: paddingLeft,
      onChange: this.onLeft
    })), /*#__PURE__*/React.createElement("div", {
      className: "landing-padding-icon "
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-align-justify"
    })), /*#__PURE__*/React.createElement("div", {
      className: "d-flex align-items-start  justify-content-center flex-column w-100"
    }, /*#__PURE__*/React.createElement("div", null, "right:"), /*#__PURE__*/React.createElement("input", {
      type: "number",
      className: "input dark w_45 p-0",
      value: paddingRight,
      onChange: this.onRight
    }))), /*#__PURE__*/React.createElement("div", {
      className: "d-flex w-100"
    }, /*#__PURE__*/React.createElement("div", {
      className: " w-100"
    }), /*#__PURE__*/React.createElement("div", {
      className: "d-flex justify-content-center w-100"
    }, /*#__PURE__*/React.createElement("div", null, "bottom:"), /*#__PURE__*/React.createElement("input", {
      type: "number",
      className: "input dark w_45 mr-2 p-0",
      value: paddingBottom,
      onChange: this.onBottom
    })), /*#__PURE__*/React.createElement("div", {
      className: " w-100"
    })));
  }

}

export default Padding;