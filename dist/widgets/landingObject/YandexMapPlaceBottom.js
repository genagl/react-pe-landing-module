function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";

class YandexMapPlaceBottom extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onClick", () => {
      this.props.onClick(this.props.object);
    });

    this.state = {
      object: this.props.object
    };
  }

  render() {
    // console.log( this.props );
    const style = {
      height: 120,
      width: 120
    };
    return /*#__PURE__*/React.createElement("div", {
      className: "square2 bg-secondary mr-1 btn-item my-1",
      style: style,
      onClick: this.onClick
    }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      className: "small text-white "
    }, this.state.object.title)));
  }

}

export default YandexMapPlaceBottom;