import { Button, ButtonGroup } from "@blueprintjs/core";
import React, { useState } from "react";
import { Icon } from "react-pe-scalars";
import { __ } from "react-pe-utilities";
import { MediaChooser } from "react-pe-useful";
import { CARD_SEPARATOR } from "../../views/LandingState/Card";
import { getCurrencies } from "../../views/LandingState/card/CardFieldPrice";

const CardSingleAllFields = props => {
  const [value, setValue] = useState(props.value && Array.isArray(props.value) ? props.value : []);

  const on = evt => {
    const __field = evt.currentTarget.value;
    console.log(__field);
  };

  const btn = (field, i) => {
    const fieldData = props.origin.card_fields[i];
    const __field = field.field;
    if (!fieldData) return ""; //console.log(fieldData)
    //console.log(__field)

    switch (fieldData.type) {
      case "check":
        return check(__field, on, fieldData);

      case "icon":
        return icon(__field, on, fieldData);

      case "cf":
      case "navlink":
      case "outerlink":
        return outerlink(__field, on, fieldData);

      case "media":
        return media(__field, on, fieldData);

      case "personal_links":
        return personal_links(__field, on, fieldData);

      case "price":
        return price(__field, on, fieldData);

      case "section":
        return section(__field, on, fieldData);

      case "empty":
        return "";

      case "divider":
        return "";

      case "string":
      default:
        return string(__field, on, fieldData);
    }
  };

  const fields = props.origin.card_fields.map((field, i) => {
    return ["empty", "divider"].filter(e => e === field.type).length > 0 ? null : /*#__PURE__*/React.createElement("div", {
      className: "row mt-3",
      key: i
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-12 title"
    }, field.metafor, ":"), /*#__PURE__*/React.createElement("div", {
      className: "col-12"
    }, btn(value[i], i)));
  });
  return /*#__PURE__*/React.createElement("div", {
    className: "w-100"
  }, fields);
};

export default CardSingleAllFields;

const string = (__field, on, fieldData) => {
  return /*#__PURE__*/React.createElement("div", {
    className: "w-100 card p-3"
  }, /*#__PURE__*/React.createElement("input", {
    type: "text",
    className: "dark input form-control",
    value: __field,
    onChange: on
  }));
};

const check = (__field, on, fieldData) => {
  let datas = [];

  try {
    datas = JSON.parse(__field.replaceAll("!~!~", '"'));
    datas = Array.isArray(datas) ? datas : [];
  } catch (e) {
    datas = [];
  }

  return /*#__PURE__*/React.createElement("div", {
    className: "w-100 card p-3"
  }, datas.map((dt, index) => {
    return /*#__PURE__*/React.createElement("div", {
      className: "w-100 d-flex",
      key: index
    }, /*#__PURE__*/React.createElement("label", {
      className: "_check_"
    }, /*#__PURE__*/React.createElement("input", {
      type: "checkbox",
      className: " ",
      checked: dt.icon || dt.icon == 1 ? true : false,
      onChange: evt => on(evt, "icon", index)
    })), /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "dark input form-control",
      value: dt.label ? dt.label : "",
      onChange: evt => on(evt, "label", index)
    }), /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      icon: "minus",
      onClick: evt => on(evt, "remove_check", index),
      minimal: true
    }));
  }), /*#__PURE__*/React.createElement(Button, {
    minimal: true,
    icon: "plus",
    className: "mt-2",
    onClick: evt => on(evt, "add_check")
  }, __("add  feature")));
};

const icon = (__field, on, fieldData) => {
  console.log(fieldData);
  const datas = __field ? __field.split(CARD_SEPARATOR()) : ["", 40];
  return /*#__PURE__*/React.createElement("div", {
    className: "w-100 card p-3"
  }, /*#__PURE__*/React.createElement(Icon, {
    editable: true,
    value: datas[0],
    on: data => on(data, 0)
  }));
};

const outerlink = (__field, on, fieldData) => {
  // const { card_fields, i } = this.props.origin.object
  const datas = __field ? __field.split(CARD_SEPARATOR()) : ["", ""]; // console.log( datas );

  return /*#__PURE__*/React.createElement("div", {
    className: "w-100 card p-3"
  }, /*#__PURE__*/React.createElement("div", {
    className: "row dat "
  }, /*#__PURE__*/React.createElement("div", {
    className: "col-4 layout-label"
  }, __("Button's outer URL")), /*#__PURE__*/React.createElement("div", {
    className: "col-8 layout-data"
  }, /*#__PURE__*/React.createElement("input", {
    type: "text",
    className: "dark input form-control",
    value: datas[0],
    onChange: evt => on(evt, 0)
  })), /*#__PURE__*/React.createElement("div", {
    className: "col-4 layout-label"
  }, __("Button label")), /*#__PURE__*/React.createElement("div", {
    className: "col-8 layout-data"
  }, /*#__PURE__*/React.createElement("input", {
    type: "text",
    className: "dark input form-control",
    value: datas[1],
    onChange: evt => on(evt, 1)
  }))));
};

const media = (__field, on, fieldData) => {
  return /*#__PURE__*/React.createElement("div", {
    className: "my-2"
  }, /*#__PURE__*/React.createElement(MediaChooser, {
    url: __field,
    id: "media",
    padding: 5,
    height: 140,
    onChange: on
  }));
};

const personal_links = (__field, on, fieldData) => {
  // console.log( __field);
  const datas = __field ? __field.split(CARD_SEPARATOR()) : [];
  const btns = datas.map((ee, ii) => /*#__PURE__*/React.createElement("div", {
    className: "d-flex",
    key: ii
  }, /*#__PURE__*/React.createElement("input", {
    type: "text",
    className: "dark input form-control",
    value: ee,
    onChange: evt => on(evt, ii),
    placeholder: __("put the link")
  }), /*#__PURE__*/React.createElement(Button, {
    icon: "minus",
    minimal: true,
    className: "my-2",
    onClick: () => onRemoveLink(ii)
  }), /*#__PURE__*/React.createElement(Button, {
    icon: "chevron-down",
    minimal: true,
    className: "my-2",
    disabled: ii == datas.length - 1,
    onClick: () => on("down", ii)
  }), /*#__PURE__*/React.createElement(Button, {
    icon: "chevron-up",
    minimal: true,
    className: "my-2",
    disabled: ii == 0,
    onClick: () => on("up", ii)
  })));

  const onRemoveLink = i => {};

  return /*#__PURE__*/React.createElement("div", {
    className: "container card p-3"
  }, /*#__PURE__*/React.createElement("div", {
    className: "row p-2 my-2 bg-light border "
  }, /*#__PURE__*/React.createElement("div", {
    className: "col-12 flex-column"
  }, btns), /*#__PURE__*/React.createElement("div", {
    className: "offset-2 col-10"
  }, /*#__PURE__*/React.createElement(Button, {
    icon: "plus",
    className: "my-2",
    onClick: () => on("addLink")
  }))));
};

const price = (__field, on, fieldData) => {
  const datas = __field ? __field.split(CARD_SEPARATOR()) : ["", "", "", ""];
  console.log(fieldData.variant_param_1);
  console.log(fieldData.variant_param_2);

  const currencySelector = selected => {
    return getCurrencies().map((e, i) => {
      return /*#__PURE__*/React.createElement("option", {
        value: e.id,
        selected: selected,
        key: i
      }, __(e.title));
    });
  };

  let currency = getCurrencies().filter(e => {
    // console.log(e, e.id == datas[1])
    return e.id == datas[1];
  })[0];
  currency = currency ? currency : getCurrencies()[0];
  return /*#__PURE__*/React.createElement("div", {
    className: "container card p-3"
  }, /*#__PURE__*/React.createElement("div", {
    className: "row dat "
  }, /*#__PURE__*/React.createElement("div", {
    className: "col-12 layout-label"
  }, __("Nominal")), /*#__PURE__*/React.createElement("div", {
    className: "col-12 layout-data"
  }, /*#__PURE__*/React.createElement("input", {
    type: "text",
    className: "dark input form-control",
    value: datas[0],
    onChange: evt => on(evt, 0)
  })), /*#__PURE__*/React.createElement("div", {
    className: "col-12 layout-label"
  }, __("Mini nominal")), /*#__PURE__*/React.createElement("div", {
    className: "col-12 layout-data"
  }, /*#__PURE__*/React.createElement("input", {
    type: "text",
    className: "dark input form-control",
    value: datas[3],
    onChange: evt => on(evt, 3)
  })), /*#__PURE__*/React.createElement("div", {
    className: "col-12 lead title opacity_75"
  }, __(fieldData.variant_param_1))));
};

const section = (__field, on, fieldData) => {
  return /*#__PURE__*/React.createElement("input", {
    type: "text",
    className: "dark input form-control",
    value: __field,
    onChange: on
  });
};