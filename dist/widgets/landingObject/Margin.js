function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";

class Margin extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onTop", evt => {
      const marginTop = evt.currentTarget.value;
      this.setState({
        marginTop
      });
    });

    _defineProperty(this, "onLeft", evt => {
      const marginLeft = evt.currentTarget.value;
      this.setState({
        marginLeft
      });
    });

    _defineProperty(this, "onRight", evt => {
      const marginRight = evt.currentTarget.value;
      this.setState({
        marginRight
      });
    });

    _defineProperty(this, "onBottom", evt => {
      const marginBottom = evt.currentTarget.value;
      this.setState({
        marginBottom
      });
    });

    this.state = {
      marginTop: parseInt(this.props.value.marginTop),
      marginRight: parseInt(this.props.value.marginRight),
      marginBottom: parseInt(this.props.value.marginBottom),
      marginLeft: parseInt(this.props.value.marginLeft)
    };
  }

  render() {
    const {
      marginTop,
      marginRight,
      marginBottom,
      marginLeft
    } = this.state; // console.log( this.props );

    return /*#__PURE__*/React.createElement("div", {
      className: "d-flex flex-column  mb-3"
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex w-100"
    }, /*#__PURE__*/React.createElement("div", {
      className: " w-100"
    }), /*#__PURE__*/React.createElement("div", {
      className: "d-flex justify-content-center w-100"
    }, /*#__PURE__*/React.createElement("div", null, "top:"), /*#__PURE__*/React.createElement("input", {
      type: "number",
      className: "input dark w_45 ml-2 p-0",
      value: marginTop,
      onChange: this.onTop
    })), /*#__PURE__*/React.createElement("div", {
      className: " w-100"
    })), /*#__PURE__*/React.createElement("div", {
      className: "d-flex w-100"
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex align-items-end justify-content-center flex-column w-100"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text-right"
    }, "left:"), /*#__PURE__*/React.createElement("input", {
      type: "number",
      className: "input dark w_45 p-0",
      value: marginLeft,
      onChange: this.onLeft
    })), /*#__PURE__*/React.createElement("div", {
      className: "landing-padding-icon "
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-align-justify"
    })), /*#__PURE__*/React.createElement("div", {
      className: "d-flex align-items-start  justify-content-center flex-column w-100"
    }, /*#__PURE__*/React.createElement("div", null, "right:"), /*#__PURE__*/React.createElement("input", {
      type: "number",
      className: "input dark w_45 p-0",
      value: marginRight,
      onChange: this.onRight
    }))), /*#__PURE__*/React.createElement("div", {
      className: "d-flex w-100"
    }, /*#__PURE__*/React.createElement("div", {
      className: " w-100"
    }), /*#__PURE__*/React.createElement("div", {
      className: "d-flex justify-content-center w-100"
    }, /*#__PURE__*/React.createElement("div", null, "bottom:"), /*#__PURE__*/React.createElement("input", {
      type: "number",
      className: "input dark w_45 mr-2 p-0",
      value: marginBottom,
      onChange: this.onBottom
    })), /*#__PURE__*/React.createElement("div", {
      className: " w-100"
    })));
  }

}

export default Margin;