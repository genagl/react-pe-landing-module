function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
  Кнопка единичного поля в интерфейсе редактирования Карточки (Card)
*/
import React, { Component } from "react";
import { __ } from "react-pe-utilities";
import { LayoutIcon } from 'react-pe-useful';
import { CardFieldTypes, CARD_SEPARATOR } from "../../views/LandingState/Card";
import { sectionDataParse } from "./CardSingleFieldType";
import SectionButton from "./SectionButton";
import __cards from "../../views/LandingState/assets/img/landing/card.svg";
import CardFieldMetaphors from "../../views/LandingState/card/CardFieldMetaphors";

class CardSingleField extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "getCardMetafor", id => {
      // console.log(id)
      const met = CardFieldMetaphors().filter(metafor => metafor._id == id);
      return met[0] ? met[0] : {
        title: "Dummy"
      };
    });

    _defineProperty(this, "brt", () => {
      const {
        card_fields,
        i
      } = this.props.object;
      const {
        __field
      } = this.state;
      let datas;
      if (!card_fields[i]) return "";

      switch (card_fields[i].type) {
        case "person":
          datas = __field ? __field.split(CARD_SEPARATOR()) : ["", "", ""];
          return /*#__PURE__*/React.createElement("div", {
            className: "d-flex text-white p-2"
          }, /*#__PURE__*/React.createElement("div", {
            style: {
              width: 100,
              height: 100,
              backgroundImage: "url(" + datas[2] + ")",
              backgroundSize: "cover",
              flexGrow: 1,
              height: card_fields[i].height + "px"
            }
          }), /*#__PURE__*/React.createElement("div", {
            className: " landing-card-field-single-content "
          }, /*#__PURE__*/React.createElement("div", null, datas[0]), /*#__PURE__*/React.createElement("div", null, datas[1])));

        case "cf":
        case "navlink":
        case "outerlink":
          datas = __field ? __field.split(CARD_SEPARATOR()) : ["", ""];
          return /*#__PURE__*/React.createElement("div", {
            className: " text-white p-2"
          }, datas[1]);

        case "personal_links":
          datas = __field ? __field.split(CARD_SEPARATOR()) : [];
          const btns = datas.map((ee, ii) => {
            let fa = "fas fa-link";

            if (ee.indexOf("facebook.com") >= 0) {
              fa = "fab fa-facebook-f";
            } else if (ee.indexOf("vk.com") >= 0) {
              fa = "fab fa-vk";
            } else if (ee.indexOf("youtube.com") >= 0) {
              fa = "fab fa-youtube";
            } else {
              fa = "fas fa-link";
            }

            return /*#__PURE__*/React.createElement("div", {
              key: ii,
              className: "lcfpl"
            }, /*#__PURE__*/React.createElement("span", {
              className: fa
            }));
          });
          return /*#__PURE__*/React.createElement("div", {
            className: "d-flex text-white p-2"
          }, btns);

        case "media":
          return /*#__PURE__*/React.createElement("div", {
            className: "landing-image",
            style: {
              backgroundImage: `url(${__field})`,
              minHeight: "90px",
              minHeight: card_fields[i].height + "px"
            }
          });

        case "icon":
          datas = __field ? __field.split(CARD_SEPARATOR()) : ["", "", "", ""];
          return /*#__PURE__*/React.createElement("div", {
            className: " text-white p-2 " + datas[0],
            style: {
              fontSize: "45px",
              height: card_fields[i].height + "px"
            }
          });

        case "check":
          try {
            datas = JSON.parse(__field.replaceAll("!~!~", '"'));
            datas = Array.isArray(datas) ? datas : [];
          } catch (e) {
            datas = [];
          }

          return /*#__PURE__*/React.createElement("div", {
            className: " text-white p-2 "
          }, /*#__PURE__*/React.createElement("div", {
            className: "w-100"
          }, datas.map((dt, index) => {
            return /*#__PURE__*/React.createElement("div", {
              key: index
            }, /*#__PURE__*/React.createElement("span", {
              className: " px-2 "
            }, dt.icon ? /*#__PURE__*/React.createElement("i", {
              className: "fas fa-check text-success"
            }) : /*#__PURE__*/React.createElement("i", {
              className: "fas fa-times text-danger"
            })), /*#__PURE__*/React.createElement("span", {
              className: " "
            }, dt.label));
          })));

        case "price":
          datas = __field ? __field.split(CARD_SEPARATOR()) : ["", "", "", ""];
          return /*#__PURE__*/React.createElement("div", {
            className: " text-white p-2 "
          }, /*#__PURE__*/React.createElement("span", {
            className: " title "
          }, datas[0]), ".", /*#__PURE__*/React.createElement("span", {
            className: "pr-1"
          }, datas[3]), /*#__PURE__*/React.createElement("span", {
            className: "px-1"
          }, datas[1]));

        case "section":
          const __object = sectionDataParse(__field);

          const button = __object.length > 0 ? __object.map((e, i) => /*#__PURE__*/React.createElement(SectionButton, {
            key: i,
            object: e
          })) : "EMPTY";
          return /*#__PURE__*/React.createElement("div", {
            className: "d-flex text-white p-2"
          }, button);

        default:
        case "string":
          return /*#__PURE__*/React.createElement("div", {
            className: " d-flex text-white p-2"
          }, __field ? __field.toString() : "");
      }
    });

    _defineProperty(this, "onClick", () => {
      this.props.onClick(this.props.object);
    });

    this.state = { ...this.props,
      __field: this.props.object.field
    };
    this.ref = /*#__PURE__*/React.createRef();
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.object.field !== this.state.__field) {
      this.setState({
        __field: nextProps.object.field
      });
    }
  }

  render() {
    const {
      card_fields,
      i,
      variant
    } = this.props.object;
    const index = i < card_fields.length ? i : 0; //console.log( this.props );
    //console.log( { height : card_fields[index].height } );

    const rr = CardFieldTypes().filter((eee, ii) => eee.type == card_fields[index].type)[0];
    const r = i < card_fields.length ? rr : CardFieldTypes()[0];
    const tp = card_fields[index] ? card_fields[index].type : "empty";
    return /*#__PURE__*/React.createElement("div", {
      className: "landing-card-field-single bg-secondary ",
      style: {
        height: card_fields[index].height + "px"
      },
      onClick: this.onClick
    }, /*#__PURE__*/React.createElement("div", {
      className: "l-icon "
    }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(LayoutIcon, {
      src: r.icon,
      className: "layout-icon white scale-60"
    }), /*#__PURE__*/React.createElement("div", {
      className: "mt-1 text-light hidden"
    }, __(r.title)))), /*#__PURE__*/React.createElement("div", {
      className: `landing-card-field w-100 p-0 ${tp}${variant ? variant : "0"}`
    }, this.brt(this.props.object)), /*#__PURE__*/React.createElement("div", {
      className: "landing-card-field-metaphore"
    }, __(this.getCardMetafor(card_fields[index].metafor).title)));
  }

  on(data) {
    if (this.props.on) {
      console.log(data);
      this.props.on("fields", data);
    }
  }

}

export default CardSingleField;