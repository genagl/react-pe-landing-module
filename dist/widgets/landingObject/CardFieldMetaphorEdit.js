import React, { useState } from "react";
import { __ } from "react-pe-utilities";
import CardFieldMetaphors from "../../views/LandingState/card/CardFieldMetaphors";

const CardFieldMetaphorEdit = props => {
  const [value, setValue] = useState(props.value ? {
    value: props.value,
    label: CardFieldMetaphors().filter(e => e._id == props.value)[0] ? CardFieldMetaphors().filter(e => e._id == props.value)[0].title : "---" + props.value
  } : {
    value: -1,
    label: null
  });

  const on = value => {
    if (props.on) {
      props.on(value);
    }
  };

  const onChange = value => {
    console.log(value.value);
    setValue(value);
    on(value.value);
  };

  return /*#__PURE__*/React.createElement("div", {
    className: "w-100"
  }, /*#__PURE__*/React.createElement("div", {
    className: "row"
  }, CardFieldMetaphors().map((e, i) => {
    return /*#__PURE__*/React.createElement("div", {
      className: "col-md-6",
      key: i
    }, /*#__PURE__*/React.createElement("label", {
      className: "_check_"
    }, /*#__PURE__*/React.createElement("input", {
      type: "radio",
      checked: value.value == e._id,
      onChange: evt => onChange({
        value: e._id
      })
    }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", null, __(e.title)), /*#__PURE__*/React.createElement("small", null, __(e.description)))));
  }))); // const options = CardFieldMetaphors().map((e, i) => {
  //     return {
  //         value: e._id,
  //         label: e.title
  //     }
  // })
  // return <>
  //     <Select
  //         value={value}
  //         isSearchable
  //         onChange={onChange}
  //         options={options}
  //         placeholder={__("Select Field metaphor")}
  //         className="basic-multi-select w-100 flex-grow-100 "
  //         classNamePrefix="select-color-"
  //     />
  // </>
};

export default CardFieldMetaphorEdit;