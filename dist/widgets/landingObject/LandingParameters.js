import { Button, ButtonGroup, Intent } from "@blueprintjs/core";
import React from "react";
import { __ } from "react-pe-utilities";

const LandingParameters = ({
  onAdd,
  onDownload,
  onLoadChange,
  onClearOpen
}) => {
  return /*#__PURE__*/React.createElement("div", {
    className: "py-4 w-100"
  }, /*#__PURE__*/React.createElement(ButtonGroup, {
    fill: true,
    vertical: true,
    large: true
  }, /*#__PURE__*/React.createElement(ButtonGroup, {
    vertical: true
  }, /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    onClick: onAdd,
    large: false,
    fill: true,
    className: "mr-0 pe-room-btn my-1",
    icon: "plus"
  }, __("Add section")), /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    onClick: onDownload,
    fill: true,
    className: "mr-1 pe-room-btn my-1",
    icon: "download"
  }, __("Download source json")), /*#__PURE__*/React.createElement("div", {
    className: "position-relative pe-room-btn my-1"
  }, /*#__PURE__*/React.createElement("input", {
    type: "file",
    style: {
      width: "100%",
      height: 38
    },
    onChange: onLoadChange
  }), /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    onClick: onDownload,
    className: "position-absolute z-index-100 untouchble pe-room-btn",
    icon: "upload",
    fill: true,
    title: __("Upload Landing json")
  }, __("Upload Landing json"))), /*#__PURE__*/React.createElement(Button, {
    intent: Intent.DANGER,
    onClick: onClearOpen,
    fill: true,
    className: "mr-1 pe-room-btn my-1",
    icon: "clear"
  }, __("Clear all")))));
};

export default LandingParameters;