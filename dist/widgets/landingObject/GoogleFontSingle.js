function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import WebFont from "webfontloader";
import googleFontList from "../../views/LandingState/data/google_fonts.list.json";
import { __ } from "react-pe-utilities";

class GoogleFontSingle extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onChange", evt => {
      const v = evt.currentTarget.value;
      WebFont.load({
        google: {
          families: [v]
        }
      });
      this.setState({
        value: { ...this.state.value,
          title: v
        }
      });
      setTimeout(() => {
        if (this.props.onChange) {
          this.props.onChange(v);
        }
      }, 100);
    });

    this.state = { ...props
    };
  }

  componentWillUpdate(newProps, nextState) {
    if (newProps.languages != this.state.languages) {
      this.setState({
        languages: newProps.languages
      });
    }
  }

  getList() {
    const {
      languages
    } = this.state;
    const list = googleFontList.items.filter(e => {
      if (Array.isArray(languages) && languages.length > 0) {
        return languages.filter(ee => e.subsets.filter(subset => subset == ee).length > 0).length > 0;
      }

      return true;
    }).map((e, i) => /*#__PURE__*/React.createElement("option", {
      key: i,
      value: e.family
    }, e.family));
    const current_number = ["Title font", "Subtitle font", "3 font", "4 font", "5 font", "6 font", "7 font", "8 font", "9 font", "10 font"][this.props.i];
    const current = this.state.value ? googleFontList.items.filter(e => e.family == this.state.value.title)[0] : null;
    const vars = current ? current.variants.map((e, i) => /*#__PURE__*/React.createElement("div", {
      key: i,
      className: "py-0"
    }, /*#__PURE__*/React.createElement("label", {
      className: "_check_"
    }, /*#__PURE__*/React.createElement("input", {
      type: "checkbox",
      value: e
    }), e))) : null;
    return /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-5 d-flex align-items-end"
    }, __(current_number)), /*#__PURE__*/React.createElement("div", {
      className: "col-md-7"
    }, /*#__PURE__*/React.createElement("select", {
      className: "form-control input dark mb-2",
      value: this.state.value ? this.state.value.title : "",
      onChange: this.onChange
    }, /*#__PURE__*/React.createElement("option", {
      value: ""
    }, "--"), list)), /*#__PURE__*/React.createElement("div", {
      className: "col-md-12"
    }, /*#__PURE__*/React.createElement("div", {
      style: {
        fontSize: "1.5rem",
        fontFamily: this.state.value ? this.state.value.title : null,
        opacity: this.state.value ? 1 : 0.25
      }
    }, __("Lorem ipsum"))), /*#__PURE__*/React.createElement("div", {
      className: "col-md-12 d-flex flex-wrap hidden"
    }, vars));
  }

  render() {
    return /*#__PURE__*/React.createElement("div", null, this.getList());
  }

}

export default GoogleFontSingle;