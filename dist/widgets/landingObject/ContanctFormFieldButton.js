function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Suspense } from "react";
import { Button } from "@blueprintjs/core";
import $ from "jquery";
import { __ } from "react-pe-utilities";
import ContactFormVariant from "./ContactFormVariant";
import { LayoutIcon } from 'react-pe-useful';
import { Loading } from 'react-pe-useful';
import __string from "../../views/LandingState/assets/img/landing/string.svg";
import __email from "../../views/LandingState/assets/img/landing/email.svg";
import __phone from "../../views/LandingState/assets/img/landing/phone.svg";
import __image from "../../views/LandingState/assets/img/landing/picture.svg";
import __time from "../../views/LandingState/assets/img/landing/time.svg";
import __calendar from "../../views/LandingState/assets/img/landing/calendar.svg";
import __list from "../../views/LandingState/assets/img/landing/list.svg";
const Select = /*#__PURE__*/React.lazy(() => import("react-select"));

class ContanctFormFieldButton extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "switchLabel", data => {
      const dat = data ? data : formTypes()[0];
      return {
        value: dat.type,
        label: /*#__PURE__*/React.createElement("div", {
          className: "w-100 d-flex align-items-center"
        }, /*#__PURE__*/React.createElement(LayoutIcon, {
          src: dat.icon,
          className: "layout-icon-minimal mr-3 dark"
        }), /*#__PURE__*/React.createElement("div", {
          className: " font-weight-bold "
        }, __(dat.title)))
      };
    });

    _defineProperty(this, "getSwitcher", () => {
      //console.log(this.props.object)
      const val = this.switchLabel(formTypes().filter(e => e.type == this.props.object.type)[0]);
      const menuDSelect = formTypes().map((e, i) => {
        return this.switchLabel(e);
      });
      const customStyles = {
        option: base => ({ ...base,
          minHeight: 35,
          padding: 2,
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center'
        }),
        valueContainer: base => ({ ...base,
          minHeight: 60,
          fontSize: '.8rem',
          whiteSpace: 'unset',
          backgroundColor: '#ced0d2',
          border: 'none',
          borderRadius: 0
        }),
        singleValue: base => ({ ...base,
          whiteSpace: 'unset'
        }),
        indicatorsContainer: (provided, state) => ({ ...provided,
          backgroundColor: '#ced0d2'
        }),
        dropdownIndicator: (provided, state) => ({ ...provided,
          color: '#444'
        })
      };
      return /*#__PURE__*/React.createElement(Suspense, {
        fallback: /*#__PURE__*/React.createElement(Loading, null)
      }, /*#__PURE__*/React.createElement(Select, {
        value: val,
        isMulti: false,
        isSearchable: true,
        options: menuDSelect,
        styles: customStyles,
        className: "basic-multi-select ",
        classNamePrefix: "select-route-",
        onChange: this.handleCurrentMenu
      }));
    });

    _defineProperty(this, "variantForm", () => {
      const data = Array.isArray(this.props.object.data) ? this.props.object.data : [];
      const vForms = data.map((e, i) => /*#__PURE__*/React.createElement(ContactFormVariant, _extends({
        key: i,
        i: i,
        on: this.onChangeData
      }, e)));
      return /*#__PURE__*/React.createElement("div", null, vForms, /*#__PURE__*/React.createElement(Button, {
        icon: "plus",
        onClick: this.onAddVariant
      }));
    });

    _defineProperty(this, "handleCurrentMenu", data => {
      let type = formTypes().filter(e => e.type === data.value)[0];
      if (!type) type = formTypes()[0];
      const object = { ...this.props.object,
        type: type.type
      };
      console.log(object);
      this.onChange(object);
    });

    _defineProperty(this, "onRowCount", evt => {
      const object = { ...this.props.object,
        rows: evt.currentTarget.value
      };
      this.onChange(object);
    });

    _defineProperty(this, "onLabel", evt => {
      const object = { ...this.props.object,
        label: evt.currentTarget.value
      };
      this.onChange(object);
    });

    _defineProperty(this, "onDescription", evt => {
      const object = { ...this.props.object,
        description: evt.currentTarget.value
      };
      this.onChange(object);
    });

    _defineProperty(this, "onAddVariant", () => {
      const data = Array.isArray(this.props.object.data) ? this.props.object.data : [];
      const object = { ...this.props.object,
        data: [...data, {
          label: "new variant",
          value: ""
        }]
      };
      this.onChange(object);
    });

    _defineProperty(this, "onIsReq", evt => {
      const is_required = $(evt.currentTarget).is(":checked") ? 1 : 0;
      const object = { ...this.props.object,
        is_required
      };
      this.onChange(object);
    });

    _defineProperty(this, "onChangeData", (field, value, i) => {
      const object = { ...this.props.object
      };
      if (!Array.isArray(object.data)) object.data = [];
      if (!object.data[i]) object.data[i] = {};
      object.data[i][field] = value;
      console.log(object);
      this.onChange(object);
    });

    _defineProperty(this, "onChange", object => {
      console.log(object);
      this.setState(object);
      this.props.onChange(object, this.props.i);
    });

    _defineProperty(this, "onClose", () => {
      this.props.onClose(this.props.i);
    });

    this.state = {
      object: this.props.object
    };
  }

  render() {
    //console.log(this.props)
    //console.log(this.props.object)
    const style = {
      margin: 0
    };
    return /*#__PURE__*/React.createElement("div", {
      className: " btn-item p-0  h-100",
      style: style
    }, /*#__PURE__*/React.createElement("div", {
      className: " layout-centered flex-column card p-4 m-0 h-100"
    }, /*#__PURE__*/React.createElement("div", {
      className: "title hidden"
    }, this.props.i), /*#__PURE__*/React.createElement("div", {
      className: " hidden "
    }, __("Put form label")), /*#__PURE__*/React.createElement("div", {
      className: "mb-4"
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      value: this.props.object.label,
      onChange: this.onLabel,
      className: "form-control input dark",
      placeholder: __("label")
    })), /*#__PURE__*/React.createElement("div", {
      className: " hidden "
    }, __("Select input type")), /*#__PURE__*/React.createElement("div", {
      className: "mb-4"
    }, this.getSwitcher()), this.props.object.type == "radio" ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", null, __("Set choosed variants")), /*#__PURE__*/React.createElement("div", {
      className: "mb-3"
    }, this.variantForm())) : null, this.props.object.type == "text" ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "mb-3"
    }, /*#__PURE__*/React.createElement("input", {
      type: "number",
      value: this.props.object.rows,
      onChange: this.onRowCount,
      className: "form-control input dark",
      placeholder: __("row count")
    }))) : null, /*#__PURE__*/React.createElement("div", {
      className: " hidden "
    }, __("Put form description")), /*#__PURE__*/React.createElement("div", {
      className: "mb-4"
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      value: this.props.object.description,
      onChange: this.onDescription,
      className: "form-control input dark",
      placeholder: __("description")
    })), /*#__PURE__*/React.createElement("div", {
      className: "mb-4"
    }, /*#__PURE__*/React.createElement("input", {
      type: "checkbox",
      value: 1,
      onChange: this.onIsReq,
      checked: this.props.object.is_required,
      className: "checkbox",
      id: `is_required_${this.props.i}`,
      placeholder: __("description")
    }), /*#__PURE__*/React.createElement("label", {
      htmlFor: `is_required_${this.props.i}`
    }, __("Is Required"))), /*#__PURE__*/React.createElement("button", {
      className: "close top-right m-2",
      "aria-label": "Close",
      onClick: this.onClose
    }, /*#__PURE__*/React.createElement("span", {
      "aria-hidden": "true"
    }, "\xD7"))));
  }

}

export default ContanctFormFieldButton;
export function formTypes() {
  return [{
    type: "string",
    title: "String",
    icon: __string
  }, {
    type: "text",
    title: "Text",
    icon: __string
  }, {
    type: "email",
    title: "email",
    icon: __email
  }, {
    type: "phone",
    title: "phone",
    icon: __phone
  }, {
    type: "file_loader",
    title: "File loader",
    icon: __image
  }, {
    type: "time",
    title: "Time",
    icon: __time
  }, {
    type: "calendar",
    title: "Calendar",
    icon: __calendar
  }, {
    type: "radio",
    title: "Choosing one variant",
    icon: __list
  }];
}