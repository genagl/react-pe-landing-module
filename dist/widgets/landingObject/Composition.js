function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { __ } from "react-pe-utilities";
import { LayoutIcon } from 'react-pe-useful';
import composition1 from "../../views/LandingState/assets/img/landing/composition1.svg";
import composition2 from "../../views/LandingState/assets/img/landing/composition2.svg";
import composition3 from "../../views/LandingState/assets/img/landing/composition3.svg";
import composition4 from "../../views/LandingState/assets/img/landing/composition4.svg";

class Composition extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onTypeSwitch", evt => {
      const type = evt.currentTarget.getAttribute("type");
      this.setState({
        value: type
      });
      this.props.onChange(type, "type");
    });

    this.state = { ...this.props
    };
  }

  render() {
    // console.log(this.state);
    const btns = [];
    [{
      id: 0,
      icon: composition1,
      title: "title by left and content by right"
    }, {
      id: 1,
      icon: composition2,
      title: "content by left and title by right"
    }, {
      id: 3,
      icon: composition3,
      title: "title and content are full width"
    }, {
      id: 2,
      icon: composition4,
      title: "content and title are full width"
    }].forEach((e, i) => {
      const ccl = e.id === this.state.value ? " active " : " ";
      btns.push( /*#__PURE__*/React.createElement("div", {
        key: i,
        type: e.id,
        className: `l-icon-gian border-dark ${ccl}`,
        onClick: this.onTypeSwitch
      }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(LayoutIcon, {
        src: e.icon,
        className: "layout-icon-giant grey "
      }), /*#__PURE__*/React.createElement("div", null, __(e.title)))));
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "p-4"
    }, btns);
  }

}

export default Composition;