function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { LayoutIcon } from 'react-pe-useful';
import __cards from "../../views/LandingState/assets/img/landing/card.svg";

class CardButton extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onClick", () => {
      this.props.onClick(this.props.object);
    });

    this.state = { ...this.props
    };
    this.ref = /*#__PURE__*/React.createRef();
  }

  render() {
    // console.log( this.props.object );
    return /*#__PURE__*/React.createElement("div", {
      className: " square2 bg-secondary m-1 btn-item",
      onClick: this.onClick
    }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(LayoutIcon, {
      src: __cards,
      className: " layout-icon white"
    }), /*#__PURE__*/React.createElement("div", {
      className: "small text-white "
    }, this.props.object.title)));
  }

}

export default CardButton;