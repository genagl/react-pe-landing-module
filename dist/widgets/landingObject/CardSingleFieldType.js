function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
  Редактирование свойств поля Карточки (Card)
*/
import React, { Component, Fragment } from "react";
import { Button, ButtonGroup, Intent } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import { MediaChooser } from "react-pe-useful";
import { CARD_SEPARATOR } from "../../views/LandingState/Card";
import InputForm from "../../views/LandingState/edit/InputForm";
import { Icon } from "react-pe-scalars";
import { getCurrencies } from "../../views/LandingState/card/CardFieldPrice";
import { AppToaster } from 'react-pe-useful';
import $ from "jquery";

class CardSingleFieldType extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "form", () => {
      const {
        card_fields,
        i
      } = this.props.origin.object; // console.log( this.state.__field );

      if (card_fields[i]) {
        switch (card_fields[i].type) {
          case "person":
            return this.person();

          case "check":
            return this.check();

          case "icon":
            return this.icon();

          case "cf":
          case "navlink":
          case "outerlink":
            return this.outerlink();

          case "media":
            return this.media();

          case "personal_links":
            return this.personal_links();

          case "price":
            return this.price();

          case "section":
            return this.section();

          case "string":
          default:
            return this.string();
        }
      } else return this.string();
    });

    _defineProperty(this, "onAddLink", () => {
      const __field = this.state.__field ? this.state.__field + CARD_SEPARATOR() : CARD_SEPARATOR();

      this.setState({
        __field
      });
      this.on(__field);
    });

    _defineProperty(this, "onRemoveLink", ii => {
      let __field = this.state.__field ? this.state.__field.split(CARD_SEPARATOR()) : [];

      __field.splice(ii, 1);

      __field = __field.join(CARD_SEPARATOR());
      this.setState({
        __field
      });
      this.on(__field);
    });

    _defineProperty(this, "onDn", ii => {
      let __field = this.state.__field ? this.state.__field.split(CARD_SEPARATOR()) : [];

      const fl = __field.splice(ii, 1);

      __field.splice(ii + 1, 1, fl);

      __field = __field.join(CARD_SEPARATOR());
      this.setState({
        __field
      });
      this.on(__field);
    });

    _defineProperty(this, "onUp", ii => {
      let __field = this.state.__field ? this.state.__field.split(CARD_SEPARATOR()) : [];

      const fl = __field.splice(ii - 1, 1);

      __field.splice(ii, 1, fl);

      __field = __field.join(CARD_SEPARATOR());
      this.setState({
        __field
      });
      this.on(__field);
    });

    _defineProperty(this, "onIcon", __field => {
      this.setState({
        __field
      });
      this.on(__field);
    });

    _defineProperty(this, "onString", evt => {
      const __field = evt.currentTarget.value;
      this.setState({
        __field
      });
      this.on(__field);
    });

    _defineProperty(this, "onAddCheck", () => {
      let __field = this.state.__field;

      try {
        __field = JSON.parse(this.state.__field.replaceAll("!~!~", '"'));
        __field = Array.isArray(__field) ? __field : [];
      } catch (e) {
        __field = [];
      }

      __field.push({
        icon: 1,
        label: ''
      });

      __field = JSON.stringify(__field);
      __field = __field.replaceAll('"', '!~!~');
      this.setState({
        __field
      });
    });

    _defineProperty(this, "onRemoveCheck", pos => {
      let __field = this.state.__field;

      try {
        __field = JSON.parse(this.state.__field.replaceAll("!~!~", '"'));
        __field = Array.isArray(__field) ? __field : [];
      } catch (e) {
        __field = [];
      }

      __field.splice(pos, 1);

      __field = JSON.stringify(__field);
      __field = __field.replaceAll('"', '!~!~');
      this.setState({
        __field
      });
      this.on(__field);
    });

    _defineProperty(this, "onDatasCheck", (evt, pos, field) => {
      let __field = this.state.__field,
          val;

      try {
        __field = JSON.parse(this.state.__field.replaceAll("!~!~", '"'));
        __field = Array.isArray(__field) ? __field : [];
      } catch (e) {
        __field = [];
      }

      switch (field) {
        case "icon":
          val = evt.currentTarget.checked ? 1 : 0;
          __field[pos].icon = val;
          break;

        case "label":
        default:
          val = evt.currentTarget.value;
          __field[pos][field] = val;
          break;
      }

      __field = JSON.stringify(__field);
      __field = __field.replaceAll('"', '!~!~');
      this.setState({
        __field
      });
      this.on(__field);
    });

    _defineProperty(this, "onDatas", (evt, pos) => {
      let __field = this.state.__field ? [...this.state.__field.split(CARD_SEPARATOR())] : [];

      __field[pos] = evt.currentTarget.value;
      __field = __field.join(CARD_SEPARATOR());
      console.log(__field);
      this.setState({
        __field
      });
      this.on(__field);
    });

    _defineProperty(this, "onDatasValue", (value, pos) => {
      console.log(value);

      let __field = this.state.__field ? [...this.state.__field.split(CARD_SEPARATOR())] : [];

      __field[pos] = value;
      __field = __field.join(CARD_SEPARATOR());
      console.log(__field);
      this.setState({
        __field
      });
      this.on(__field);
    });

    _defineProperty(this, "onDatasIcon", (data, pos) => {
      let __field = this.state.__field ? this.state.__field.split(CARD_SEPARATOR()) : [];

      __field[pos] = data;
      __field = __field.join(CARD_SEPARATOR());
      this.setState({
        __field
      });
      this.on(__field);
    });

    _defineProperty(this, "onMedia", (value, file) => {
      const __field = value;
      this.setState({
        __field
      });
      this.on(__field);
    });

    _defineProperty(this, "copy", evt => {
      evt.stopPropagation();
      const copy = JSON.stringify(this.state.__field); //console.log(copy)

      console.log(this.state.__field);
      $("body").append(`<div style='position:absolute; z-index:-100; width:100%; top:0; left:0;'><textarea style='width:100%;' id='StyleClipboard'>${copy}</textarea></div>`);
      const copyText = document.getElementById("StyleClipboard");
      copyText.select();
      copyText.setSelectionRange(0, 99999999999999999999);
      document.execCommand("copy");
      $("#StyleClipboard").remove();
      AppToaster.show({
        intent: Intent.SUCCESS,
        icon: "tick",
        duration: 10000,
        message: __("Section copy to clipbord")
      });
    });

    _defineProperty(this, "paste", evt => {
      evt.stopPropagation();
      navigator.clipboard.readText().then(clipText => {
        try {
          //console.log(clipText)
          const clip = JSON.parse(clipText);
          this.setState({
            __field: clip
          });
          this.on(clip);
        } catch (e) {
          AppToaster.show({
            intent: Intent.DANGER,
            icon: "tick",
            duration: 10000,
            message: __("Error read clipboard data")
          });
        }
      });
    });

    this.state = { ...props,
      __field: this.props.origin.field
    };
  }

  render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "w-100 d-flex"
    }, this.form(), /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      "data-hint": __("copy to clipboard"),
      onClick: this.copy,
      className: "hint hint--lef "
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-file-import"
    })), /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      "data-hint": __("Paste from clipboard"),
      onClick: this.paste,
      className: "hint hint--left "
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-file-export"
    })));
  }

  section() {
    const {
      card_fields,
      i
    } = this.props.origin.object;
    const datas = this.state.__field ? this.parse(this.state.__field) : [];
    return /*#__PURE__*/React.createElement("div", {
      className: " w-100 "
    }, /*#__PURE__*/React.createElement(InputForm, _extends({}, card_fields[i], {
      cellData: {
        sections: {
          title: "",
          type: "landing_object",
          kind: "array",
          visualization: "landing-object",
          landing_object: "section",
          hidden: 0
        },
        visible_value: {
          hidden: 1,
          value: "class_name"
        }
      },
      source: "section",
      id: i,
      data: {
        sections: datas
      },
      sourceType: "section",
      on: (value, field) => this.onSection(value, field)
    })));
  }

  string() {
    // const { card_fields, i } = this.props.origin.object
    return /*#__PURE__*/React.createElement("input", {
      autoFocus: true,
      type: "text",
      className: "dark input form-control",
      value: this.state.__field,
      onChange: this.onString
    });
  }

  person() {
    // const { card_fields, i } = this.props.origin.object
    // console.log(this.state.__field);
    const datas = this.state.__field ? this.state.__field.split(CARD_SEPARATOR()) : [];
    return /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-12 title"
    }, __("Name:")), /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "dark input form-control col-md-12 mb-3",
      value: datas[0],
      onChange: evt => this.onDatas(evt, 0)
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-12 title"
    }, __("metas:")), /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "dark input form-control col-md-12 mb-3",
      value: datas[1],
      onChange: evt => this.onDatas(evt, 1)
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-12 title"
    }, __("Icon:")), /*#__PURE__*/React.createElement(MediaChooser, {
      url: datas[2],
      id: "media",
      padding: 5,
      height: 140,
      onChange: data => this.onDatasValue(data, 2)
    }));
  }

  personal_links() {
    // const { card_fields, i } = this.props.origin.object
    // console.log(this.state.__field);
    const datas = this.state.__field ? this.state.__field.split(CARD_SEPARATOR()) : [];
    const btns = datas.map((ee, ii) => /*#__PURE__*/React.createElement("div", {
      className: "d-flex",
      key: ii
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "dark input form-control",
      value: ee,
      onChange: evt => this.onDatas(evt, ii),
      placeholder: __("put the link")
    }), /*#__PURE__*/React.createElement(Button, {
      icon: "minus",
      minimal: true,
      className: "my-2",
      onClick: () => this.onRemoveLink(ii)
    }), /*#__PURE__*/React.createElement(Button, {
      icon: "chevron-down",
      minimal: true,
      className: "my-2",
      disabled: ii == datas.length - 1,
      onClick: () => this.onDn(ii)
    }), /*#__PURE__*/React.createElement(Button, {
      icon: "chevron-up",
      minimal: true,
      className: "my-2",
      disabled: ii == 0,
      onClick: () => this.onUp(ii)
    })));
    return /*#__PURE__*/React.createElement("div", {
      className: "container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "row p-2 my-2 bg-light border "
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-12 flex-column"
    }, btns), /*#__PURE__*/React.createElement("div", {
      className: "offset-2 col-10"
    }, /*#__PURE__*/React.createElement(Button, {
      icon: "plus",
      className: "my-2",
      onClick: this.onAddLink
    }))));
  }

  outerlink() {
    // const { card_fields, i } = this.props.origin.object
    const datas = this.state.__field ? this.state.__field.split(CARD_SEPARATOR()) : ["", ""]; // console.log( datas );

    return /*#__PURE__*/React.createElement("div", {
      className: "row dat "
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-4 layout-label"
    }, __("Button's outer URL")), /*#__PURE__*/React.createElement("div", {
      className: "col-8 layout-data"
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "dark input form-control",
      value: datas[0],
      onChange: evt => this.onDatas(evt, 0)
    })), /*#__PURE__*/React.createElement("div", {
      className: "col-4 layout-label"
    }, __("Button label")), /*#__PURE__*/React.createElement("div", {
      className: "col-8 layout-data"
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "dark input form-control",
      value: datas[1],
      onChange: evt => this.onDatas(evt, 1)
    })), /*#__PURE__*/React.createElement("div", {
      className: "col-4 layout-label"
    }, __("How open content?")), /*#__PURE__*/React.createElement("div", {
      className: "col-8 layout-data flex-column"
    }, /*#__PURE__*/React.createElement("label", {
      className: "_check_ pt-2"
    }, /*#__PURE__*/React.createElement("input", {
      type: "radio",
      className: "",
      checked: !datas[2] || datas[2] == 0,
      onChange: evt => this.onDatasValue(0, 2)
    }), __("switch to url")), /*#__PURE__*/React.createElement("label", {
      className: "_check_ "
    }, /*#__PURE__*/React.createElement("input", {
      type: "radio",
      className: "",
      checked: datas[2] == 1,
      onChange: evt => this.onDatasValue(1, 2)
    }), __("open in modal")), /*#__PURE__*/React.createElement("label", {
      className: "_check_ "
    }, /*#__PURE__*/React.createElement("input", {
      type: "radio",
      className: "",
      checked: datas[2] == 2,
      onChange: evt => this.onDatasValue(2, 2)
    }), __("switch to target blank"))));
  }

  check() {
    let datas = [];

    try {
      datas = JSON.parse(this.state.__field.replaceAll("!~!~", '"'));
      datas = Array.isArray(datas) ? datas : [];
    } catch (e) {
      datas = [];
    }

    return /*#__PURE__*/React.createElement("div", {
      className: "w-100"
    }, this.state.__field, datas.map((dt, index) => {
      return /*#__PURE__*/React.createElement("div", {
        className: "w-100 d-flex",
        key: index
      }, /*#__PURE__*/React.createElement("label", {
        className: "_check_"
      }, /*#__PURE__*/React.createElement("input", {
        type: "checkbox",
        className: " ",
        checked: dt.icon || dt.icon == 1 ? true : false,
        onChange: evt => this.onDatasCheck(evt, index, "icon")
      })), /*#__PURE__*/React.createElement("input", {
        type: "text",
        className: "dark input form-control",
        value: dt.label ? dt.label : "",
        onChange: evt => this.onDatasCheck(evt, index, "label")
      }), /*#__PURE__*/React.createElement(Button, {
        icon: "minus",
        onClick: () => this.onRemoveCheck(index),
        minimal: true
      }));
    }), /*#__PURE__*/React.createElement(Button, {
      icon: "plus",
      className: "mt-2",
      onClick: this.onAddCheck
    }, __("add  feature")));
  }

  icon() {
    const {
      card_fields,
      i
    } = this.props.origin.object;
    console.log(card_fields, i);
    const datas = this.state.__field ? this.state.__field.split(CARD_SEPARATOR()) : ["", 40];
    return /*#__PURE__*/React.createElement("div", {
      className: "w-100"
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "dark input form-control",
      value: datas[0],
      onChange: evt => this.onDatas(evt, 0)
    }), /*#__PURE__*/React.createElement(Icon, {
      editable: true,
      value: datas[0],
      on: data => this.onDatasIcon(data, 0)
    }), /*#__PURE__*/React.createElement("input", {
      type: "number",
      className: "dark input form-control",
      value: datas[1] ? datas[1] : card_fields[i].variant_param_1 ? card_fields[i].variant_param_1 : 40,
      onChange: evt => this.onDatas(evt, 1)
    }));
  }

  price() {
    // const { card_fields, i } = this.props.origin.object
    const datas = this.state.__field ? this.state.__field.split(CARD_SEPARATOR()) : ["", "", "", ""];
    console.log(this.state); // const currencySelector = selected => {
    //   return getCurrencies().map((e, i) => {
    //     return <option value={e.id} selected={selected} key={i}>
    //       {__(e.title)}
    //     </option>
    //   })
    // }

    let currency = getCurrencies().filter(e => {
      //console.log(e, e.id == datas[1])
      //return e.id == datas[1]
      if (this.state.origin && this.state.origin.object) {
        return this.state.origin.object.card_fields[this.state.origin.object.i].variant_param_1 === e.id;
      }

      return false;
    })[0];
    currency = currency ? currency : getCurrencies()[0];

    let currency_label = __(currency.title);

    if (this.state.origin && this.state.origin.object) {
      currency_label = currency[this.state.origin.object.card_fields[this.state.origin.object.i].variant_param_2];

      if (this.state.origin.object.card_fields[this.state.origin.object.i].variant_param_2 == "icon") {
        currency_label = /*#__PURE__*/React.createElement("i", {
          className: currency_label
        });
      } else {
        __(currency_label);
      }
    }

    return /*#__PURE__*/React.createElement("div", {
      className: "row dat "
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-6 layout-label"
    }, /*#__PURE__*/React.createElement("div", {
      className: "row dat "
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-12 layout-label"
    }, __("Nominal")), /*#__PURE__*/React.createElement("div", {
      className: "col-12 layout-data align-items-center"
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "dark input form-control",
      value: datas[0],
      onChange: evt => this.onDatas(evt, 0)
    }), currency_label))), /*#__PURE__*/React.createElement("div", {
      className: "col-md-6 layout-label"
    }, /*#__PURE__*/React.createElement("div", {
      className: "row dat "
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-12 layout-label"
    }, __("Mini nominal")), /*#__PURE__*/React.createElement("div", {
      className: "col-12 layout-data"
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "dark input form-control",
      value: datas[3],
      onChange: evt => this.onDatas(evt, 3)
    })))));
  }

  media() {
    // const { card_fields, i } = this.props.origin.object
    return /*#__PURE__*/React.createElement(MediaChooser, {
      url: this.state.__field,
      id: "media",
      padding: 5,
      height: 140,
      onChange: this.onMedia
    });
  }

  onSection(value) {
    const __field = this.stringify(value);

    this.setState({
      __field
    });
    this.on(__field);
  }

  on(data) {
    if (this.props.on) {
      this.props.on("fields", data);
    }
  }

  stringify(data) {
    return secionDataStringify(data);
  }

  parse(field) {
    return sectionDataParse(field);
  }

}

export default CardSingleFieldType;
export function sectionDataParse(field) {
  try {
    const __data = field.replace(/~!~!~/g, "\"").replace(/~!~!'!~!~/g, "\"\"");

    return JSON.parse(__data);
  } catch (error) {
    // console.log(field)
    return [];
  }
}
export function secionDataStringify(data) {
  const __data = JSON.stringify(data);

  return __data.replace(/"/g, "~!~!~");
}