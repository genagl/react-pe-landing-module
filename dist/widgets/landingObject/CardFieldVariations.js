function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import FieldInput from "react-pe-scalars";
import { CardFieldTypes } from "../../views/LandingState/Card";

class CardFieldVariations extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "on", (value, field) => {
      // console.log(field, value)
      const state = { ...this.state
      };
      state.object[field] = value;
      console.log(state); //this.setState(state, field)

      setTimeout(() => {
        if (this.props.on) this.props.on(field, value);
      }, 100);
    });

    this.state = { ...props
    };
  }

  componentDidUpdate(newProps, newState) {
    const state = {};

    if (this.state.object.variant && this.state.object.variant != parseInt(newProps.object.variant)) {
      const cardField = CardFieldTypes().filter(e => e.type == this.state.object.type)[0] || CardFieldTypes()[0]; // console.log( cardField, parseInt(newProps.object.variant) );

      this.setState({
        object: { ...this.state.object,
          variant: parseInt(newProps.object.variant)
        }
      });
    }
  }

  render() {
    const variant = parseInt(this.state.object.variant);
    const cardField = CardFieldTypes().filter(e => e.type == this.state.object.type)[0] || CardFieldTypes()[0]; // console.log( this.state );

    const fields = [];

    for (let i = 1; i < 12; i++) {
      if (cardField.variants && cardField.variants[variant] && cardField.variants[variant][`variant_param_${i}`]) {
        const matrixData = cardField.variants[variant][`variant_param_${i}`]; //console.log( matrixData );

        fields.push( /*#__PURE__*/React.createElement(FieldInput, _extends({
          key: i,
          field: `variant_param_${i}`,
          _id: this.state.id,
          on: value => this.on(value, `variant_param_${i}`),
          onChange: value => this.on(value, `variant_param_${i}`),
          origin: this.state.origin // visibled_value={ visibled_value }

        }, matrixData, {
          editable: true,
          value: this.state.object[`variant_param_${i}`],
          vertical: false
        })));
      }
    }

    return /*#__PURE__*/React.createElement("div", {
      className: "p-1"
    }, fields);
  }

}

export default CardFieldVariations;