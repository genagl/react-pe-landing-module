function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";

class Border extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onTop", evt => {
      const marginTop = evt.currentTarget.value;
      this.setState({
        marginTop
      });
    });

    _defineProperty(this, "onLeft", evt => {
      const marginLeft = evt.currentTarget.value;
      this.setState({
        marginLeft
      });
    });

    _defineProperty(this, "onRight", evt => {
      const marginRight = evt.currentTarget.value;
      this.setState({
        marginRight
      });
    });

    _defineProperty(this, "onBottom", evt => {
      const marginBottom = evt.currentTarget.value;
      this.setState({
        marginBottom
      });
    });

    this.state = {
      borderTop: this.props.value.borderTop,
      borderRight: this.props.value.borderRight,
      borderBottom: this.props.value.borderBottom,
      borderLeft: this.props.value.borderLeft
    };
  }

  render() {
    // console.log( this.props );
    return /*#__PURE__*/React.createElement("div", {
      className: "d-flex flex-column p-5 mb-3"
    }, /*#__PURE__*/React.createElement("div", {
      className: "",
      style: {
        width: 200,
        position: "absolute",
        left: "50%",
        marginLeft: -100,
        height: 10,
        marginTop: 0
      }
    }));
  }

}

export default Border;