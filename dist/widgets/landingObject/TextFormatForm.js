function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { Slider } from "@blueprintjs/core";
import React, { Component } from "react";
import CSSSize from "../utilities/CSSSize";
import { __ } from "react-pe-utilities";
import FieldInput from "react-pe-scalars";
import DataContext from "../../views/LandingState/DataContext";

class TextFormatForm extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "on", (evt, field) => {
      const state = {
        value: { ...this.state.value
        }
      };
      state.value[field] = evt;
      this.setState(state);

      if (this.props.on) {
        this.props.on(this.props.field, state.value);
      }
    });

    _defineProperty(this, "onFontFamilyID", evt => {
      const {
        value
      } = evt.currentTarget;
      this.on(value, "fontFamilyID");
    });

    _defineProperty(this, "getFont", fontID => {
      const fonts = ["Open Sans", ...DataContext.data.landing.fonts.map(e => e.title)]; // console.log( fonts[ fontID ] );

      return fonts[fontID] ? `${fonts[fontID].split(":")[0]}, Open Sans` : "Open Sans";
    });

    this.state = { ...props,
      value: props.value ? props.value : {},
      example: props.example ? props.example : "Lorem ipsum"
    };
  }

  render() {
    console.log(this.state);
    let {
      value
    } = this.state;
    value = value || {};
    const {
      size,
      weight,
      letterSpace,
      fontFamilyID,
      color
    } = value;
    return /*#__PURE__*/React.createElement("div", {
      className: "row w-100 p-3 border-bottom  border-secondary"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-5 col-12"
    }, /*#__PURE__*/React.createElement("div", {
      className: "py-2"
    }, /*#__PURE__*/React.createElement("label", null, __("Font size"), " "), /*#__PURE__*/React.createElement(CSSSize, {
      value: size,
      on: value => this.on(value, "size")
    })), /*#__PURE__*/React.createElement("div", {
      className: "py-2"
    }, /*#__PURE__*/React.createElement("label", null, __("Font weight"), " "), /*#__PURE__*/React.createElement(Slider, {
      min: 100,
      max: 900,
      stepSize: 100,
      labelStepSize: 1000000,
      onChange: value => this.on(value, "weight"),
      value: parseInt(weight)
    })), /*#__PURE__*/React.createElement("div", {
      className: "py-2"
    }, /*#__PURE__*/React.createElement("label", null, __("Space between letters"), " "), /*#__PURE__*/React.createElement(Slider, {
      min: 0,
      max: 35,
      stepSize: 1,
      labelStepSize: 1000000,
      onChange: value => this.on(value, "letterSpace"),
      value: parseInt(letterSpace)
    })), /*#__PURE__*/React.createElement("div", {
      className: "py-2"
    }, /*#__PURE__*/React.createElement("label", null, __("Font family"), " "), this.getFontSelector()), /*#__PURE__*/React.createElement("div", {
      className: "py-2"
    }, /*#__PURE__*/React.createElement("label", null, __("Font color"), " "), /*#__PURE__*/React.createElement(FieldInput, {
      type: "color",
      field: "color",
      on: value => this.on(value, "color"),
      onChange: value => this.on(value, "color"),
      editable: true,
      value: color,
      vertical: true
    }))), /*#__PURE__*/React.createElement("div", {
      className: "col-md-7 col-12 d-flex align-items-center"
    }, /*#__PURE__*/React.createElement("div", {
      style: {
        textOverflow: "ellipsis",
        whiteSpace: "nowrap",
        overflow: "hidden",
        fontSize: this.state.value.size,
        fontWeight: weight,
        letterSpacing: letterSpace,
        fontFamily: this.getFont(fontFamilyID)
      }
    }, this.state.example)));
  }

  getFontSelector() {
    const fonts = ["Open Sans", ...DataContext.data.landing.fonts.map(e => e.title.split(":")[0])];
    return /*#__PURE__*/React.createElement("select", {
      className: "form-control ",
      value: this.state.value.fontFamilyID,
      onChange: this.onFontFamilyID
    }, fonts.map((e, i) => /*#__PURE__*/React.createElement("option", {
      value: i,
      key: i
    }, e)));
  }

}

export default TextFormatForm;