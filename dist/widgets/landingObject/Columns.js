function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";

class Columns extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "count", 13);

    _defineProperty(this, "onCount", evt => {
      const count = evt.currentTarget.getAttribute("n");
      this.setState({
        value: parseInt(count)
      });
      this.props.on(this.props.field, parseInt(count));
    });

    this.state = { ...props,
      value: props.value ? props.value : 0
    };
  }

  render() {
    // console.log(this.state);
    const {
      value
    } = this.state;
    const c = [];

    for (let i = 0; i <= this.count; i++) {
      const cl = i < value + 1 ? i === 0 ? " bg-secondary opacity_5 " : " bg-danger " : " bg-dark ";
      c.push( /*#__PURE__*/React.createElement("div", {
        className: ` w-25 h-100 mr-2${cl}`,
        n: i,
        onClick: this.onCount,
        key: i
      }));
    }

    return /*#__PURE__*/React.createElement("div", {
      className: "w-100 d-flex"
    }, /*#__PURE__*/React.createElement("div", {
      className: " pr-4 title"
    }, value), c);
  }

}

export default Columns;