function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { __ } from "react-pe-utilities";
import PaletteSingleForm from "./PaletteSingleForm";
import { sprintf } from "react-pe-utilities";
import PalettePresets from "../../views/LandingState/data/PalettePresets";
import { Button, Callout, Classes, Dialog, Intent } from "@blueprintjs/core";

class PaletteForm extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onDelete", i => {
      console.log(i);
      const value = [...this.state.value];
      value.splice(i, 1);
      this.setState({
        value
      });
      setTimeout(() => {
        if (this.props.on) this.props.on(value);
      }, 100);
    });

    _defineProperty(this, "onEdit", (element, field) => {
      let value = [...this.state.value];
      value = value.map((e, i) => {
        if (e.id === element.id) {
          e = element;
        }

        return e;
      });
      console.log(element);
      console.log(value);
      this.setState({
        value
      });
      setTimeout(() => {
        if (this.props.on) this.props.on(value);
      }, 100);
    });

    _defineProperty(this, "onCheck", (evt, element) => {
      //const check = evt.currentTarget.checked
      const presets = [...this.state.presets];
      const value = Array.isArray(this.state.value) ? [...this.state.value] : [];
      presets.forEach((preset, i) => {
        if (preset.id === element.id) {
          if (value.filter(preset => preset.id === element.id).length > 0) {
            value.splice(i, 1);
          } else {
            value.push(element);
          }

          presets[i].checked = !presets[i].checked;
        }
      });
      this.setState({
        presets,
        value
      });
      setTimeout(() => {
        if (this.props.on) this.props.on(value);
      }, 100);
    });

    _defineProperty(this, "onOpen", () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    });

    this.car = /*#__PURE__*/React.createRef();
    this.state = { ...props,
      presets: PalettePresets().map((e, i) => {
        if (Array.isArray(props.value) && props.value.filter(ee => ee.id === e.id).length > 0) {
          e.checked = true;
        }

        return e;
      })
    };
  }

  render() {
    // console.log( this.state.value )
    // const options = {
    //   loop: true,
    //   dots: false,
    //   margin: 3,
    //   nav: false,
    //   items: 1,
    // }
    // const events = {}
    const __sections = this.state.presets.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "col-md-6 py-4 ",
      key: i
    }, /*#__PURE__*/React.createElement(PaletteSingleForm, {
      e: e,
      i: i,
      id: e.id,
      isEdit: true,
      onCheck: evt => this.onCheck(evt, e)
    })));

    const value = Array.isArray(this.state.value) ? this.state.value : [];
    const checkeds = value.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "py-4",
      key: i
    }, /*#__PURE__*/React.createElement(PaletteSingleForm, {
      e: e,
      i: i,
      isEdit: false,
      onCheck: evt => this.onCheck(evt, e),
      onEdit: this.onEdit,
      onDelete: this.onDelete
    })));
    return /*#__PURE__*/React.createElement("div", {
      className: "w-100 pr-3"
    }, /*#__PURE__*/React.createElement("div", {
      className: "py-4"
    }, /*#__PURE__*/React.createElement(Callout, {
      className: " my-3",
      intent: Intent.WARNING
    }, __("You can select one or more style templates and edit them. Any Section can be styled for any of the working templates. This is done in the «Template» tab of the Sections pane."), /*#__PURE__*/React.createElement(Button, {
      onClick: this.onOpen,
      fill: true,
      large: true,
      className: "mt-3",
      intent: Intent.WARNING,
      minimal: true
    }, __("Choose presets")))), checkeds, /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isOpen,
      onClose: this.onOpen,
      className: "landing-outer-container",
      title: __("Choose presets")
    }, /*#__PURE__*/React.createElement("div", {
      className: Classes.DIALOG_BODY + " overflow-y-auto mx-0"
    }, /*#__PURE__*/React.createElement("div", {
      className: "p-5"
    }, /*#__PURE__*/React.createElement("div", {
      className: "container-fluid"
    }, /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-2"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-10"
    }, /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, __sections)))))), /*#__PURE__*/React.createElement("div", {
      className: Classes.DIALOG_FOOTER + " p-4 align-items-center d-flex justify-content-end"
    }, /*#__PURE__*/React.createElement("div", {
      className: "title lead"
    }, sprintf(__("Selected: %s Palettes"), value.length)), /*#__PURE__*/React.createElement("div", {
      className: Classes.DIALOG_FOOTER_ACTIONS
    }, /*#__PURE__*/React.createElement(Button, {
      onClick: this.onOpen
    }, __("close"))))));
  }

}

export default PaletteForm;