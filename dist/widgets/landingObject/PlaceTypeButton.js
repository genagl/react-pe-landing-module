function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { LayoutIcon } from 'react-pe-useful';
import signs from "../../views/LandingState/assets/img/landing/signs.svg";

class PlaceTypeButton extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onClick", () => {
      this.props.onClick(this.props.object);
    });
  }

  render() {
    //console.log(this.props.object)
    const style = {
      height: 120,
      width: 120,
      margin: 1
    };
    style.backgroundColor = this.props.object.color ? this.props.object.color : "#777";
    return /*#__PURE__*/React.createElement("div", {
      className: "square2 mr-1 btn-item",
      style: style,
      onClick: this.onClick
    }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(LayoutIcon, {
      src: signs,
      className: " layout-icon white"
    }), /*#__PURE__*/React.createElement("div", {
      className: "small text-white text-center"
    }, this.props.object.title)));
  }

}

export default PlaceTypeButton;