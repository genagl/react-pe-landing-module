function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { LayoutIcon } from 'react-pe-useful';
import { components } from "../../views/LandingState/data/components";

class SectionButton extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onClick", () => {
      if (this.props.onClick) this.props.onClick(this.props.object);
    });
  }

  render() {
    // console.log( this.props );
    const style = {
      height: 120,
      width: 120
    };
    let background = this.props.object.background ? `url(${this.props.object.background.image})` : null;

    switch (this.props.object.type) {
      case "image":
      default:
        background = this.props.object.data && this.props.object.data.src ? `url(${this.props.object.data.src})` : background;
        style.backgroundImage = background;
        break;
    }

    style.backgroundImage = background;
    return /*#__PURE__*/React.createElement("div", {
      className: "square2 bg-secondary mr-1 btn-item",
      style: style,
      onClick: this.onClick
    }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(LayoutIcon, {
      src: components()[this.props.object.type].icon,
      className: " layout-icon white"
    }), /*#__PURE__*/React.createElement("div", {
      className: "small text-white "
    }, components()[this.props.object.type].title)));
  }

}

export default SectionButton;