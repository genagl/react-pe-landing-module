function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";

class ContactFormVariant extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", { ...this.props
    });

    _defineProperty(this, "onLabelChange", evt => {
      const ddd = evt.currentTarget.value;
      this.setState({
        label: ddd
      });
      this.props.on("label", ddd, this.props.i);
    });

    _defineProperty(this, "onValueChange", evt => {
      const ddd = evt.currentTarget.value;
      this.setState({
        value: ddd
      });
      this.props.on("label", ddd, this.props.i);
    });

    _defineProperty(this, "onRemove", () => {});
  }

  render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "cf-variant"
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-3 layout-label"
    }, __("label")), /*#__PURE__*/React.createElement("input", {
      type: "string",
      className: "col-8 form-control input dark",
      value: this.state.label,
      onChange: this.onLabelChange
    }), /*#__PURE__*/React.createElement(Button, {
      className: "col-1",
      icon: "minus",
      onClick: this.onRemove
    })), /*#__PURE__*/React.createElement("div", {
      className: "row dat hidden"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-3 layout-label"
    }, __("Value")), /*#__PURE__*/React.createElement("input", {
      type: "string",
      className: "col-8 form-control input dark",
      value: this.state.value,
      onChange: this.onValueChange
    })));
  }

}

export default ContactFormVariant;