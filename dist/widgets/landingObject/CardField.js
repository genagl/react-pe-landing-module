function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, Intent, Dialog } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import FieldInput from "react-pe-scalars";
import { LayoutIcon } from 'react-pe-useful';
import { CardFieldTypes } from "../../views/LandingState/Card";
import matrix from "../../views/LandingState/data/matrix";
import InputForm from "../../views/LandingState/edit/InputForm";
import CardFieldVariations from "./CardFieldVariations";
import CardFieldMetaphors from "../../views/LandingState/card/CardFieldMetaphors";

class CardField extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onDClose", () => {
      this.setState({
        isDOpen: !this.state.isDOpen
      });
    });

    _defineProperty(this, "getSwitcher", () => {
      let current = "";
      const sletecter = CardFieldTypes().map((e, i) => {
        const isActive = e.type === this.state.object.type;

        if (isActive) {
          current = /*#__PURE__*/React.createElement("div", {
            i: i,
            type: e.type,
            className: "l-icon p-0 w-50px "
          }, /*#__PURE__*/React.createElement(Button, {
            onClick: this.onOpen,
            type: e.type,
            d: this.onSelectType
          }, /*#__PURE__*/React.createElement(LayoutIcon, {
            src: e.icon,
            className: "layout-icon p-1 w-30px "
          }), /*#__PURE__*/React.createElement("div", {
            className: "hidden"
          }, __(e.title))));
        } //console.log(e)


        return /*#__PURE__*/React.createElement("div", {
          className: `l-icon ${isActive ? " active " : ""}`,
          i: i,
          key: i,
          type: e.type,
          onClick: this.onType
        }, /*#__PURE__*/React.createElement(LayoutIcon, {
          src: e.icon,
          className: "layout-icon p-2 "
        }), /*#__PURE__*/React.createElement("div", {
          className: "smaller-text"
        }, __(e.title)));
      }); //console.log( this.state.origin )

      return /*#__PURE__*/React.createElement("div", {
        className: "d-flex"
      }, current, /*#__PURE__*/React.createElement(Dialog, {
        isOpen: this.state.isOpen,
        onClose: this.onOpen,
        title: this.state.currentType + " type!",
        className: "little2"
      }, /*#__PURE__*/React.createElement("div", {
        className: "p-5  dialog-content overflow-y-auto"
      }, sletecter, /*#__PURE__*/React.createElement(InputForm, _extends({}, this.state, {
        source: "CardField",
        id: this.state.id,
        data: this.state.object,
        on: (value, field) => this.onField(value, field, "CardField")
      })), /*#__PURE__*/React.createElement("div", {
        className: "p-1"
      }, this.getSwitchVariant()), /*#__PURE__*/React.createElement(CardFieldVariations, _extends({}, this.state, {
        data: this.state.object,
        on: this.onVariation
      })))));
    });

    _defineProperty(this, "getSwitchVariant", () =>
    /*#__PURE__*/

    /*
      return <select
    	  className="form-control input dark mb-1 "
    	  value={ this.state.object.variant }
    	  onChange={ this.onSelectVariant }
      >
    	  {
    		  matrix.CardField.variant.values.map((e, i) =>
    		  {
    			  if( !this.existsVariant( i ) ) return;
    			  let variant = this.getField().variants[ i ];
    			  return <option
    				  key={i}
    				  value={ e._id }
    				  onClick={ this.onVariant }
    
    			  >
    				  { __( variant ? variant.title : "" ) }
    			  </option>
    		  })
    	  }
      </select>
      */
    React.createElement(FieldInput, {
      title: __("Design variant"),
      field: "variant",
      type: "image_radio",
      _id: this.state.id,
      on: value => this.on(value, "variant"),
      onChange: this.onVariant,
      values: matrix.CardField.variant.values.filter((e, i) => this.existsVariant(i)).map((e, i) => ({ ...e,
        img: this.getField().variants[i].img,
        title: this.getField().variants[i].title,
        height: 30,
        icon_opacity: this.getField().variants[i].icon_opacity
      })),
      editable: true,
      value: this.state.object.variant,
      vertical: false
    }));

    _defineProperty(this, "onType", evt => {
      const type = evt.currentTarget.getAttribute("type");
      const object = {
        object: { ...this.state.object,
          type
        }
      };
      this.setState(object);
      this.onChange(object);
    });

    _defineProperty(this, "onHeight", evt => {
      const val = evt.currentTarget.value;
      const object = {
        object: { ...this.state.object,
          height: val
        }
      };
      this.setState(object);
      this.onChange(object);
    });

    _defineProperty(this, "onColor", color => {
      const val = color.hex;
      const object = {
        object: { ...this.state.object,
          color: val
        }
      };
      this.setState(object);
      this.onChange(object);
    });

    _defineProperty(this, "onColorValue", evt => {
      const val = evt.currentTarget.value;
      const object = {
        object: { ...this.state.object,
          color: val
        }
      };
      this.setState(object);
      this.onChange(object);
    });

    _defineProperty(this, "onSelectVariant", evt => {
      const variant = evt.currentTarget.value;
      const object = {
        object: { ...this.state.object,
          variant
        }
      };
      this.setState(object);
      this.onChange(object);
    });

    _defineProperty(this, "onVariant", variant => {
      const object = {
        object: { ...this.state.object,
          variant
        }
      };
      this.setState(object);
      this.onChange(object);
    });

    _defineProperty(this, "onChange", object => {
      setTimeout(() => {
        if (this.props.onChange) this.props.onChange(object.object, this.props.i);
      }, 100);
    });

    _defineProperty(this, "onClose", () => {
      this.setState({
        isDOpen: !this.state.isDOpen
      });
      this.props.onClose(this.props.i);
    });

    _defineProperty(this, "on", value => {
      //console.log(value)
      this.props.on(value);
    });

    _defineProperty(this, "onOpen", evt => {
      //const currentType = evt.carrentTarget ? evt.carrentTarget.getAttribute("type") : ""
      this.setState({
        isOpen: !this.state.isOpen
      });
    });

    _defineProperty(this, "onVariation", (field, value) => {//console.log(field, value)
      //this.onField(value, field, "CardField")
    });

    this.state = { ...this.props,
      type: props.type || "string"
    };
    this.ref = /*#__PURE__*/React.createRef(); // console.log( this.state );
  }

  componentWillUpdate(nextProps, nextState) {
    const state = {};
    Object.keys({ ...this.state
    }) // .filter( e => !this.state[e].hidden )
    .forEach((e, i) => {
      if (nextProps[e] !== this.state[e] && typeof nextProps[e] != "undefined") {
        // console.log(e, nextProps[e]);
        state[e] = nextProps[e];
      }

      if (nextState[e] !== this.state[e] && typeof nextState[e] != "undefined") {
        // console.log(e, nextState[e]);
        state[e] = nextState[e];
      }
    });

    if (Object.keys(state).length > 0) {
      this.setState(state);
    }
  }

  render() {
    // console.log(this.state);
    return /*#__PURE__*/React.createElement("div", {
      className: " w-100 layout-centered flex-column p-2 m-0 position-relative border border-light " + this.state.object.className,
      style: {
        //...getStyle( this.state.object.style ),
        backgroundColor: this.state.object.bgcolor,
        minHeight: this.state.object.height ? `${this.state.object.height}px` : "50px"
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: ` landing-card-field border-0 ${this.getField().type}${this.state.object.variant}`,
      style: {}
    }, this.getExample()), /*#__PURE__*/React.createElement("div", {
      className: "position-absolute d-flex align-items-end",
      style: {
        zIndex: 10000
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "mb-2"
    }, this.getSwitcher()), /*#__PURE__*/React.createElement("div", {
      className: "mb-2 w-100 "
    })), /*#__PURE__*/React.createElement(Button, {
      className: "close top-right m-2",
      style: {
        zIndex: 5
      },
      icon: "cross",
      onClick: this.onDClose
    }), /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isDOpen,
      onClose: this.onDClose,
      className: "little"
    }, /*#__PURE__*/React.createElement("div", {
      className: "p-5"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text-center mb-4"
    }, __("Delete card's field?")), /*#__PURE__*/React.createElement("div", {
      className: "d-flex justify-content-center"
    }, /*#__PURE__*/React.createElement(Button, {
      intent: Intent.DANGER,
      onClick: this.onClose
    }, __("Yes")), /*#__PURE__*/React.createElement(Button, {
      intent: Intent.NONE,
      onClick: this.onDClose
    }, __("No"))))));
  }

  getMetaphorTitle(metafor) {
    const m = metafor ? metafor : "";
    let title = CardFieldMetaphors().filter(e => e._id === m)[0];
    return title ? __(title.title) : "";
  }

  getExample() {
    //console.log(this.state.object.metafor)
    //const field = this.getField()
    switch (this.state.object.type) {
      case "media":
        const stl = {
          backgroundImage: "url(/assets/img/employee.svg)",
          height: `${this.state.object.height}px`
        };

        if (this.state.object.variant === 1) {
          stl.minWidth = `${this.state.object.height}px`;
          stl.width = `${this.state.object.height}px`;
          stl.marginLeft = -parseInt(this.state.object.height) / 2;
        }

        return /*#__PURE__*/React.createElement("div", {
          className: " media ",
          style: stl
        }, /*#__PURE__*/React.createElement("div", {
          className: "landing-card-field-example-string title text-uppercase p-2 bg-light"
        }, this.getMetaphorTitle(this.state.object.metafor)));

      case "check":
        return /*#__PURE__*/React.createElement("div", {
          className: "d-flex flex-column w-100"
        }, /*#__PURE__*/React.createElement("div", {
          className: "landing-card-field-example-string title text-uppercase p-2"
        }, this.getMetaphorTitle(this.state.object.metafor)), /*#__PURE__*/React.createElement("div", {
          className: " check hidden "
        }, /*#__PURE__*/React.createElement("i", {
          className: "fas fa-check text-success pr-1"
        }), /*#__PURE__*/React.createElement("span", null, "Lorem ipsum")));

      case "personal_links":
        return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
          className: "landing-card-field-example-string title text-uppercase p-2"
        }, this.getMetaphorTitle(this.state.object.metafor)), /*#__PURE__*/React.createElement("div", {
          className: " personal_links "
        }, /*#__PURE__*/React.createElement("a", {
          className: "lcard-pl",
          href: "#"
        }, /*#__PURE__*/React.createElement("i", {
          className: "fab fa-vk"
        })), /*#__PURE__*/React.createElement("a", {
          className: "lcard-pl",
          href: "#"
        }, /*#__PURE__*/React.createElement("i", {
          className: "fab fa-facebook-f"
        }))));

      case "outerlink":
      case "navlink":
        // console.log(this.state.object.variant)
        // console.log(this.getVariant())
        return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
          className: "landing-card-field-example-string title text-uppercase p-2"
        }, this.getMetaphorTitle(this.state.object.metafor)));

      default:
        // console.log( this.state.object.variant );
        // console.log( this.getVariant() );
        return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
          className: "landing-card-field-example-string title text-uppercase p-2"
        }, this.getMetaphorTitle(this.state.object.metafor)), /*#__PURE__*/React.createElement("div", {
          className: "landing-card-field-example-string hidden"
        }, this.getVariant() ? this.getVariant().example : "--"));
    }
  }

  getField() {
    let field = CardFieldTypes().filter(ee => ee.type === this.state.object.type)[0];
    field = field ? field : CardFieldTypes()[0];
    return field;
  }

  existsVariant(i) {
    // console.log( i, this.getField().variants[ i ] );
    return this.getField().variants && typeof this.getField().variants[i] != "undefined";
  }

  getVariant() {
    return this.getField().variants && this.getField().variants.length > 0 ? this.getField().variants[parseInt(this.state.object.variant)] ? this.getField().variants[parseInt(this.state.object.variant)] : this.getField().variants[0] : this.getField().variants[0];
  }

  onField(value, field, type) {
    // console.log(value, field, type);
    const object = {
      object: { ...this.state.object
      }
    };
    object.object[field] = value;
    this.setState(object);
    this.onChange(object);
  }

}

export default CardField;