function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { NumericInput, Position } from "@blueprintjs/core";

class CSSSize extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "getPostfix", (string = "0") => {
      let postfix = "px";
      const str = string.toString();
      postfixes().forEach(e => {
        if (str.indexOf(e.id) > 0) {
          postfix = e.id;
        }
      });
      return postfix;
    });

    _defineProperty(this, "onPostfix", evt => {
      const {
        value
      } = evt.currentTarget;
      this.on("postfix", value);
    });

    _defineProperty(this, "onNumeric", value => {
      this.on("numeric", value);
    });

    _defineProperty(this, "on", (field, value) => {
      const state = { ...this.state
      };
      state[field] = value;
      state.numeric = ["px", "%", "vh", "vw"].filter(e => e == this.state.postfix).length > 0 ? parseInt(state.numeric) : state.numeric;
      state.value = state.numeric + state.postfix;
      this.setState(state);

      if (this.props.on) {
        setTimeout(() => this.props.on(state.value), 100);
      }
    });

    this.state = {
      value: props.value ? props.value : "1px",
      numeric: parseFloat(props.value ? props.value : "1px"),
      postfix: this.getPostfix(props.value ? props.value : "1px")
    };
  }

  render() {
    const {
      numeric,
      postfix,
      min,
      max,
      value
    } = this.state; // console.log(numeric);

    return /*#__PURE__*/React.createElement("div", {
      className: "d-flex css-style-for w-100"
    }, /*#__PURE__*/React.createElement(NumericInput, {
      value: numeric,
      min: min || null,
      max: max || 10000000,
      onValueChange: this.onNumeric,
      buttonPosition: Position.LEFT,
      stepSize: ["px", "%", "vh", "vw"].filter(e => e == postfix).length > 0 ? 1 : 0.1,
      className: " "
    }), /*#__PURE__*/React.createElement("select", {
      className: "form-control ",
      value: postfix,
      onChange: this.onPostfix
    }, postfixes().map((e, i) => /*#__PURE__*/React.createElement("option", {
      key: e.id,
      value: e.id
    }, e.id))));
  }

}

export default CSSSize;
export function postfixes() {
  return [{
    id: "px"
  }, {
    id: "em"
  }, {
    id: "rem"
  }, {
    id: "%"
  }, {
    id: "vh"
  }, {
    id: "vw"
  }];
}