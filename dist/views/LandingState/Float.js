function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, Intent, Dialog, ButtonGroup, Popover } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import Title from "./Title";
import FloatSetting from "./edit/FloatSetting";
import { components } from "./data/components";

class Float extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", { ...this.props,
      current_type: this.props.type,
      dwidth: document.body.clientWidth,
      isOpen: false,
      isShowReg: false
    });

    _defineProperty(this, "updateWindowDimensions", () => {
      this.setState({
        dwidth: document.body.clientWidth,
        dheight: document.body.clientHeight
      });
    });

    _defineProperty(this, "onDailogHandler", () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    });

    _defineProperty(this, "onMouseOut", evt => {
      const rect = evt.currentTarget.getBoundingClientRect();
      const usl = evt.clientX + window.scrollX > rect.left + window.scrollX && evt.clientX + window.scrollX < rect.right + window.scrollX + 124 && evt.clientY + window.scrollY > rect.top + window.scrollY && evt.clientY + window.scrollY < rect.bottom + window.scrollY; // console.log( rect.right + 124, usl );

      if (usl) return;
      this.setState({
        isShowReg: false
      });
    });

    _defineProperty(this, "onDelete", () => {
      this.setState({
        isShowDelete: !this.state.isShowDelete
      }); //
    });

    _defineProperty(this, "onRemoveFloat", () => {
      this.props.onRemoveFloat(this.state.float_id);
    });

    _defineProperty(this, "onUpdate", (data, float_id) => {
      console.log(data);
      this.props.onUpdate(data, float_id);
      this.onDailogHandler();
    });
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  componentDidUpdate(nextProps) {
    // console.log(nextProps);
    let isUpdate = false;
    const state = {};
    ["is_edit", "data", "class_name", "style", "type", "position"].forEach(e => {
      if (nextProps[e] != this.state[e]) {
        isUpdate = true;
        state[e] = nextProps[e];
      }
    });

    if (isUpdate) {
      // console.log(state);
      this.setState(state);
    }
  }

  render() {
    const {
      class_name,
      is_edit,
      type,
      isShowReg,
      isShowDelete
    } = this.state; // console.log( is_edit );

    const ElComponent = type ? components()[type].c : Title; //const size = "mc"

    const editble = is_edit && (isShowReg || isShowDelete) ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "float-btns"
    }, /*#__PURE__*/React.createElement("div", {
      className: " layout-centered "
    }, /*#__PURE__*/React.createElement(Button, {
      icon: "annotation",
      intent: Intent.NONE,
      className: "",
      onClick: this.onDailogHandler
    }), /*#__PURE__*/React.createElement(Button, {
      icon: "move",
      intent: Intent.NONE,
      className: "hidden"
    }), /*#__PURE__*/React.createElement(Popover, {
      isOpen: isShowDelete,
      content: /*#__PURE__*/React.createElement("div", {
        className: "p-3"
      }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
        className: "mb-2"
      }, __("Delete this Float?")), /*#__PURE__*/React.createElement(ButtonGroup, {
        className: "w-100"
      }, /*#__PURE__*/React.createElement(Button, {
        intent: Intent.NONE,
        fill: true,
        onClick: this.onRemoveFloat
      }, __("Yes")), /*#__PURE__*/React.createElement(Button, {
        icon: "cross",
        onClick: this.onDelete,
        intent: Intent.DANGER
      }))))
    }, /*#__PURE__*/React.createElement(Button, {
      icon: "cross",
      intent: Intent.DANGER,
      className: "",
      onClick: this.onDelete
    }))))) : null;
    return /*#__PURE__*/React.createElement("div", {
      id: `float-${this.state.float_id}`,
      className: `landing-fload ${is_edit ? " edit " : ""}${class_name}`,
      style: {
        // ...style,
        position: "absolute",
        left: this.getDst("x") === "L" ? this.getAttr("x") : "auto",
        right: this.getDst("x") === "R" ? this.getAttr("x") : "auto",
        top: this.getDst("y") === "T" ? this.getAttr("y") : "auto",
        bottom: this.getDst("y") === "B" ? this.getAttr("y") : "auto",
        width: this.getAttr("w"),
        height: this.getAttr("h")
      },
      onMouseEnter: event => this.setState({
        isShowReg: true
      }),
      onMouseOut: this.onMouseOut
    }, /*#__PURE__*/React.createElement(ElComponent, _extends({}, this.props, {
      columns: 0,
      is_edit: is_edit,
      level: this.props.level + 1
    })), editble, /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isOpen,
      onClose: this.onDailogHandler,
      className: "little2",
      title: __("Edit Float")
    }, /*#__PURE__*/React.createElement("div", {
      className: "p-0"
    }, /*#__PURE__*/React.createElement(FloatSetting, {
      float_id: this.props.float_id,
      onChange: this.onUpdate
    }))));
  }

  getScreenSize() {
    let p = "mc";

    if (this.state.dwidth > 940) {
      p = "lg";
    } else if (this.state.dwidth > 760) {
      p = "xl";
    } else if (this.state.dwidth > 560) {
      p = "sm";
    }

    return p;
  }

  getDst(coord) {
    const {
      position
    } = this.props;

    if (!position) {
      return;
    }

    const p = this.getScreenSize(); // console.log( coord, p,  position.mc[coord], position );

    return position[p][coord] ? position[p][coord].dst : position.mc[coord].dst;
  }

  getAttr(coord) {
    const {
      position
    } = this.props;
    const p = this.getScreenSize();

    if (!position) {
      return;
    } // console.log( coord, p, position[p], position[p][coord] );


    const ei = position[p][coord] ? position[p][coord].ei : position.mc[coord].ei;
    let coo = position[p][coord] ? position[p][coord].value : position.mc[coord].value;
    coo = isNaN(parseInt(coo)) ? coo : parseInt(coo);
    coo = ei === "%" ? `${coo}%` : coo; // console.log( coord,  position.mc[coord].ei, ei, position, coo );

    return coo;
  }

}

export default Float;