import React, { Component } from "react";
import { Icon } from "@blueprintjs/core";

class TeamMember extends Component {
  render() {
    const {
      avatar,
      name,
      description,
      class_name,
      style
    } = this.props;
    return avatar || name ? /*#__PURE__*/React.createElement("div", {
      className: `l-col position-relative py-4 ${class_name}`,
      style: { ...style
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "ava ",
      style: {
        backgroundImage: `url(${avatar})`
      }
    }, this.props.is_edit && false ? /*#__PURE__*/React.createElement("div", {
      className: "l-inline-edit-btn"
    }, /*#__PURE__*/React.createElement(Icon, {
      icon: "annotation"
    })) : null), /*#__PURE__*/React.createElement("div", {
      className: "name"
    }, name), /*#__PURE__*/React.createElement("div", {
      className: "description",
      dangerouslySetInnerHTML: {
        __html: description
      }
    })) : null;
  }

}

export default TeamMember;