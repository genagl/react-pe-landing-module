function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { getStyle } from "../Section";

class CardTail extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "top_plate", () => {
      const {
        tail_class,
        tail_style,
        tail_text
      } = this.props;
      return /*#__PURE__*/React.createElement("div", {
        className: "landing-card-tail ",
        dangerouslySetInnerHTML: {
          __html: tail_text
        }
      });
    });

    this.state = { ...props
    };
  }

  render() {
    const {
      tail_type
    } = this.props;

    switch (tail_type) {
      case "origamu":
        return "";

      case "top_plate":
        return this.top_plate();

      case "none":
      default:
        return "";
    }
  }

}

export default CardTail;