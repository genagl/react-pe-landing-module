export default function () {
  return [{
    _id: "empty",
    title: "Dummy",
    type: "empty",
    fill: false
  }, {
    _id: "divider",
    title: "Divider",
    type: "divider",
    fill: false
  }, {
    _id: "title",
    title: "Title",
    type: "string",
    fill: true
  }, {
    _id: "description",
    title: "Description",
    type: "string",
    fill: true,
    edit_field: "html"
  }, {
    _id: "order",
    title: "Order",
    type: "string",
    fill: true
  }, {
    _id: "date",
    title: "Date",
    type: "string",
    fill: true
  }, {
    _id: "commentary",
    title: "Commentary",
    type: "string",
    fill: true,
    edit_field: "html"
  }, {
    _id: "author",
    title: "Author",
    type: "string",
    fill: true
  }, {
    _id: "author_meta",
    title: "Author's meta",
    type: "string",
    fill: true
  }, {
    _id: "offer_status",
    title: "Offer's status",
    description: "For example: «standart», «comfort», «premium»",
    type: "string",
    fill: true
  }, {
    _id: "offer_definition",
    title: "Offer's definition",
    description: "For example: «new­», «special», «discont»",
    type: "string",
    fill: true
  }, {
    _id: "thumbnail",
    title: "Thumbnail",
    type: "media",
    fill: true
  }, {
    _id: "price",
    title: "Price",
    type: "price",
    fill: true
  }, {
    _id: "old_price",
    title: "Old price",
    type: "price",
    fill: true
  }, {
    _id: "features",
    title: "List of features",
    type: "check_label",
    fill: true
  }, {
    _id: "personal_links",
    title: "Social links",
    type: "personal_links",
    fill: true
  }, {
    _id: "icon",
    title: "Icon",
    type: "icon",
    fill: true
  }, {
    _id: "tag",
    title: "Tag",
    type: "navlink",
    fill: true
  }, {
    _id: "cart",
    title: "Cart button",
    type: "navlink",
    fill: true
  }, {
    _id: "navlink",
    title: "Inner link",
    type: "navlink",
    fill: true
  }, {
    _id: "outerlink",
    title: "Outer link",
    type: "outerlink",
    fill: true
  }];
}