import { Callout, Dialog, Icon } from "@blueprintjs/core";
import React from "react";
import { __ } from "react-pe-utilities";

const Outer = props => {
  return /*#__PURE__*/React.createElement(Dialog, {
    className: "landing-outer-container",
    isOpen: props.isOuterOpen
  }, props.outerURL ? /*#__PURE__*/React.createElement("iframe", {
    src: props.outerURL,
    style: {
      width: "100%",
      height: "100%",
      border: "none"
    }
  }) : /*#__PURE__*/React.createElement(Callout, {
    className: " p-5 text-center h-100 d-flex align-items-center justify-content-center title"
  }, __("No url exists")), /*#__PURE__*/React.createElement("div", {
    className: "landing-outer-close",
    onClick: props.onOpen
  }, /*#__PURE__*/React.createElement(Icon, {
    icon: "cross"
  })));
};

export default Outer;