import React, { Component } from "react";
import Dot from "./Dot";
import Moment from "react-moment";
import MomentLocaleUtils, { formatDate, parseDate } from "react-day-picker/moment";
import "react-day-picker/lib/style.css";
import DayPicker from "react-day-picker";
import moment from "moment";
import "moment/locale/ru";
import $ from "jquery";
import spile from "../assets/img/spile.svg";
import { getFontNameByID } from "../data/PalettePresets";

class SpileDot extends Dot {
  constructor(props) {
    super(props);
    this.state = {
      height: 100,
      maxH: props.maxH
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.maxH !== this.state.maxH) {
      // console.log( nextProps );
      this.setState({
        maxH: nextProps.maxH
      });
    }
  }

  renderDot() {
    const e = this.props;
    const time = e.date ? /*#__PURE__*/React.createElement(Moment, {
      locale: "ru",
      format: "D MMMM YYYY"
    }, new Date(e.date)) : null;
    const {
      title
    } = e;
    const {
      description
    } = e;
    const {
      palette
    } = e;
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "landing-time-top",
      style: {
        minHeight: this.state.maxH
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-time-line-dot-title"
    })), /*#__PURE__*/React.createElement("div", {
      className: "landing-time-line-dot-dot",
      style: {}
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-time-line-dot-dot-dot",
      style: {
        width: e.dots_size ? e.dots_size : 120,
        height: e.dots_size ? e.dots_size : 120,
        backgroundColor: e.backgroundColor,
        alignItems: "flex-start",
        borderWidth: 10,
        marginTop: e.height
      }
    }, this.splie())), /*#__PURE__*/React.createElement("div", {
      className: "landing-time-bottom p-2",
      style: {
        minHeight: this.state.maxH
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-time-line-dot-sub-title",
      dangerouslySetInnerHTML: {
        __html: title
      },
      style: {
        color: palette.card.color,
        ...palette.card.title,
        fontFamily: getFontNameByID(palette.card.title.fontFamilyID)
      }
    }), /*#__PURE__*/React.createElement("div", {
      className: "landing-time-line-dot-description",
      dangerouslySetInnerHTML: {
        __html: description
      },
      style: {
        color: palette.card.color
      }
    })));
  }

  splie() {
    const {
      color,
      dots_size,
      height,
      thumbnail,
      icon,
      is_shadow,
      palette
    } = this.props;
    const st0 = {
      fill: "url(#SVGID_1_)"
    };
    const st3 = {
      fill: "url(#SVGID_3_)"
    };
    const st1 = {
      fill: color || "grey"
    };
    const st2 = {
      opacity: 0.18
    };
    const fw = dots_size - 40;
    const prpr = fw / 215;
    const yy = 107.7 * prpr;
    const hh = height ? height * prpr : 100;
    const fh = 768 * prpr;
    const fh2 = 107 * prpr;
    const shadow = is_shadow && false ? /*#__PURE__*/React.createElement("svg", {
      version: "1.1",
      xmlns: "http://www.w3.org/2000/svg",
      x: "0px",
      y: "0px",
      width: fw,
      height: 500 * prpr,
      viewBox: "0 0 215 768",
      style: {
        top: 0,
        position: "absolute"
      }
    }, /*#__PURE__*/React.createElement("g", {
      id: "shadow"
    }, /*#__PURE__*/React.createElement("linearGradient", {
      id: "SVGID_1_",
      gradientUnits: "userSpaceOnUse",
      x1: "107.7939",
      y1: "611.5692",
      x2: "107.7939",
      y2: "280.6064"
    }, /*#__PURE__*/React.createElement("stop", {
      offset: "3.276967e-02",
      style: {
        stopColor: "#00000000"
      }
    }), /*#__PURE__*/React.createElement("stop", {
      offset: "0.848",
      style: {
        stopColor: "#00000042"
      }
    })), /*#__PURE__*/React.createElement("rect", {
      style: st0,
      x: "0.6",
      y: hh + yy,
      width: "214.4",
      height: "631"
    }))) : null;
    return /*#__PURE__*/React.createElement("div", {
      className: "position-relative",
      style: {
        marginLeft: -fw + 0,
        marginTop: -10 - height
      }
    }, shadow, /*#__PURE__*/React.createElement("svg", {
      version: "1.1",
      xmlns: "http://www.w3.org/2000/svg",
      x: "0px",
      y: "0px",
      width: fw,
      height: fh2,
      viewBox: "0 0 215 107",
      style: {
        top: height + yy - 1,
        position: "absolute"
      }
    }, /*#__PURE__*/React.createElement("g", {
      id: "bottom_1_"
    }, /*#__PURE__*/React.createElement("path", {
      style: st1,
      d: "M 107.5, 107.5 C 166.9, 107.5, 215, 59.4, 215, 0 H 0 C 0, 59.4, 48.1, 107.5, 107.5, 107.5 z"
    })), /*#__PURE__*/React.createElement("g", {
      id: "bottom_1_D"
    }, /*#__PURE__*/React.createElement("path", {
      style: st2,
      d: "M 107.5, 107.5 C 166.9, 107.5, 215, 59.4, 215, 0 H 0 C 0, 59.4, 48.1, 107.5, 107.5, 107.5 z"
    }))), /*#__PURE__*/React.createElement("svg", {
      version: "1.1",
      xmlns: "http://www.w3.org/2000/svg",
      x: "0px",
      y: "0px",
      width: fw,
      height: fh,
      viewBox: "0 0 215 768",
      style: {
        top: 0,
        position: "absolute"
      }
    }, /*#__PURE__*/React.createElement("g", {
      id: "bevel_1_"
    }, /*#__PURE__*/React.createElement("rect", {
      y: "107.7",
      style: st1,
      width: "215",
      height: height / prpr
    })), /*#__PURE__*/React.createElement("g", {
      id: "bevel_x5F_dark"
    }, /*#__PURE__*/React.createElement("linearGradient", {
      id: "SVGID_2_",
      gradientUnits: "userSpaceOnUse",
      x1: "107.5",
      y1: "280.6064",
      x2: "107.5",
      y2: "107.5"
    }, /*#__PURE__*/React.createElement("stop", {
      offset: "6.830000e-02",
      style: {
        stopColor: "#000000"
      }
    }), /*#__PURE__*/React.createElement("stop", {
      offset: "1",
      style: {
        stopColor: "#000000"
      }
    })), /*#__PURE__*/React.createElement("rect", {
      y: "107.7",
      style: st2,
      width: "215",
      height: height / prpr
    })), /*#__PURE__*/React.createElement("g", {
      id: "round"
    }, /*#__PURE__*/React.createElement("circle", {
      style: st1,
      cx: "107.5",
      cy: "107.5",
      r: "107.5"
    }))), /*#__PURE__*/React.createElement("div", {
      className: "landing-time-line-dot-thumb",
      style: {
        backgroundImage: `url(${thumbnail})`,
        top: 25,
        left: 25,
        width: fh2 * 2 - 50,
        height: fh2 * 2 - 50
      }
    }, this.renderArrow()), /*#__PURE__*/React.createElement("div", {
      className: "landing-time-line-dot-icon",
      style: {
        backgroundImage: `url(${icon})`,
        position: "absolute",
        top: -48
      }
    }));
  }

}

export default SpileDot;