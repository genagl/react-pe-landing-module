function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import $ from "jquery";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import SectionContent from "./SectionContent";
import { __ } from "react-pe-utilities";
import Section, { getDefault } from "./Section";

class ZoomPanPinch extends SectionContent {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "up", evt => {
      $("body,html").animate({
        scrollTop: this.ref.current.getBoundingClientRect().top + window.scrollY - 60
      }, 400);
    });

    _defineProperty(this, "dwn", evt => {
      // const h = this.ref.current.getBoundingClientRect().height;
      $("body,html").animate({
        scrollTop: this.ref.current.getBoundingClientRect().bottom + window.scrollY - 60
      }, 400);
    });

    _defineProperty(this, "onEdit", (data, id) => {
      console.log("onEdit", id, data, this.state);
      const sections = [...this.state.data.sections];
      const secs = [];
      sections.forEach(e => {
        if (e.id == data.id) {
          secs.push(data);
        } else {
          secs.push(e);
        }
      });
      this.setState({
        data: { ...this.state.data,
          sections: secs
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections: secs
        }
      }, this.props.id);
    });

    _defineProperty(this, "onUp", data => {
      console.log("onUp", data, this.state);
      const sections = [...this.state.data.sections];
      const sec = { ...sections[data]
      };
      sections.splice(data, 1);
      sections.splice(data - 1, 0, sec);
      console.log(sections);
      this.setState({
        data: { ...this.state.data,
          sections
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onDn", data => {
      console.log("onDn", data, this.state);
      const sections = [...this.state.data.sections];
      const sec = { ...sections[data]
      };
      sections.splice(data, 1);
      sections.splice(data + 1, 0, sec);
      console.log(sections);
      this.setState({
        data: { ...this.state.data,
          sections
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onAdd", data => {
      console.log("onAdd", data, this.state);
      const sections = [...this.state.data.sections];
      const sec = getDefault();
      sections.splice(data + 1, 0, sec);
      console.log(sections);
      this.setState({
        data: { ...this.state.data,
          sections
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onRnv", data => {
      console.log("onRnv", data, this.state.data.sections);
      const sections = [...this.state.data.sections];
      sections.splice(data, 1);
      console.log(sections);
      this.setState({
        data: { ...this.state.data,
          sections
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onHide", (id, is_hide) => {
      console.log("HIDE", id, is_hide);
    });

    _defineProperty(this, "onRemoveFloat", float_id => {});

    _defineProperty(this, "onUpdateFloat", (data, float_id, section_id) => {});
  }

  getState() {
    this.ref = /*#__PURE__*/React.createRef();
    return {};
  }

  is() {
    const {
      section
    } = this.state.data;
    return section;
  }

  renderContent() {
    const {
      type
    } = this.props;
    const {
      class_name,
      style,
      section,
      maxScale
    } = this.props.data;
    return /*#__PURE__*/React.createElement("div", {
      className: "landing-zoom-pan-pinch-cont ",
      style: { ...style,
        width: this.state.width
      },
      ref: this.ref
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-zoom-pan-pinch",
      style: {
        width: this.state.width,
        height: this.state.height
      }
    }, /*#__PURE__*/React.createElement(TransformWrapper, {
      defaultScale: 1,
      defaultPositionX: 0,
      defaultPositionY: 0,
      options: {
        maxScale
      }
    }, ({
      zoomIn,
      zoomOut,
      resetTransform,
      ...rest
    }) => /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "tools"
    }, /*#__PURE__*/React.createElement("button", {
      onClick: zoomIn
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-search-plus fa-2x"
    })), /*#__PURE__*/React.createElement("button", {
      onClick: zoomOut
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-search-minus fa-2x"
    })), /*#__PURE__*/React.createElement("button", {
      onClick: resetTransform
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-times fa-2x"
    })), /*#__PURE__*/React.createElement("button", {
      onClick: this.up
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-caret-up fa-2x"
    })), /*#__PURE__*/React.createElement("button", {
      onClick: this.dwn
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-caret-down fa-2x"
    }))), /*#__PURE__*/React.createElement(TransformComponent, null, section.map((e, i) => {
      const estyle = { ...this.getStyle(e.style),
        width: "100%"
      };
      return /*#__PURE__*/React.createElement("div", {
        style: estyle,
        className: e.className,
        key: i
      }, /*#__PURE__*/React.createElement(Section, _extends({}, e, {
        style: {
          height: "100%",
          ...this.getStyle(e.style)
        },
        i: i,
        user: this.props.user,
        is_edit: this.state.is_edit,
        level: this.props.level + 1,
        onEdit: this.onEdit,
        onUp: this.onUp,
        onDn: this.onDn,
        onAdd: this.onAdd,
        onRnv: this.onRnv
      })));
    }))))));
  }

}

export default ZoomPanPinch;