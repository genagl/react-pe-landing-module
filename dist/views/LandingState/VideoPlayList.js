function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Fragment } from "react";
import { withApollo } from "react-apollo";
import { compose } from "recompose";
import { __ } from "react-pe-utilities";
import Video, { getVideoThumbnail } from "./Video";
import SectionContent from "./SectionContent";
import DataContext from "./DataContext";
import { getQueryArgs, getQueryName, querySingle, queryCollection } from "react-pe-layouts";
import { getFontNameByID } from "./data/PalettePresets";

class VideoPlayList extends SectionContent {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onVimeoError", err => {
      console.error(err);
    });

    _defineProperty(this, "onChoose", i => {
      this.setState({
        current: i
      });
    });
  }

  is() {
    const {
      video
    } = this.state.data;
    return Array.isArray(video) && video.length > 0 || this.state.data.src_type != "here";
  }

  getState() {
    const {
      current,
      default_video,
      height
    } = this.props.data;
    return {
      height: height ? height : 350,
      current: default_video ? -1 : typeof current == "undefined" ? 0 : this.props.current
    };
  }

  componentDidMount() {
    if (this.props.data.src_type != "server" || !this.props.data.data_type) return;
    const query_name = getQueryName(this.props.data.data_type);
    const query_args = getQueryArgs(this.props.data.data_type);
    const query = queryCollection(this.props.data.data_type, query_name, query_args, "{order:\"asc\"}"); // console.log(query);

    this.props.client.query({
      query
    }).then(result => {
      const res = result.data.getPEVideoPoints.map(e => ({ ...e,
        title: e.post_title
      })); // console.log( res );

      this.setState({
        data: { ...this.state.data,
          video: res
        }
      });
    });
  }

  renderContent(style) {
    const {
      palette
    } = this.props;
    const {
      width,
      height,
      title,
      is_sort_by_group,
      is_sign_by_group,
      default_video
    } = this.props.data;
    const videos = Array.isArray(this.props.data.video) ? [...this.props.data.video] : [];

    if (is_sort_by_group && DataContext.data.landing.video_group) {
      const vgs = DataContext.data.landing.video_group.map((e, i) => ({
        unique: e.unique,
        i
      }));
      /*
      videos.sort((a,b) =>
      {
        let avg = vgs.filter(e => e.unique == a.video_group)[0]
          ?
          vgs.filter(e => e.unique == a.video_group)[0].i
          :
          0;
        let bvg = vgs.filter(e => e.unique == b.video_group)[0]
          ?
          vgs.filter(e => e.unique == b.video_group)[0].i
          :
          0;
        //console.log(avg > bvg, avg, bvg, a.video_group, b.video_group, vgs );
        return avg - bvg;
      })
      //console.log( videos );
      */
    }

    let prev_btn_style = {}; // console.log( this.props.data )

    const buttonList = videos ? videos.map((e, i) => {
      let btn_style;
      let pr;
      let groupHead;

      if (DataContext.data.landing.video_group) {
        const vg = DataContext.data.landing.video_group.filter((e2, i2) => e2.unique == e.video_group); // console.log( vg );

        pr = vg && vg[0] ? `${vg[0].color}50` : "transparent";

        if (is_sign_by_group) {
          btn_style = {
            backgroundColor: pr
          };
        }

        if (is_sort_by_group && pr != prev_btn_style) {
          groupHead = /*#__PURE__*/React.createElement("div", {
            className: "landing-playlist-group-button",
            style: {
              backgroundColor: vg && vg[0] ? vg[0].color : "transparent"
            }
          }, /*#__PURE__*/React.createElement("span", {
            className: "landing-group-thumb",
            style: {
              backgroundImage: `url(${vg && vg[0] ? vg[0].thumbnail : null})`
            }
          }), /*#__PURE__*/React.createElement("span", null, vg && vg[0] ? vg[0].title : "___"));
        }
      }

      prev_btn_style = pr;
      return /*#__PURE__*/React.createElement(Fragment, {
        key: i
      }, groupHead, /*#__PURE__*/React.createElement("div", {
        className: `landing-playlist-button ${i == this.state.current ? " current " : ""}`,
        onClick: () => this.onChoose(i),
        style: btn_style
      }, e.title, i == this.state.current && e.description ? /*#__PURE__*/React.createElement("div", {
        className: "landing-playlist-button-descr",
        dangerouslySetInnerHTML: {
          __html: e.description
        }
      }) : null, e.author_name ? /*#__PURE__*/React.createElement("div", {
        className: "landing-playlist-author-name"
      }, /*#__PURE__*/React.createElement("i", {
        className: "fas fa-user mr-2 small "
      }), e.author_name) : null));
    }) : /*#__PURE__*/React.createElement("div", null, __("No video"));
    const currentVideo = this.state.current == -1 ? default_video : videos[this.state.current];
    const thumb = default_video && default_video.thumbnail ? default_video.thumbnail : default_video ? getVideoThumbnail({
      source: default_video.source,
      video_id: default_video.id
    }) : null; // console.log(default_video);

    return /*#__PURE__*/React.createElement("div", {
      className: "landing-playlist-container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-playlist-video"
    }, videos && videos.length > 0 ? /*#__PURE__*/React.createElement(Video, {
      data: { ...currentVideo,
        height,
        width
      },
      type: "video"
    }) : /*#__PURE__*/React.createElement("div", {
      className: "",
      style: {
        height: 300
      }
    }, __("No video"))), /*#__PURE__*/React.createElement("div", {
      className: "landing-playlist-list",
      style: {
        height: `${height}px`
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-playlist-button title"
    }, default_video ? /*#__PURE__*/React.createElement("div", {
      className: "landing-video-player-default-thumb",
      style: {
        backgroundImage: `url(${thumb})`
      },
      onClick: () => this.onChoose(-1)
    }) : null, /*#__PURE__*/React.createElement("div", {
      style: {
        fontFamily: getFontNameByID(palette.card.title.fontFamilyID)
      }
    }, title), /*#__PURE__*/React.createElement("span", {
      className: "comment"
    }, `${buttonList.length} ${__(" videos")}`)), buttonList));
  }

}

export default compose(withApollo)(VideoPlayList);