function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { Component } from "react";
import { LayoutIcon } from 'react-pe-useful';
import MotivationMember from "./MotivationMember";
import EditLabel from "./EditLabel";
import { components } from "./data/components";

class Motivation extends Component {
  render() {
    const {
      columns,
      type,
      palette
    } = this.props;
    const {
      motivation,
      class_name,
      style,
      design_type,
      contour_type,
      form_type,
      color
    } = this.props.data;
    return motivation && Array.isArray(motivation) ? /*#__PURE__*/React.createElement("div", {
      className: `${columns} landing-motivation ${class_name}`,
      style: style
    }, motivation.map((e, i) => /*#__PURE__*/React.createElement(MotivationMember, _extends({}, e, {
      key: i,
      design_type: design_type,
      form_type: form_type,
      contour_type: contour_type,
      color: color,
      palette: palette
    })))) : /*#__PURE__*/React.createElement("div", {
      className: ` landing-empty ${class_name}`,
      style: { ...style,
        height: 300
      }
    }, /*#__PURE__*/React.createElement(LayoutIcon, {
      src: components()[this.props.type].icon,
      className: " layout-icon-giant "
    }), /*#__PURE__*/React.createElement("div", {
      className: "lead text-white"
    }, components()[this.props.type].title), /*#__PURE__*/React.createElement(EditLabel, _extends({}, this.props, {
      source: type,
      onEdit: this.props.onEdit,
      isBtn: true
    })));
  }

}

export default Motivation;