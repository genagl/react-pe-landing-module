function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import SectionContent from "./SectionContent";
import Section, { getDefault } from "./Section";

class Columns extends SectionContent {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onEdit", (data, id) => {
      console.log("onEdit", id, data, this.props);
      const sections = [...this.props.data.sections];
      const secs = [];
      sections.forEach(e => {
        if (e.id === data.id) {
          secs.push(data);
        } else {
          secs.push(e);
        }
      });
      this.setState({
        data: { ...this.props.data,
          sections: secs
        }
      });
      this.props.onEdit({ ...this.props,
        data: { ...this.props.data,
          sections: secs
        }
      }, this.props.id);
    });

    _defineProperty(this, "onUp", data => {
      //console.log("onUp", data, this.props)
      const sections = [...this.props.data.sections];
      const sec = { ...sections[data]
      };
      sections.splice(data, 1);
      sections.splice(data - 1, 0, sec); //console.log(sections)

      this.setState({
        data: { ...this.props.data,
          sections
        }
      });
      this.props.onEdit({ ...this.props,
        data: { ...this.props.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onDn", data => {
      console.log("onDn", data, this.props);
      const sections = [...this.props.data.sections];
      const sec = { ...sections[data]
      };
      sections.splice(data, 1);
      sections.splice(data + 1, 0, sec);
      console.log(sections);
      this.setState({
        data: { ...this.props.data,
          sections
        }
      });
      this.props.onEdit({ ...this.props,
        data: { ...this.props.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onAdd", data => {
      console.log("onAdd", data, this.props);
      const sections = [...this.props.data.sections];
      const sec = getDefault();
      sections.splice(data + 1, 0, sec); //console.log(sections)

      this.setState({
        data: { ...this.props.data,
          sections
        }
      });
      this.props.onEdit({ ...this.props,
        data: { ...this.props.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onRnv", data => {
      console.log("onRnv", data, this.props.data.sections);
      const sections = [...this.props.data.sections];
      sections.splice(data, 1);
      console.log(sections);
      this.setState({
        data: { ...this.props.data,
          sections
        }
      });
      this.props.onEdit({ ...this.props,
        data: { ...this.props.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onHide", (id, is_hide) => {
      console.log("HIDE", id, is_hide);
    });

    _defineProperty(this, "onRemoveFloat", float_id => {});

    _defineProperty(this, "onUpdateFloat", (data, float_id, section_id) => {});
  }

  is() {
    const {
      sections
    } = this.props.data;
    return Array.isArray(sections) && sections.length > 0;
  }

  renderContent(style) {
    const {
      class_name,
      sections,
      proportia,
      column_gap
    } = this.props.data;
    return /*#__PURE__*/React.createElement("div", {
      className: `landing-columns ${class_name || ""} columns-${this.props.composition.columns}`,
      style: { ...style,
        columnGap: `${column_gap}px`
      }
    }, sections.map((e, i) => {
      const gap = column_gap ? column_gap / sections.length : .1;
      const width = proportia && proportia[i] && sections.length > 0 ? "calc(" + proportia[i] + "% - " + gap + "px)" : "calc(" + 100 / sections.length + "% - " + gap + "px)";
      const estyle = { ...e.style,
        width: width
      };
      return /*#__PURE__*/React.createElement("div", {
        style: estyle,
        className: ` landing-single-column  ${e.className}`,
        key: i
      }, /*#__PURE__*/React.createElement(Section, _extends({}, e, {
        style: {
          height: "100%",
          ...e.style
        },
        i: i,
        user: this.props.user,
        is_edit: this.props.is_edit,
        level: this.props.level + 1,
        onEdit: this.onEdit,
        onUp: this.onUp,
        onDn: this.onDn,
        onAdd: this.onAdd,
        onRnv: this.onRnv
      })));
    }));
  }

}

export default Columns;