import React, { Component } from "react";
import Style from "style-it";

class Testanomial extends Component {
  render() {
    const {
      class_name,
      style
    } = this.props;
    const {
      text,
      name,
      avatar,
      description,
      palette
    } = this.props;
    return /*#__PURE__*/React.createElement("div", {
      className: `landing-testanomial ${class_name}`,
      style: style
    }, Style.it(`.text::before {
              content: "";
              position: absolute;
              left: 120px;
              bottom: -20px;
              border: 20px solid transparent;
              border-left: 20px solid ${palette.card.backgroundColor};
            }`, /*#__PURE__*/React.createElement("div", {
      className: "text",
      style: { ...palette.card
      }
    }, /*#__PURE__*/React.createElement("div", {
      dangerouslySetInnerHTML: {
        __html: text
      }
    }))), /*#__PURE__*/React.createElement("div", {
      className: "d-flex"
    }, /*#__PURE__*/React.createElement("div", {
      className: "avatar",
      style: {
        backgroundImage: `url(${avatar})`
      }
    }), /*#__PURE__*/React.createElement("div", {
      className: "ltest-cont",
      style: {
        color: palette.main_text_color
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "name"
    }, name), /*#__PURE__*/React.createElement("div", {
      className: "",
      dangerouslySetInnerHTML: {
        __html: description
      }
    }))));
  }

}

export default Testanomial;