import React from "react";
import VK, { Poll } from "react-vk";
export default (({
  api_key,
  poll_id
}) => {
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    className: "landing-vp-poll",
    id: "vp_pol"
  }), /*#__PURE__*/React.createElement(VK, {
    apiId: api_key,
    onApiAvailable: () => console.log("VK ready")
  }, /*#__PURE__*/React.createElement(Poll, {
    elementId: "vp_pol",
    pollId: poll_id
  })));
});