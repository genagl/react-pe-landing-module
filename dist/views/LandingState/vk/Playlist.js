import React from "react";
import VK, { Playlist } from "react-vk";

const _PlayList = ({
  api_key,
  owner_id,
  playlist_id,
  hash
}) => {
  try {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "landing-vp-poll",
      id: "vk_playlist_" + owner_id + playlist_id
    }), /*#__PURE__*/React.createElement(VK, {
      apiId: api_key
    }, /*#__PURE__*/React.createElement(Playlist, {
      elementId: "vk_playlist_" + owner_id + playlist_id,
      ownerId: owner_id,
      playlistId: playlist_id,
      hash: hash
    })));
  } catch (e) {
    return e.message;
  }
};

export default _PlayList;