function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Fragment } from "react";
import { __ } from "react-pe-utilities";
import { Button, ButtonGroup, Intent, Popover, Dialog, Icon, Tooltip, Position, Callout } from "@blueprintjs/core";
import { withRouter } from "react-router";
import gql from "graphql-tag";
import { withApollo } from "react-apollo";
import { compose } from "recompose";
import $ from "jquery";
import { DateInput, IDateFormatProps, TimePrecision, DateTimePicker } from "@blueprintjs/datetime";
import Moment from "react-moment";
import MomentLocaleUtils, { formatDate, parseDate } from "react-day-picker/moment";
import "react-day-picker/lib/style.css";
import DayPicker from "react-day-picker";
import moment from "moment";
import { AppToaster } from 'react-pe-useful';
import { MediaChooser } from "react-pe-useful";
import SectionContent from "./SectionContent";
import "moment/locale/ru";

class ContactForm extends SectionContent {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onRadioChange", evt => {
      const i = evt.currentTarget.getAttribute("i");
      const ii = evt.currentTarget.getAttribute("ii");
      const cheched = evt.currentTarget.checked;
      const value = this.props.data.forms[i].data[ii].label;
      const values = [...this.state.values];
      values[i] = value;
      console.log(value, i, values);
      this.setState({
        values
      });
    });

    _defineProperty(this, "onMediaChange", (value, file, i) => {
      const values = [...this.state.values];
      values[i] = value;
      this.setState({
        values
      });
    });

    _defineProperty(this, "handleStartChange", (value, i) => {
      const values = [...this.state.values];
      values[i] = value;
      this.setState({
        values
      }); // const state = { ...this.state, values };
      // this.on( moment( value ).toISOString() );
    });

    _defineProperty(this, "onSend", () => {
      const values = [...this.state.values]; // console.log( values );

      const {
        forms,
        toast_text
      } = this.state.data;

      if (values.filter(e => e != "" || typeof e != "undefined").length == 0) {
        AppToaster.show({
          intent: Intent.DANGER,
          icon: "tick",
          message: __("Form is empty.")
        });
        return;
      }

      const req = forms.filter((e, i) => e.is_required && (values[i] == "" || typeof values[i] == "undefined")).map(e => e.label); // console.log( values, req );

      if (req.length > 0) {
        AppToaster.show({
          intent: Intent.DANGER,
          icon: "tick",
          duration: 10000,
          message: __("Some required are empty: ") + req.join(", ")
        });
        return;
      } // sending


      const message = [];
      forms.forEach((e, i) => {
        message.push({ ...e,
          i,
          value: values[i]
        });
      });
      const message_json = JSON.stringify(message).replace(/"/g, "'");
      const matrix_json = JSON.stringify(forms).replace(/"/g, "'");
      const mutation = gql`
			mutation sendPELandingContactFormMessage
			{
				sendPELandingContactFormMessage( input: 
					{
						message_json 	: "${message_json}",
						matrix_json 	: "${matrix_json}"
					} 
				)
			}`;
      this.props.client.mutate({
        mutation,
        update: (store, {
          data
        }) => {
          this.setState({
            values: [],
            message: __(toast_text),
            height: this.ref.current.getBoundingClientRect().height
          });
          AppToaster.show({
            intent: Intent.SUCCESS,
            icon: "tick",
            duration: 10000,
            message: __("Your message sent successfully")
          });
        }
      });
    });
  }

  getState() {
    this.ref = /*#__PURE__*/React.createRef();
    return {
      values: [],
      message: "",
      height: 200,
      isOpen: false
    };
  }

  is() {
    const {
      forms
    } = this.state.data;
    return Array.isArray(forms) && forms.length > 0;
  }

  renderContent() {
    const {
      as_button
    } = this.props.data;
    return this.getAsContent();
  }

  getAs_button() {
    return /*#__PURE__*/React.createElement(React.Fragment, null);
  }

  getAsContent() {
    const {
      type
    } = this.props;
    const {
      class_name,
      style,
      forms,
      label,
      as_button
    } = this.props.data;
    return this.state.message ? /*#__PURE__*/React.createElement("div", {
      className: "w-100 ",
      style: { ...style
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-contact-form",
      style: {
        minHeight: this.state.height
      }
    }, /*#__PURE__*/React.createElement("span", {
      className: " cf-message "
    }, __(this.state.message)))) : /*#__PURE__*/React.createElement("div", {
      className: "w-100",
      style: { ...style
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-contact-form",
      style: {
        minHeight: this.state.height
      },
      ref: this.ref
    }, this.getForms(), /*#__PURE__*/React.createElement("div", {
      className: "p-4 d-flex"
    }, /*#__PURE__*/React.createElement("div", {
      className: "btn btn-primary mx-auto",
      onClick: this.onSend
    }, __(label)))));
  }

  getForms() {
    const {
      forms
    } = this.props.data;
    const jsDateFormatter = {
      // note that the native implementation of Date functions differs between browsers
      formatDate: date => moment(date).format("D.MM.YYYY HH:mm"),
      // parseDate: str => new Date(str),
      parseDate: str => new Date(Date.parse(str)),
      placeholder: "M/D/YYYY"
    }; // console.log(this.props.data.forms);

    return forms.map((e, i) => {
      const label = e.label ? /*#__PURE__*/React.createElement("div", {
        className: "title"
      }, __(e.label)) : null;
      const description = e.description ? /*#__PURE__*/React.createElement("div", {
        className: "description"
      }, __(e.description)) : null;
      let input; // console.log( e.type );

      switch (e.type) {
        case "email":
          input = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("input", {
            type: "email",
            className: "form-control my-2",
            name: e.label,
            value: this.state.values[i],
            onChange: evt => this.onValue(evt, i)
          }));
          break;

        case "text":
          const rows = this.props.data.forms[i].rows;
          input = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("textarea", {
            className: "form-control my-2 w-100",
            name: e.label,
            value: this.state.values[i],
            onChange: evt => this.onValue(evt, i),
            rows: rows ? rows : 6
          }));
          break;

        case "phone":
          input = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("input", {
            type: "phone",
            className: "form-control my-2",
            name: e.label,
            value: this.state.values[i],
            onChange: evt => this.onValue(evt, i)
          }));
          break;

        case "file_loader":
          input = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(MediaChooser, {
            prefix: `_${e.label}`,
            url: e.value,
            id: `mc_${i}`,
            ID: `mc_${i}`,
            padding: 5,
            height: 140,
            onChange: (value, file) => this.onMediaChange(value, file, i)
          }));
          break;

        case "time":
          input = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(DateInput, _extends({
            minDate: new Date(new Date().setFullYear(new Date().getFullYear() - 100)),
            maxDate: new Date(new Date().setFullYear(new Date().getFullYear() + 10))
          }, jsDateFormatter, {
            className: " " + "",
            closeOnSelection: true,
            date: this.state.values[i],
            defaultValue: new Date(),
            onChange: value => this.handleStartChange(value, i),
            invalidDateMessage: __("Invalid date"),
            timePrecision: TimePrecision.MINUTE,
            timePickerProps: {
              showArrowButtons: true
            }
          })));
          break;

        case "calendar":
          input = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(DateTimePicker, _extends({
            minDate: new Date(new Date().setFullYear(new Date().getFullYear() - 100)),
            maxDate: new Date(new Date().setFullYear(new Date().getFullYear() + 10))
          }, jsDateFormatter, {
            className: " " + "",
            closeOnSelection: true,
            date: this.state.values[i],
            defaultValue: new Date(),
            onChange: value => this.handleStartChange(value, i),
            invalidDateMessage: __("Invalid date"),
            timePrecision: TimePrecision.MINUTE,
            timePickerProps: {
              showArrowButtons: true
            }
          })));
          break;

        case "radio":
          // console.log(this.state.values[i], e.label);
          const variants = this.props.data.forms[i].data.map((e, ii) => /*#__PURE__*/React.createElement("div", {
            className: "p-2",
            key: ii
          }, /*#__PURE__*/React.createElement("label", {
            htmlFor: `cf-radio${this.props.section_id}_${ii}`,
            className: "_check_"
          }, /*#__PURE__*/React.createElement("input", {
            type: "radio",
            className: "",
            ii: ii,
            i: i,
            id: `cf-radio${this.props.section_id}_${ii}`,
            checked: this.state.values[i] == e.label,
            onChange: this.onRadioChange
          }), e.label)));
          input = /*#__PURE__*/React.createElement("div", null, variants);
          break;

        case "string":
        default:
          input = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("input", {
            type: "text",
            className: "form-control my-2",
            name: e.label,
            onChange: evt => this.onValue(evt, i)
          }));
      }

      return /*#__PURE__*/React.createElement("div", {
        key: i,
        className: "py-2"
      }, label, /*#__PURE__*/React.createElement("div", {
        className: `req_input ${e.is_required ? "required" : ""}`
      }, input, e.is_required ? /*#__PURE__*/React.createElement("span", {
        className: "req"
      }, __("required field")) : null), description);
    });
  }

  onValue(evt, i) {
    const {
      value
    } = evt.currentTarget;
    const values = [...this.state.values];
    values[i] = value; // console.log( value, i , values );

    this.setState({
      values
    });
  }

}

export default compose(withApollo, withRouter)(ContactForm);