function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import Float from "./Float";
import matrix from "./data/matrix";

class Floats extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", { ...this.props
    });

    _defineProperty(this, "onRemoveFloat", float_id => this.props.onRemoveFloat(float_id));

    _defineProperty(this, "onUpdate", (data, float_id) => this.props.onUpdate(data, float_id));
  }

  componentDidUpdate(nextProps) {
    // /.log( nextProps.is_edit );
    if (nextProps.is_edit !== this.state.is_edit) {
      this.setState({
        is_edit: nextProps.is_edit
      });
    }

    const state = {};

    for (const i in matrix.floats) {
      if (nextProps[i] !== this.state[i]) {
        state[i] = nextProps[i];
      }
    }

    if (Object.keys(state).length > 0) {
      // console.log( Object.keys(state).length, state );
      this.setState({ ...state
      });
    }
  }

  render() {
    const {
      floats
    } = this.state;

    const __floats = floats ? floats.map((e, i) => /*#__PURE__*/React.createElement(Float, _extends({}, e, {
      key: i,
      is_edit: this.state.is_edit,
      onRemoveFloat: this.onRemoveFloat,
      onUpdate: this.onUpdate
    }))) : null;

    return floats && floats.length > 0 ? /*#__PURE__*/React.createElement(React.Fragment, null, __floats) : null;
  }

}

export default Floats;