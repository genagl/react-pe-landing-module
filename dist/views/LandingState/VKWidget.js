function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import SectionContent from "./SectionContent";
import Group from "./vk/Group";
import Playlist from "./vk/Playlist";
import Poll from "./vk/Poll";

class VKWidget extends SectionContent {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onJoin", () => {});

    _defineProperty(this, "onLeave", () => {});
  }

  is() {
    const {
      api_key
    } = this.state.data;
    return api_key;
  }

  renderContent(style) {
    const {
      vk_widget_type,
      api_key,
      poll_id,
      owner_id,
      playlist_id,
      play_list_hash,
      groupId,
      width,
      height,
      mode,
      no_cover,
      wide,
      color1,
      color2,
      color3 // onJoin, onLeave

    } = this.state.data;
    let widget = null;

    switch (vk_widget_type) {
      case "playlist":
        widget = /*#__PURE__*/React.createElement(Playlist, {
          owner_id: owner_id,
          playlist_id: playlist_id,
          hash: play_list_hash
        });
        break;

      case "group":
        widget = /*#__PURE__*/React.createElement(Group, {
          api_key: api_key,
          groupId: groupId,
          width: width,
          height: height,
          mode: mode,
          no_cover: no_cover,
          wide: wide,
          color1: color1,
          color2: color2,
          color3: color3,
          onJoin: this.onJoin,
          onLeave: this.onLeave
        });
        break;

      case "poll":
      default:
        widget = /*#__PURE__*/React.createElement(Poll, {
          api_key: api_key,
          poll_id: poll_id
        });
    }

    return /*#__PURE__*/React.createElement(React.Fragment, null, widget);
  }

}

export default VKWidget;