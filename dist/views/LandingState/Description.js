function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import matrix from "./data/matrix";
import EditLabel from "./EditLabel";
import { getStyle } from "./Section";

class Description extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onEdit", data => {
      console.log(this.props);

      if (this.props.onEdit) {
        this.props.onEdit({
          descriptions: data
        });
      }
    });

    this.state = { ...this.props
    };
  }

  componentWillUpdate(nextProps) {
    if (nextProps.is_edit !== this.state.is_edit) {
      this.setState({
        is_edit: nextProps.is_edit
      });
    }

    const state = {};

    for (const i in matrix.descriptions) {
      if (nextProps[i] !== this.state[i]) {
        state[i] = nextProps[i];
      }
    }

    if (Object.keys(state).length > 0) {
      // console.log( Object.keys(state).length, state );
      this.setState({ ...state
      });
    }
  }

  render() {
    const {
      text,
      text_src,
      class_name,
      style
    } = this.state;
    const styleObj = getStyle(style);
    return text || text_src ? /*#__PURE__*/React.createElement("div", {
      className: `landing-description ${class_name}`,
      style: styleObj
    }, /*#__PURE__*/React.createElement("span", null, /*#__PURE__*/React.createElement("span", {
      dangerouslySetInnerHTML: {
        __html: text
      },
      style: {
        backgroundColor: style.plateColor,
        padding: "3px 10px"
      }
    }), /*#__PURE__*/React.createElement(EditLabel, _extends({}, this.state, {
      isBtn: true,
      st: {
        marginRight: -20
      },
      children: null,
      source: "descriptions",
      onEdit: this.onEdit,
      data: this.state
    })))) : null;
  }

}

export default Description;