import ArchorsMenu from "../ArchorsMenu";
import Cards from "../Cards";
import Matchings from "../Matchings";
import Carousel from "../Carousel";
import Layers from "../Layers";
import Rows from "../Rows";
import Columns from "../Columns";
import InnerLink from "../InnerLink";
import OuterURL from "../OuterURL";
import ButtonAnimateTo from "../ButtonAnimateTo";
import Image from "../Image";
import Mask from "../Mask";
import Team from "../Team";
import Motivation from "../Motivation";
import Quote from "../Quote";
import Testanomials from "../Testanomials";
import SocialLikes from "../SocialLikes";
import HTML from "../HTML";
import PostFeed from "../PostFeed";
import PostCarousel from "../PostCarousel";
import YandexMap from "../YandexMap";
import Video from "../Video";
import VideoPlayList from "../VideoPlayList";
import ContactForm from "../ContactForm";
import ZoomPanPinch from "../ZoomPanPinch";
import Accordeon from "../Accordeon";
import TimeLine from "../TimeLine";
import VKWidget from "../VKWidget";
import IFrame from "../IFrame";
import IncludeSection from "../IncludeSection";
import KraevedTitle from "../../kraeved/KraevedTitle";
import __menu from "../assets/img/landing/archor-menu.svg";
import __html from "../assets/img/landing/html.svg";
import __calculator from "../assets/img/landing/calculator.svg";
import __carousel from "../assets/img/landing/carousel.svg";
import __merge from "../assets/img/landing/merge.svg";
import __quote from "../assets/img/landing/quote.svg";
import __eventsFeed from "../assets/img/landing/events-feed.svg";
import __image from "../assets/img/landing/picture.svg";
import __map from "../assets/img/landing/map.svg";
import __motivation from "../assets/img/landing/motivation.svg";
import __portfolio from "../assets/img/landing/portfolio.svg";
import __postFeed from "../assets/img/landing/post-feed.svg";
import __team from "../assets/img/landing/team.svg";
import __testanomials from "../assets/img/landing/testanomials.svg";
import __video from "../assets/img/landing/video.svg";
import __inner_link from "../assets/img/landing/008-mouse.svg";
import __button_animate_to from "../assets/img/landing/008-mouse.svg";
import __likes from "../assets/img/landing/likes.svg";
import __design from "../assets/img/landing/section-growth.svg";
import __title from "../assets/img/landing/title.svg";
import __description from "../assets/img/landing/description.svg";
import __contact_form from "../assets/img/landing/email.svg";
import __layers from "../assets/img/landing/layers.svg";
import __rows from "../assets/img/landing/149-menu-1.svg";
import __columns from "../assets/img/landing/150-menu-2.svg";
import __cards from "../assets/img/landing/card.svg";
import __matchings from "../assets/img/landing/matchings.svg";
import __zoompanpinch from "../assets/img/landing/zoompanpinch.svg";
import __accordeon from "../assets/img/landing/accordeon.svg";
import __time_line from "../assets/img/landing/time_line.svg";
import __sm from "../assets/img/landing/android.svg";
import __xs from "../assets/img/landing/android1.svg";
import __iframe from "../assets/img/landing/iframe.svg";
export const components = () => {
  return {
    html: {
      c: HTML,
      icon: __html,
      title: "Simple HTML"
    },
    archor_menu: {
      c: ArchorsMenu,
      icon: __menu,
      title: "Smart archors menu"
    },
    contact_form: {
      c: ContactForm,
      icon: __contact_form,
      title: "Constanc Form"
    },
    carousel: {
      c: Carousel,
      icon: __carousel,
      title: "Carousel"
    },
    layers: {
      c: Layers,
      icon: __layers,
      title: "Layers"
    },
    rows: {
      c: Rows,
      icon: __rows,
      title: "Rows"
    },
    columns: {
      c: Columns,
      icon: __columns,
      title: "Columns"
    },
    image: {
      c: Image,
      icon: __image,
      title: "Image"
    },
    mask: {
      c: Mask,
      icon: __image,
      title: "Mask"
    },
    inner_link: {
      c: InnerLink,
      icon: __inner_link,
      title: "Inner Link"
    },
    outer_url: {
      c: OuterURL,
      icon: __inner_link,
      title: "Outer URL"
    },
    button_animate_to: {
      c: ButtonAnimateTo,
      icon: __button_animate_to,
      title: "Buttom to Archor"
    },
    team: {
      c: Team,
      icon: __team,
      title: "Team"
    },
    quote: {
      c: Quote,
      icon: __quote,
      title: "Quote"
    },
    motivation: {
      c: Motivation,
      icon: __motivation,
      title: "Features"
    },
    testanomials: {
      c: Testanomials,
      icon: __testanomials,
      title: "Testanomials"
    },
    social_likes: {
      c: SocialLikes,
      icon: __likes,
      title: "Social Likes"
    },
    yandex_map: {
      c: YandexMap,
      icon: __map,
      title: "Yandex Map"
    },
    video_play_list: {
      c: VideoPlayList,
      icon: __video,
      title: "Video Play List"
    },
    video: {
      c: Video,
      icon: __video,
      title: "Video"
    },
    post_feed: {
      c: PostFeed,
      icon: __postFeed,
      title: "Feed"
    },
    post_carousel: {
      c: PostCarousel,
      icon: __carousel,
      title: "Post carousel"
    },
    cards: {
      c: Cards,
      icon: __cards,
      title: "Cards"
    },
    matchings: {
      c: Matchings,
      icon: __matchings,
      title: "Matchings"
    },
    zoom_pan_pinch: {
      c: ZoomPanPinch,
      icon: __zoompanpinch,
      title: "Zoom-pan-pinch"
    },
    accordeon: {
      c: Accordeon,
      icon: __accordeon,
      title: "Accordeon"
    },
    time_line: {
      c: TimeLine,
      icon: __time_line,
      title: "Time Line"
    },
    vk: {
      c: VKWidget,
      icon: __time_line,
      title: "VKontakte"
    },
    iframe: {
      c: IFrame,
      icon: __iframe,
      title: "IFrame"
    },
    include_section: {
      c: IncludeSection,
      icon: __quote,
      title: "Include Section"
    },
    KreavedTitle: {
      c: KraevedTitle,
      icon: __time_line,
      title: "Kraeved Title"
    }
  };
};