import React, { Component } from "react";
import { Link } from "react-router-dom";
import { getFontNameByID } from "./data/PalettePresets";

class MotivationMember extends Component {
  render() {
    const {
      thumbnail,
      title,
      description,
      class_name,
      style,
      design_type,
      contour_type,
      form_type,
      color,
      link_type,
      link_label,
      link_route,
      palette
    } = this.props; // console.log(this.props);

    const sr = {
      backgroundImage: `url(${thumbnail})`
    };

    if (color) {
      switch (contour_type) {
        case "round_line_contoure":
          sr.borderColor = color;
          break;

        case "round_fill_contoure":
          sr.backgroundColor = color;
          break;

        default:
          break;
      }
    }

    let link;

    switch (link_type) {
      case "inner":
        link = /*#__PURE__*/React.createElement("div", {
          className: "landing-motivation-link-cont"
        }, /*#__PURE__*/React.createElement(Link, {
          className: "landing-motivation-link",
          to: link_route
        }, link_label));
        break;

      case "outer":
        link = /*#__PURE__*/React.createElement("div", {
          className: "landing-motivation-link-cont"
        }, /*#__PURE__*/React.createElement("a", {
          className: "landing-motivation-link",
          href: link_route
        }, link_label));
        break;

      case "nothing":
      default:
        break;
    }

    return thumbnail || title || description ? /*#__PURE__*/React.createElement("div", {
      className: `l-col motivation-element ${class_name}`,
      style: {
        color: palette.main_text_color,
        ...style
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: `thumbnail ${design_type} ${contour_type} ${form_type}`,
      style: sr
    }), /*#__PURE__*/React.createElement("div", {
      className: "title",
      style: { ...palette.card.title,
        fontFamily: getFontNameByID(palette.card.title.fontFamilyID)
      }
    }, title), /*#__PURE__*/React.createElement("div", {
      className: "description",
      dangerouslySetInnerHTML: {
        __html: description
      }
    }), link) : null;
  }

}

export default MotivationMember;