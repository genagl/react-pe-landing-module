function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { Component, Suspense } from "react";
import { Loading } from 'react-pe-useful';
import { LayoutIcon } from 'react-pe-useful';
import PostFeedCard from "./postFeed/PostFeedCard";
import gql from "graphql-tag";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import { getQueryArgs } from "react-pe-layouts";
import EditLabel from "./EditLabel";
import { components } from "./data/components";
const OwlCarousel = /*#__PURE__*/React.lazy(() => import('react-owl-carousel2fix'));

class PostCarousel extends Component {
  constructor(props) {
    super(props);
    this.car = /*#__PURE__*/React.createRef();
  }

  render() {
    const {
      class_name,
      style,
      composition
    } = this.props;
    const {
      dots,
      nav,
      autoplay,
      loop,
      data_type,
      offset,
      count
    } = this.props.data;
    let html;

    if (data_type) {
      const options = {
        dots: typeof dots !== "undefined" ? !!dots : true,
        items: typeof composition.columns !== "undefined" ? composition.columns : 1,
        nav: typeof nav !== "undefined" ? !!nav : false,
        rewind: true,
        autoplay: typeof autoplay !== "undefined" ? !!autoplay : true,
        loop: typeof loop !== "undefined" ? !!loop : true,
        responsive: {
          0: {
            items: 1
          },
          760: {
            items: typeof composition.columns !== "undefined" ? composition.columns : 1
          }
        }
      };
      const name = `get${data_type}s`;
      const fields = getQueryArgs(data_type);
      const query = gql`
				query ${name} 
				{
					${name}( paging:{ count:${count}, offset:${offset} })
					{
						${fields}
					}
				}
			`;
      html = /*#__PURE__*/React.createElement(Query, {
        query: query
      }, ({
        loading,
        error,
        data,
        client
      }) => {
        if (loading) {
          return /*#__PURE__*/React.createElement(Loading, null);
        }

        if (data) {
          // console.log(data[name]);
          const __sliders = data[name].map((e, i) => /*#__PURE__*/React.createElement(PostFeedCard, _extends({}, e, {
            key: i,
            i: i
          })));

          return /*#__PURE__*/React.createElement("div", {
            className: `landing-post-carousel landing-element${class_name}`,
            style: style
          }, /*#__PURE__*/React.createElement(Suspense, {
            fallback: /*#__PURE__*/React.createElement(Loading, null)
          }, /*#__PURE__*/React.createElement(OwlCarousel, {
            ref: this.car,
            options: options
          }, __sliders)));
        }
      });
    } else {
      html = this.no();
    }

    return html;
  }

  no() {
    const {
      class_name,
      style,
      type
    } = this.props;
    return /*#__PURE__*/React.createElement("div", {
      className: ` landing-empty ${class_name}`,
      style: { ...style,
        height: 300
      }
    }, /*#__PURE__*/React.createElement(LayoutIcon, {
      src: components()[this.props.type].icon,
      className: " layout-icon-giant "
    }), /*#__PURE__*/React.createElement("div", {
      className: "lead text-white"
    }, components()[this.props.type].title), /*#__PURE__*/React.createElement(EditLabel, _extends({}, this.props, {
      source: type,
      onEdit: this.props.onEdit,
      isBtn: true
    })));
  }

}

export default compose(withApollo)(PostCarousel);