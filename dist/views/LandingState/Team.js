function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { Component } from "react";
import TeamMember from "./TeamMember";
import { LayoutIcon } from 'react-pe-useful';
import EditLabel from "./EditLabel";
import { components } from "./data/components";

class Team extends Component {
  render() {
    const {
      columns,
      type,
      is_edit
    } = this.props;
    const {
      team,
      class_name,
      style
    } = this.props.data;
    return team && Array.isArray(team) ? /*#__PURE__*/React.createElement("div", {
      className: `${columns} landing-team ${class_name}`,
      style: style
    }, team.map((e, i) => /*#__PURE__*/React.createElement(TeamMember, _extends({}, e, {
      key: i,
      is_edit: is_edit
    })))) : /*#__PURE__*/React.createElement("div", {
      className: ` landing-empty ${class_name}`,
      style: { ...style,
        height: 300
      }
    }, /*#__PURE__*/React.createElement(LayoutIcon, {
      src: components()[this.props.type].icon,
      className: " layout-icon-giant "
    }), /*#__PURE__*/React.createElement("div", {
      className: "lead text-white"
    }, components()[this.props.type].title), /*#__PURE__*/React.createElement(EditLabel, _extends({}, this.props, {
      source: type,
      onEdit: this.props.onEdit,
      isBtn: true
    })));
  }

}

export default Team;