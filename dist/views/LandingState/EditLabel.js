function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, Intent, Icon, Dialog, Popover, PopoverInteractionKind, Position, ButtonGroup } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import InputForm from "./edit/InputForm";

class EditLabel extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onDailogHandler", evt => {
      evt.stopPropagation();
      this.setState({
        isOpen: !this.state.isOpen
      });
    });

    _defineProperty(this, "onEdit", () => {
      this.setState({
        isOpen: false
      });
      if (this.props.onEdit) this.props.onEdit(this.state.data, this.props.id);
    });

    _defineProperty(this, "onField", (value, field, block, dopol) => {
      // console.log( value, field, block );
      const state = { ...this.state
      };
      if (!state[block]) state[block] = {};
      state[block][field] = value; // console.log( state );
      // if (this.state.type == "cards") {
      //   if (field == "fields" && dopol && Array.isArray(dopol)) {
      //     state.data.cards = dopol
      //   }
      // }
      //console.log(state.data)

      this.setState({
        data: state.data
      });
    });

    _defineProperty(this, "on", (data, field) => {
      console.log(data, field);
    });

    this.state = { ...this.props,
      isOpen: false
    };
  }

  componentDidUpdate(nextProps) {
    if (nextProps.is_edit !== this.state.is_edit) {
      this.setState({
        is_edit: nextProps.is_edit
      });
    }
  }

  render() {
    // return null;
    return this.state.is_edit ? /*#__PURE__*/React.createElement(React.Fragment, null, this.state.isBtn ? this.btn() : this.pic(), /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isOpen,
      onClose: this.onDailogHandler,
      className: "little2",
      title: __("Edit") + ": " + __(this.props.source)
    }, /*#__PURE__*/React.createElement("div", {
      className: "p-4 dialog-content overflow-y-auto"
    }, this.dialogContent()), /*#__PURE__*/React.createElement(ButtonGroup, {
      className: "d-flex justify-content-center pt-2",
      fill: true,
      large: true
    }, /*#__PURE__*/React.createElement(Button, {
      intent: Intent.SUCCESS,
      onClick: this.onEdit,
      className: "flex-grow-100",
      icon: "cog"
    }, __("Edit")), /*#__PURE__*/React.createElement(Button, {
      intent: Intent.DANGER,
      onClick: this.onDailogHandler,
      className: "flex-grow-1",
      icon: "cross"
    }, __("Close"))))) : null;
  }

  pic() {
    return /*#__PURE__*/React.createElement("div", {
      className: "l-inline-edit-btn",
      onClick: this.onDailogHandler,
      style: {
        width: this.props.style.width,
        height: this.props.style.height
      }
    }, /*#__PURE__*/React.createElement(Icon, {
      icon: "annotation"
    }));
  }

  btn() {
    return this.props.children ? /*#__PURE__*/React.createElement(Popover, {
      className: "edit-btn-cont",
      interactionKind: PopoverInteractionKind.HOVER,
      position: Position.LEFT_TOP,
      content: /*#__PURE__*/React.createElement("div", {
        className: " "
      }, /*#__PURE__*/React.createElement(ButtonGroup, {
        vertical: true,
        fill: true,
        alignText: "left",
        large: true
      }, /*#__PURE__*/React.createElement(Button, {
        minimal: true,
        onClick: this.onDailogHandler
      }, __("Edit")), this.props.children))
    }, /*#__PURE__*/React.createElement("div", {
      className: "edit-btn",
      style: this.props.st
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-ellipsis-v"
    }))) : /*#__PURE__*/React.createElement("div", {
      className: "edit-btn-cont"
    }, /*#__PURE__*/React.createElement("div", {
      className: "edit-btn",
      style: this.props.st,
      onClick: this.onDailogHandler
    }, /*#__PURE__*/React.createElement(Icon, {
      icon: "edit"
    })));
  }

  dialogContent() {
    //console.log( matrix.Card )
    //console.log( this.state.data )
    return /*#__PURE__*/React.createElement("div", {
      className: "p-4"
    }, /*#__PURE__*/React.createElement(InputForm, _extends({}, this.state.data, {
      source: this.state.source,
      id: this.state.id,
      data: this.state.data,
      sourceType: this.state.source,
      on: (value, field, dopol) => this.onField(value, field, "data", dopol)
    })));
    /*
    let html = [];
    for(let m in matrix[this.props.source])
    {
      if(matrix[this.props.source][m].hidden) continue;
      const cntxt = DataContext.getSection( this.state.id );
      const src = cntxt && cntxt[this.props.source]
          ? DataContext.getSection( this.state.id )[this.props.source][m]
          : "";
      html.push( <FieldInput
            field={ m }
            key={ m }
            id={this.props.ID}
            on={this.on}
            onChange={this.on}
            { ...matrix[this.props.source][m] }
            editable = { true }
            value={ src }
            vertical={ false }
            visibled_value={ "title" }
          /> )
    }
    return html;
    */
  }

}

export default EditLabel;