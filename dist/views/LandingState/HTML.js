function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Fragment } from "react";
import ReactDOMServer, { renderToString } from 'react-dom/server';
import { __ } from "react-pe-utilities";
import SectionContent from "./SectionContent";
import IncludeSection from "./IncludeSection";
import Style from "style-it";
import $ from "jquery";

class HTML extends SectionContent {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "includes", []);
  }

  getState() {
    this.includes = [];
  }

  componentDidMount() {
    const {
      palette
    } = this.props;
    const {
      sections
    } = this.props.data;

    if (Array.isArray(sections)) {
      try {
        this.includes = sections.map((section, i) => {
          //console.log( section.text_id )
          //console.log( document.getElementById( section.text_id ) )
          return /*#__PURE__*/React.createElement(IncludeSection, {
            key: i,
            data: { ...section
            },
            palette: palette,
            modal: document.getElementById(section.text_id)
          });
        });
        this.setState({
          includes: this.includes
        });
      } catch (e) {
        console.alert(e);
      }
    }
  }

  renderContent(style) {
    const {
      palette
    } = this.props;
    const {
      class_name,
      text,
      height,
      width,
      vertical_align,
      exact_value,
      horizontal_align,
      exact_horizontal_value,
      text_align,
      sections
    } = this.props.data;
    let valign = vertical_align ? vertical_align : "center";
    let ve;
    let marginLeft, marginRight;

    if (vertical_align === "exact_value") {
      valign = "top";
      ve = exact_value + "%";
    }

    switch (horizontal_align) {
      case "start":
        marginLeft = 0;
        marginRight = "auto";
        break;

      case "end":
        marginLeft = "auto";
        marginRight = 0;
        break;

      default:
        marginLeft = exact_horizontal_value ? exact_horizontal_value + "%" : "auto";
        marginRight = "auto";
        break;
    }

    return /*#__PURE__*/React.createElement(React.Fragment, null, Style.it(`.landing-html
                {
                    color:${palette ? palette.main_text_color : null};
                    text-align: ${text_align};
                    display: flex;
                }`, /*#__PURE__*/React.createElement("div", {
      className: `landing-html ${class_name || ""} columns-${this.state.composition.columns}`,
      style: {
        maxWidth: width ? width : null,
        alignItems: valign,
        marginTop: ve,
        marginLeft,
        marginRight,
        ...style,
        height: height ? height : null,
        overflowX: "hidden",
        overflowY: "auto"
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "hidden-1 landing-html-includes"
    }, this.state.includes), /*#__PURE__*/React.createElement("div", {
      dangerouslySetInnerHTML: {
        __html: text
      },
      className: "w-100"
    }))));
  }

}

export default HTML;