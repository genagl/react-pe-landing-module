function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { Suspense } from "react";
import $ from "jquery";
import { __ } from "react-pe-utilities";
import Testanomial from "./Testanomial";
import SectionContent from "./SectionContent";
import { Loading } from 'react-pe-useful';
import { Carousel as ElCarousel } from '@trendyol-js/react-carousel';
import { CarouselLeftArrow, CarouselRightArrow } from "./Carousel";

class Testanomials extends SectionContent {
  getState() {
    this.car = /*#__PURE__*/React.createRef();
    return {};
  }

  componentDidMount() {
    let h = 100;
    const xx = $(".landing-testanomials .landing-testanomial .text");
    xx.each(e => {
      // console.log(e, $( xx[e] ).height(), h);
      h = Math.max($(xx[e]).height(), h);
    });
    xx.height(h);
  }

  renderContent(style) {
    const {
      composition,
      palette
    } = this.state;
    const {
      class_name,
      testanomials,
      dots,
      items,
      nav,
      autoplay,
      loop
    } = this.state.data; // console.log(this.state.data);

    const __testanomials = testanomials ? testanomials.map((e, i) => /*#__PURE__*/React.createElement(Testanomial, _extends({}, e, {
      i: i,
      key: i,
      palette: palette
    }))) : null;

    const options = {
      dots: typeof dots !== "undefined" ? !!dots : false,
      items: typeof composition.columns !== "undefined" ? composition.columns : 2,
      nav: typeof nav !== "undefined" ? !!nav : false,
      rewind: true,
      autoplay: typeof autoplay !== "undefined" ? !!autoplay : false,
      loop: typeof loop !== "undefined" ? !!loop : false,
      responsive: {
        0: {
          items: 1
        },
        760: {
          items: typeof items !== "undefined" ? items : 2
        }
      }
    };
    return /*#__PURE__*/React.createElement("div", {
      className: `landing-testanomials ${class_name}`,
      style: style
    }, /*#__PURE__*/React.createElement(Suspense, {
      fallback: /*#__PURE__*/React.createElement(Loading, null)
    }, /*#__PURE__*/React.createElement(ElCarousel, {
      show: options.items,
      swiping: true,
      swipeOn: -50,
      responsive: true,
      className: "landing-carousel-container",
      useArrowKeys: options.nav,
      infinite: options.loop,
      rightArrow: /*#__PURE__*/React.createElement(CarouselRightArrow, this.props.data),
      leftArrow: /*#__PURE__*/React.createElement(CarouselLeftArrow, this.props.data)
    }, __testanomials)));
  }

  is() {
    const {
      testanomials
    } = this.state.data;
    return testanomials && testanomials.length > 0;
  }

}

export default Testanomials;