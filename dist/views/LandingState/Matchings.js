function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { Button, ButtonGroup, Intent, Popover, Dialog, Icon, Tooltip, Position, Callout } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import SectionContent from "./SectionContent";
import Matching from "./matchings/Matching";
import ContactForm from "./ContactForm";
import { getFontNameByID } from "./data/PalettePresets";
import Style from "style-it";
import chroma from "chroma-js";

class Matchings extends SectionContent {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "timeOut", 1);

    _defineProperty(this, "start", () => {
      const {
        class_name,
        title,
        title_class_name,
        tytle_style,
        description,
        description_class_name,
        description_style,
        thumbnail,
        thumbnail_position_y,
        thumbnail_height,
        is_hover,
        hover_color,
        hover_size,
        hover_duration,
        hover_delay
      } = this.props.data;
      const hsl = is_hover && hover_color ? chroma(hover_color).get("hsl") : [];
      return Style.it(is_hover ? `
				.landing-matchings-thumbnail
				{
					transition: all ${hover_duration ? hover_duration : 0}ms ease-out!important;
					transition-delay: ${hover_delay ? hover_delay + "" : 0}ms!important;
					background-position-y: ${thumbnail_position_y};
					background-image:url( ${thumbnail});
				}
				.landing-matchings-thumbnail-cont:hover .landing-matchings-thumbnail.clr
				{ 
					filter:invert(28%) sepia(100%) hue-rotate(${-112 + hsl[0]}deg) saturate(${hsl[1]}) brightness(${hsl[2] * 75 + 50}%) contrast(120%);
					opacity: ${hsl[3]};
				}
				.landing-matchings-thumbnail-cont
				{
					height: ${thumbnail_height}px;
				}
				.landing-matchings-thumbnail-cont:hover .landing-matchings-thumbnail 
				{
					transform: scale( ${hover_size ? hover_size : null} );
				}
				` : `
				.landing-matchings-thumbnail-cont
				{
					height: ${thumbnail_height}px;
				}
				.landing-matchings-thumbnail
				{
					background-position-y: ${thumbnail_position_y};
					background-image:url( ${thumbnail});
				}`, /*#__PURE__*/React.createElement("div", {
        className: "landing-matchings-finish",
        style: {
          color: this.state.palette.main_text_color
        }
      }, /*#__PURE__*/React.createElement("div", {
        className: "landing-matchings-thumbnail-cont"
      }, /*#__PURE__*/React.createElement("div", {
        className: "landing-matchings-thumbnail",
        style: {}
      }), /*#__PURE__*/React.createElement("div", {
        className: "landing-matchings-thumbnail clr",
        style: {}
      })), /*#__PURE__*/React.createElement("div", {
        className: "landing-matchings-title " + title_class_name,
        style: {
          fontFamily: getFontNameByID(this.state.palette.card.title.fontFamilyID),
          ...this.getStyle(tytle_style)
        },
        dangerouslySetInnerHTML: {
          __html: title
        }
      }), /*#__PURE__*/React.createElement("div", {
        className: "landing-matchings-description " + description_class_name,
        style: { ...this.getStyle(description_style)
        },
        dangerouslySetInnerHTML: {
          __html: description
        }
      })));
    });

    _defineProperty(this, "finish", () => {
      const {
        class_name,
        matchings,
        next_button_label,
        finish_button_label,
        finish_button_action,
        finish_label,
        finish_label_class_name,
        finish_label_style,
        finish_inner_link_url,
        finish_inner_link_label,
        finish_contact_form
      } = this.props.data;
      const {
        palette
      } = this.state;

      switch (finish_button_action) {
        case "label":
          return /*#__PURE__*/React.createElement("div", {
            className: "landing-matchings-finish",
            style: {
              color: palette.main_text_color
            }
          }, /*#__PURE__*/React.createElement("div", {
            dangerouslySetInnerHTML: {
              __html: finish_label
            },
            className: "landing-matchings-finish-label " + finish_label_class_name,
            style: { ...this.getStyle(finish_label_style)
            }
          }), this.showResults());
          break;

        case "innerLink":
          return /*#__PURE__*/React.createElement("div", {
            className: "landing-matchings-finish",
            style: {
              color: palette.main_text_color
            }
          }, /*#__PURE__*/React.createElement("div", {
            dangerouslySetInnerHTML: {
              __html: finish_label
            },
            className: "landing-matchings-finish-label mb-4"
          }), this.showResults(), /*#__PURE__*/React.createElement(Link, {
            to: finish_inner_link_url,
            className: "landing-matching-btn"
          }, finish_inner_link_label));
          break;

        case "contactForm":
        default:
          return /*#__PURE__*/React.createElement("div", {
            className: "landing-matchings-finish",
            style: {
              color: palette.main_text_color
            }
          }, /*#__PURE__*/React.createElement(ContactForm, {
            data: { ...finish_contact_form,
              style: {
                width: "100%",
                padding: 0
              }
            },
            type: "contact_form",
            section_id: this.props.section_id,
            user: this.props.user
          }), this.showResults());
      }
    });

    _defineProperty(this, "onNext", evt => {
      if (this.state.disable) return;
      const chooses = [...this.state.chooses];
      chooses[this.state.current] = this.state.curr_answer_obj;
      console.log(chooses, this.state.current);
      this.setState({
        chooses,
        disable: true,
        pause: true
      });
      clearTimeout(this.timeOut);
      this.timeOut = setTimeout(() => {
        console.log(this.state.current);
        this.setState({
          disable: false,
          current: this.state.current + 1,
          pause: false
        });
      }, 100);
    });

    _defineProperty(this, "onFinish", evt => {
      const chooses = [...this.state.chooses];
      chooses[this.state.current] = this.state.curr_answer_obj;
      this.setState({
        chooses,
        current: this.state.current + 1
      });
      console.log(chooses);
      console.log(this.state.chooses);
    });

    _defineProperty(this, "onPrevous", evt => {
      const chooses = [...this.state.chooses];
      chooses.splice(this.state.current, 1);
      this.setState({
        current: this.state.current - 1,
        chooses
      });
    });

    _defineProperty(this, "onReplay", () => {
      this.setState({
        current: 0,
        curr_answer_obj: {},
        chooses: []
      });
    });

    _defineProperty(this, "onChoose", curr_answer_obj => {
      const {
        matchings
      } = this.props.data;
      const {
        current
      } = this.state;
      const obj = {
        curr_answer_obj: { ...curr_answer_obj,
          question: matchings[current - 1]
        }
      }; // console.log(obj, matchings, current, matchings[ current - 1 ]);

      this.setState(obj, () => {
        if (this.props.data.is_auto_switch) this.onNext();
      });
    });
  }

  getState() {
    return {
      current: 0,
      curr_answer_obj: {},
      chooses: []
    };
  }

  is() {
    const {
      matchings
    } = this.state.data;
    return Array.isArray(matchings) && matchings.length > 0;
  }

  renderContent(style) {
    const {
      composition,
      palette
    } = this.props;
    const {
      class_name,
      matchings,
      is_auto_switch,
      is_replay,
      start_button_label,
      start_button_class_name,
      start_button_style,
      next_button_label,
      next_button_class_name,
      next_button_style,
      finish_button_label,
      finish_button_class_name,
      finish_button_style,
      finish_button_action,
      finish_label,
      finish_inner_link_url,
      finish_inner_link_label
    } = this.props.data;
    const {
      current,
      pause
    } = this.state;
    const next = is_auto_switch ? null : current < matchings.length - 0 ? /*#__PURE__*/React.createElement("div", null, current > 0 ? /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-btn mr-2",
      style: { ...this.getStyle(next_button_style)
      },
      onClick: this.onPrevous
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-angle-left"
    })) : null, /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-btn " + next_button_class_name,
      style: { ...this.getStyle(next_button_style)
      },
      onClick: this.onNext
    }, __(next_button_label))) : current == matchings.length - 0 ? /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-btn mr-2",
      onClick: this.onPrevous
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-angle-left"
    })), /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-btn " + finish_button_class_name,
      style: { ...this.getStyle(finish_button_style)
      },
      onClick: this.onFinish
    }, __(finish_button_label))) : null;
    let cont = "";

    if (pause) {
      cont = /*#__PURE__*/React.createElement("div", {
        style: {
          height: 400
        }
      }, /*#__PURE__*/React.createElement("h1", {
        class: "display-1"
      }, "Blyams!"));
    } else if (current == matchings.length + 1) {
      cont = /*#__PURE__*/React.createElement("div", null, this.finish(), /*#__PURE__*/React.createElement("div", {
        className: "landing-matchings-next-cont"
      }, is_auto_switch ? null : /*#__PURE__*/React.createElement("div", {
        className: "landing-matching-btn mr-2",
        onClick: this.onPrevous
      }, /*#__PURE__*/React.createElement("i", {
        className: "fas fa-angle-left"
      })), is_replay ? /*#__PURE__*/React.createElement("div", {
        className: "landing-matching-btn mr-2",
        onClick: this.onReplay
      }, __("Replay")) : null));
    } else if (current == 0) {
      cont = /*#__PURE__*/React.createElement("div", null, this.start(), /*#__PURE__*/React.createElement("div", {
        className: "landing-matchings-next-cont"
      }, /*#__PURE__*/React.createElement("div", {
        className: "landing-matching-btn " + start_button_class_name,
        style: { ...this.getStyle(start_button_style)
        },
        onClick: this.onNext
      }, __(start_button_label))));
    } else {
      cont = /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Matching, _extends({
        key: current - 1,
        i: current - 1,
        section_id: this.props.section_id
      }, matchings[current - 1], {
        columns: composition.columns,
        onChoose: this.onChoose,
        palette: palette
      })), /*#__PURE__*/React.createElement("div", {
        className: "landing-matchings-next-cont"
      }, next));
    }

    return /*#__PURE__*/React.createElement("div", {
      className: `landing-matchings ${class_name || ""}`,
      style: { ...style
      }
    }, cont);
  }

  showResults() {
    const {
      chooses
    } = this.state;
    const {
      is_show_result,
      matchings
    } = this.props.data;

    if (is_show_result) {
      const results = chooses.map((result, i) => {
        if (!result.question) return;
        let right = result.question.answers.filter(answer => answer.is_right); // console.log( result.question.answers)
        // console.log(right)

        right = right[0] || {};
        const right_answer = result.is_right ? null : /*#__PURE__*/React.createElement("div", {
          className: "landing-matching-your-right ml-3"
        }, /*#__PURE__*/React.createElement("div", {
          className: "text-bold mr-2 w_100 "
        }, __("Right answer: ")), right.question_image ? /*#__PURE__*/React.createElement("div", {
          style: {
            backgroundImage: `url(${right.question_image})`
          },
          className: "landing-matching-result-img"
        }) : null, right.question_label, /*#__PURE__*/React.createElement("div", {
          className: "landing-matching-result-sing mt-auto"
        }, /*#__PURE__*/React.createElement(Icon, {
          icon: "tick",
          intent: Intent.SUCCESS
        })));
        return result.question ? /*#__PURE__*/React.createElement("div", {
          key: i,
          className: "p-4 tripped"
        }, /*#__PURE__*/React.createElement("div", {
          className: "landing-matching-single-result-title",
          style: {
            color: result.is_right ? "green" : "red"
          }
        }, `${__("Question")} ${i + 0}`), /*#__PURE__*/React.createElement("div", {
          className: "landing-match-single-result "
        }, result.question.question_image ? /*#__PURE__*/React.createElement("img", {
          src: result.question.question_image,
          className: "landing-matching-result-img"
        }) : null, /*#__PURE__*/React.createElement("div", {
          className: "mx-4"
        }, /*#__PURE__*/React.createElement("div", {
          className: "landing-match-single-result-title"
        }, result.question.title), result.question.description ? /*#__PURE__*/React.createElement("div", {
          className: "landing-match-single-result-description"
        }, result.question.description) : null), /*#__PURE__*/React.createElement("div", {
          className: "landing-matching-your-results"
        }, /*#__PURE__*/React.createElement("div", {
          className: "landing-matching-your-answer"
        }, /*#__PURE__*/React.createElement("div", {
          className: "text-bold mr-2 w_100 "
        }, __("Your answer: ")), result.question_image ? /*#__PURE__*/React.createElement("div", {
          style: {
            backgroundImage: `url(${result.question_image})`
          },
          className: "landing-matching-result-img"
        }) : null, result.question_label, /*#__PURE__*/React.createElement("div", {
          className: "landing-matching-result-sing mt-auto"
        }, /*#__PURE__*/React.createElement(Icon, {
          icon: result.is_right ? "tick" : "cross",
          intent: result.is_right ? Intent.SUCCESS : Intent.DANGER
        }))), right_answer))) : null;
      });
      return /*#__PURE__*/React.createElement("div", {
        className: "landing-matching-result"
      }, /*#__PURE__*/React.createElement("div", {
        className: "landing-matching-result-title"
      }, __("Your results: "), chooses.filter(e => e.is_right).length + __(" rights")), /*#__PURE__*/React.createElement("div", {
        className: ""
      }, results));
    }
  }

}

export default Matchings;