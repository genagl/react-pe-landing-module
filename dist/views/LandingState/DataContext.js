function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import matrix from "./data/matrix.json";
export default class DataContext {
  static upd(data) {
    // console.log( {...data} )
    DataContext.data = { ...data
    };
  }

  static clear() {
    DataContext.data.maxSectionID = 0;
    DataContext.data.maxFloatID = 0;
    DataContext.data.sections = [];
    DataContext.data.landing = matrix.landing.default;
  }

  static setLanding(data) {
    // console.log( data );
    DataContext.data.landing = data;
  }

  static updateGlobal(field, value) {
    if (!DataContext.data.globals) DataContext.data.globals = {};
    if (!DataContext.data.globals[field]) DataContext.data.globals[field] = [];
    const obj = [...DataContext.data.globals[field], ...value];
    DataContext.data.globals[field] = value;
    return DataContext.data.globals[field];
  }

  static getMaxSectionID(is_attept) {
    if (is_attept) {
      if (typeof DataContext.data.maxSectionID == "undefined") DataContext.data.maxSectionID = 0;
      ++DataContext.data.maxSectionID;
    }

    return DataContext.data.maxSectionID;
  }

  static getMaxFloatID(is_attept) {
    let id = !DataContext.data.maxFloatID ? 0 : DataContext.data.maxFloatID;

    if (is_attept) {
      id++;
      console.log(id);
    }

    return id;
  }

  static getSection(id) {
    console.log(id);
    const sec = DataContext.searchSectionById(DataContext.data.sections, id);
    return sec ? sec : {};
  }

  static getSectionJSON(id) {
    let section = typeof id == "number" ? DataContext.getSection(id) : id;
    section = { ...section,
      id: DataContext.getMaxSectionID(true)
    };
    return JSON.stringify(section);
  }

  static setSectionJSON(data) {
    return JSON.parse(data);
  }

  static searchSectionById(sections, id) {
    if (!Array.isArray(sections)) return;
    let res = null;
    sections.forEach(section => {
      if (section.id == id) {
        res = section;
        return;
      } else {
        let child = null;

        if (section.bottom_dilimiter && Array.isArray(section.bottom_dilimiter.sections)) {
          child = DataContext.searchSectionById(section.bottom_dilimiter.sections, id);
          if (child) res = child;
          return;
        }

        if (section.data && Array.isArray(section.data.sections)) {
          let ss = section.data.sections; //console.log(ss)

          child = DataContext.searchSectionById(ss, id);
          if (child) res = child;
          return;
        } //}

      }
    });
    return res;
  }

  static updateSection(id, data) {
    if (Array.isArray(DataContext.data.sections)) {
      // let section = DataContext.searchSectionById(DataContext.data.sections, id );
      // if(section)
      // {
      // 	section = {
      // 		...section,
      // 		...data
      // 	}	
      // }
      DataContext.data.sections.forEach((e, i) => {
        if (e.id == id) {
          //console.log( i, e.id )
          //console.log(DataContext.data.sections[i])
          DataContext.data.sections[i] = { ...DataContext.data.sections[i],
            ...data
          };
          return;
        }

        const containerType = DataContext.containerTypes.filter(u => u == e.type);

        if (containerType.length > 0) {
          if (Array.isArray(e.data.sections)) {
            e.data.sections.map((ee, ii) => {
              if (ee && ee.id == id) {
                DataContext.data.sections[i].data.sections[ii] = { ...DataContext.data.sections[i].data.sections[ii],
                  ...data
                };
              }
            });
          }
        }
      });
    }
  }

  static hideSection(id, is_hide) {
    if (Array.isArray(DataContext.data.sections)) {
      DataContext.data.sections.forEach((e, i) => {
        if (e.id == id) {
          DataContext.data.sections[i].is_hidden = is_hide;
        }

        const containerType = DataContext.containerTypes.filter(u => u == e.type);

        if (containerType.length > 0) {
          if (Array.isArray(e.data.sections)) {
            e.data.sections.map((ee, ii) => {
              if (ee && ee.id == id) {
                DataContext.data.sections[i][containerType[0]].sections[ii].is_hidden = is_hide;
              }
            });
          }
        }
      });
    }
  }

  static getFloatId(floatId) {
    if (Array.isArray(DataContext.data.sections)) {
      let _float;

      DataContext.data.sections.forEach((e, i) => {
        if (Array.isArray(e.floats)) {
          e.floats.forEach(ee => {
            if (ee && ee.float_id == floatId) {
              _float = ee;
            }

            const containerType = DataContext.containerTypes.filter(u => u == e.type);

            if (containerType.length > 0) {
              if (Array.isArray(e.data.sections)) {
                e.data.sections.map((ee, ii) => {
                  if (Array.isArray(ee.floats)) {
                    ee.floats.forEach(eee => {
                      if (eee.float_id == floatId) {
                        _float = eee;
                      }
                    });
                  }
                });
              } else {
                return {};
              }
            }
          });
        } else {
          return {};
        }
      });
      return _float || {};
    }

    return {};
  }

  static updateFloat(floatObj, float_id, section_id) {
    if (Array.isArray(DataContext.data.sections)) {
      let _float;

      DataContext.data.sections.forEach((e, i) => {
        if (e.menu.id == section_id) {
          console.log(section_id);
          if (!Array.isArray(e.floats)) DataContext.data.sections[i].floats = [];
          DataContext.data.sections[i].floats.push(floatObj);
        } else if (Array.isArray(e.floats)) {
          e.floats.forEach((ee, ii) => {
            if (ee && ee.float_id == float_id) {
              // _float = ee;
              DataContext.data.sections[i].floats[ii] = floatObj;
            }

            const containerType = DataContext.containerTypes.filter(u => u == e.type);

            if (containerType.length > 0) {
              if (Array.isArray(e.data.sections)) {
                e.data.sections.map((ee, ii) => {
                  if (ee && ee.menu.id == section_id) {
                    if (!Array.isArray(ee.floats)) DataContext.data.sections[i][containerType[0].type].sections[ii].floats = [];
                    DataContext.data.sections[i][containerType[0].type].sections[ii].floats.push(floatObj);
                  } else if (Array.isArray(ee.floats)) {
                    ee.floats.forEach((eee, iii) => {
                      if (eee.float_id == float_id) {
                        // _float = eee;
                        DataContext.data.sections[i].data.sections[ii].floats[iii] = floatObj;
                      }
                    });
                  }
                });
              }
            }
          });
        }
      });
    } else {
      return {};
    }
  }

  static deleteFloatId(floatId) {
    if (Array.isArray(DataContext.data.sections)) {
      DataContext.data.sections.forEach((e, i) => {
        if (Array.isArray(e.floats)) {
          e.floats.forEach((ee, ii) => {
            if (ee && ee.float_id == floatId) {
              delete DataContext.data.sections[i].floats[ii];
            }

            const containerType = DataContext.containerTypes.filter(u => u == e.type);

            if (containerType.length > 0) {
              if (Array.isArray(e.data.sections)) {
                e.data.sections.map((ee, ii) => {
                  if (Array.isArray(ee.floats)) {
                    ee.floats.forEach((eee, iii) => {
                      if (eee.float_id == floatId) {
                        delete DataContext.data.sections[i].data.sections[ii].floats[iii];
                      }
                    });
                  }
                });
              }
            }
          });
        }
      });
    }
  }

}

_defineProperty(DataContext, "containerTypes", ["carousel", "columns", "row"]);

_defineProperty(DataContext, "data", {});

_defineProperty(DataContext, "globals", []);