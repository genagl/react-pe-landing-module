import React from "react";
import { __ } from "react-pe-utilities";
import SectionContent from "./SectionContent";

class IFrame extends SectionContent {
  is() {
    const {
      src
    } = this.state.data;
    return src.toString().indexOf("http") === 0;
  }

  getState() {
    this.ref = /*#__PURE__*/React.createRef();
    return {};
  }

  componentDidMount() {//this.ref.current.contentWindow.console.error = function() { /* nop */ }
  }

  renderContent(style) {
    const {
      src,
      height,
      margin_top
    } = this.props.data;
    return /*#__PURE__*/React.createElement("div", {
      className: "landing-iframe",
      style: { ...style,
        height
      }
    }, /*#__PURE__*/React.createElement("iframe", {
      title: src,
      src: src,
      style: {
        height: height + margin_top,
        marginTop: -margin_top
      },
      ref: this.ref,
      loading: "lazy",
      sandbox: "allow-scripts allow-presentation allow-same-origin allow-forms allow-orientation-lock ",
      allow: "camera 'none'; microphone 'none'",
      seamless: true
    }, /*#__PURE__*/React.createElement("p", null, __("Old browser for show iframe"))));
  }

}

export default IFrame;