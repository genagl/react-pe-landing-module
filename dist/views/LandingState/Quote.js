import React from "react";
import SectionContent from "./SectionContent";
import Style from "style-it";
import { getFontNameByID } from "./data/PalettePresets";

class Quote extends SectionContent {
  is() {
    return this.props.data.text;
  }

  renderContent() {
    const {
      palette
    } = this.props;
    const {
      class_name,
      style,
      text,
      name,
      description,
      thumbnail
    } = this.props.data;
    const media = thumbnail ? Style.it(`.landing-quote-thumbnail 
        {
          background-image:url(${thumbnail});
        }`, /*#__PURE__*/React.createElement("div", {
      className: "landing-quote-thumbnail"
    })) : null;
    return /*#__PURE__*/React.createElement("div", {
      className: `landing-quote ${class_name}`,
      style: {
        color: palette.main_text_color,
        ...style
      }
    }, media, /*#__PURE__*/React.createElement("div", {
      className: "text",
      style: {
        fontFamily: getFontNameByID(palette.card.title.fontFamilyID),
        fontSize: palette.card.title.fontSize
      }
    }, /*#__PURE__*/React.createElement("span", {
      dangerouslySetInnerHTML: {
        __html: text
      }
    })), /*#__PURE__*/React.createElement("div", {
      className: "name"
    }, /*#__PURE__*/React.createElement("span", null, name)), /*#__PURE__*/React.createElement("div", {
      className: "description"
    }, /*#__PURE__*/React.createElement("span", null, description)));
  }

}

export default Quote;