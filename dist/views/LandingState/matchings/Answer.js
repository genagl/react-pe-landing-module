function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";

class Answer extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {});

    _defineProperty(this, "onChange", () => {
      const {
        question_label
      } = this.props; // console.log(question_label);

      this.setState({
        checked: question_label
      });
      setTimeout(() => {
        this.props.onChange(this.props);
      }, 500);
    });

    _defineProperty(this, "getStyle", styleObj => {
      const style = [];
      if (!styleObj) return style; // console.log( styleObj );

      Object.entries(styleObj).filter(e => // console.log( e );
      e[1] && e[1].field != "").forEach((e, i) => {
        // console.log( e );
        if (e[1] && e[1].field) {
          const attr = {};
          style[e.field] = e.value;
        } else {
          style[e[0]] = e[1];
        }
      }); // console.log( style );

      return style;
    });
  }

  render() {
    // console.log( this.state );
    const {
      question_label,
      question_image,
      question_descr,
      style,
      i,
      section_id
    } = this.props;
    return /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-answer-single ",
      style: this.getStyle(style)
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-answer-content"
    }, /*#__PURE__*/React.createElement("label", {
      className: "_check_ d-flex flex-column ",
      htmlFor: `matchinger_${i}_${section_id}`
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-answer-media"
    }, question_image ? /*#__PURE__*/React.createElement("img", {
      alt: "",
      src: question_image,
      className: "landing-matching-answer-image"
    }) : null), /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-answer-title "
    }, /*#__PURE__*/React.createElement("input", {
      name: "matchinger",
      type: "radio",
      className: "",
      checked: this.state.checked === question_label,
      onClick: this.onChange,
      onChange: this.onChange,
      id: `matchinger_${i}_${section_id}`
    }), question_label), question_descr ? /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-answer-descr"
    }, question_descr) : null)));
  }

}

export default Answer;