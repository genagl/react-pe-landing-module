function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { getFontNameByID } from "../data/PalettePresets";
import Answer from "./Answer";

class Matching extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onCheck", answer_obj => {
      // console.log(answer_obj);
      this.props.onChoose(answer_obj);
    });

    _defineProperty(this, "getStyle", styleObj => {
      const style = [];
      if (!styleObj) return style; // console.log( styleObj );

      Object.entries(styleObj).filter(e => // console.log( e );
      e[1] && e[1].field != "").forEach((e, i) => {
        // console.log( e );
        if (e[1] && e[1].field) {
          const attr = {};
          style[e.field] = e.value;
        } else {
          style[e[0]] = e[1];
        }
      }); // console.log( style );

      return style;
    });
  }

  render() {
    const {
      title,
      description,
      answers,
      question_image,
      style,
      palette
    } = this.props;

    const _answers = answers.map((e, i) => /*#__PURE__*/React.createElement(Answer, _extends({
      key: i
    }, e, {
      section_id: this.props.section_id,
      i: i,
      onChange: this.onCheck
    })));

    return /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-single ",
      style: this.getStyle(style)
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-content",
      style: {
        color: palette.main_text_color
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-media"
    }, /*#__PURE__*/React.createElement("img", {
      src: question_image,
      className: "landing-matching-image",
      alt: ""
    })), /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-title",
      style: { ...palette.card.title,
        fontFamily: getFontNameByID(palette.card.title.fontFamilyID)
      }
    }, title), /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-descr"
    }, description), /*#__PURE__*/React.createElement("div", {
      className: "landing-matching-answers",
      style: this.getStyle(style)
    }, _answers)));
  }

}

export default Matching;