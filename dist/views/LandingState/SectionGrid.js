import React from "react";

const SectionGrid = props => {
  const composition = props.composition ? Array(props.composition.columns).fill(1).map((e, i) => {
    return i == 0 ? null : /*#__PURE__*/React.createElement("div", {
      key: i,
      className: "landing-section-grid-left",
      style: {
        position: "absolute",
        left: `${100 / props.composition.columns * i}%`
      }
    });
  }) : null;
  return /*#__PURE__*/React.createElement("div", {
    className: "position-absolute w-100 h-100 untouchble"
  }, /*#__PURE__*/React.createElement("div", {
    className: "landing-section-grid"
  }, /*#__PURE__*/React.createElement("div", {
    className: "container mx-auto"
  }, /*#__PURE__*/React.createElement("div", {
    className: "landing-section-grid-vertical w-100 h-100"
  }, props.composition.is_blocked ? composition : null), !props.composition.is_blocked ? composition : null)));
};

export default SectionGrid;