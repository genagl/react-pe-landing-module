function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from "react";
import Section, { getStyle } from "./Section";

const Dilimiter = props => {
  const sections = Array.isArray(props.sections) ? props.sections.map((section, i) => {
    return /*#__PURE__*/React.createElement(Section, _extends({}, section, {
      background: {},
      section_id: section.id,
      key: i,
      i: i,
      user: props.user,
      is_edit: props.is_edit,
      level: props.level // onEdit={ onEdit}
      // onUp={ onUp}
      // onDn={ onDn}
      // onAdd={ onAdd}
      // onDouble={onDouble}
      // onRnv={onRnv}
      // onHide={onHide}
      // onRemoveFloat={onRemoveFloat}
      // onUpdateFloat={onUpdateFloat}
      // onClipboardCopy={onClipboardCopy}
      // onClipboardPaste={onClipboardPaste}
      // onWaypointEnter={onWaypointEnter}
      // onWaypointLeave={onWaypointLeave}
      // onFixedAdd={onFixedAdd}

    }));
  }) : null;
  let align = "justify-content-center";

  switch (props.align) {
    case "left":
      align = "justify-content-start";
      break;

    case "right":
      align = "justify-content-end";
      break;

    default:
      align = "justify-content-center";
  }

  return /*#__PURE__*/React.createElement("div", {
    className: "landing-section-dilimiter " + align,
    style: getStyle(props.style)
  }, sections);
};

export default Dilimiter;