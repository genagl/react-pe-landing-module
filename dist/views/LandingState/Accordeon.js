function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { Collapse } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import SectionContent from "./SectionContent";
import Section, { getDefault } from "./Section";
import Style from "style-it";
import { getFontNameByID } from "./data/PalettePresets";

class Accordeon extends SectionContent {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", { ...this.props,
      is_edit: false,
      is_opens: []
    });

    _defineProperty(this, "onOpen", i => {
      const is_opens = [...this.state.is_opens];
      is_opens[i] = !is_opens[i];
      this.setState({
        is_opens
      });
    });

    _defineProperty(this, "onEdit", (data, id) => {
      console.log("onEdit", id, data, this.state);
      const sections = Array.isArray(this.state.data.sections) ? [...this.state.data.sections] : [];
      const secs = [];
      sections.forEach(e => {
        if (e.id == data.id) {
          secs.push(data);
        } else {
          secs.push(e);
        }
      });
      this.setState({
        data: { ...this.state.data,
          sections: secs
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections: secs
        }
      }, this.props.id);
    });

    _defineProperty(this, "onUp", data => {
      console.log("onUp", data, this.state);
      const sections = [...this.state.data.sections];
      const sec = { ...sections[data]
      };
      sections.splice(data, 1);
      sections.splice(data - 1, 0, sec);
      console.log(sections);
      this.setState({
        data: { ...this.state.data,
          sections
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onDn", data => {
      console.log("onDn", data, this.state);
      const sections = [...this.state.data.sections];
      const sec = { ...sections[data]
      };
      sections.splice(data, 1);
      sections.splice(data + 1, 0, sec);
      console.log(sections);
      this.setState({
        data: { ...this.state.data,
          sections
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onAdd", data => {
      console.log("onAdd", data, this.state);
      const sections = [...this.state.data.sections];
      const sec = getDefault();
      sections.splice(data + 1, 0, sec);
      console.log(sections);
      this.setState({
        data: { ...this.state.data,
          sections
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onRnv", data => {
      console.log("onRnv", data, this.state.data.sections);
      const sections = [...this.state.data.sections];
      sections.splice(data, 1);
      console.log(sections);
      this.setState({
        data: { ...this.state.data,
          sections
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onHide", (id, is_hide) => {
      console.log("HIDE", id, is_hide);
    });

    _defineProperty(this, "onRemoveFloat", float_id => {});

    _defineProperty(this, "onUpdateFloat", (data, float_id, section_id) => {});
  }

  componentWillUpdate(nextProps) {
    if (nextProps.is_edit != this.state.is_edit) {
      this.setState({
        is_edit: nextProps.is_edit
      });
    }
  }

  is() {
    const {
      accordeon_sections
    } = this.state.data;
    return Array.isArray(accordeon_sections) && accordeon_sections.length > 0;
  }

  renderContent(style) {
    const {
      palette
    } = this.props;
    const {
      class_name,
      accordeon_sections,
      proportia,
      design_type,
      sign_type
    } = this.props.data;

    let __sign_type;

    switch (sign_type) {
      case "caret":
        __sign_type = /*#__PURE__*/React.createElement("div", {
          className: "landing-accordeon-sign"
        }, /*#__PURE__*/React.createElement("i", {
          className: "fas fa-caret-right"
        }));
        break;

      case "chevron":
        __sign_type = /*#__PURE__*/React.createElement("div", {
          className: "landing-accordeon-sign"
        }, /*#__PURE__*/React.createElement("i", {
          className: "fas fa-chevron-right"
        }));
        break;

      case "crest":
        __sign_type = /*#__PURE__*/React.createElement("div", {
          className: "landing-accordeon-sign"
        }, /*#__PURE__*/React.createElement("i", {
          className: "fas fa-plus"
        }));
        break;

      case "hothing":
      default:
        break;
    }

    return /*#__PURE__*/React.createElement("div", {
      className: `landing-accordeon ${this.props.data.class_name}`,
      style: this.getStyle(style)
    }, accordeon_sections.map((e, i) => {
      const estyle = { ...this.getStyle(e.style),
        width: "100%"
      };
      return /*#__PURE__*/React.createElement("div", {
        style: estyle,
        className: `landing-accordeon-section ${e.className} ${design_type} ${this.state.is_opens[i] ? " open " : " "}`,
        key: i
      }, Style.it(`.landing-accordeon-button {
											font-family:${getFontNameByID(palette.card.title.fontFamilyID)};
											font-weight:${getFontNameByID(palette.card.title.fontWeight)};
											font-size:${getFontNameByID(palette.card.title.fontSize)};
										}`, /*#__PURE__*/React.createElement("div", {
        className: `${"landing-accordeon-button " + " "}${design_type}`,
        i: i,
        onClick: () => this.onOpen(i)
      }, __(e.title), __sign_type)), /*#__PURE__*/React.createElement(Collapse, {
        isOpen: this.state.is_opens[i]
      }, Array.isArray(e.section) ? e.section.map((section, index) => /*#__PURE__*/React.createElement(Section, _extends({
        key: index
      }, section, {
        style: {
          height: "100%",
          ...this.getStyle(section.style)
        },
        i: index,
        user: this.props.user,
        is_edit: this.state.is_edit,
        level: this.props.level + 1,
        onEdit: this.onEdit,
        onUp: this.onUp,
        onDn: this.onDn,
        onAdd: this.onAdd,
        onRnv: this.onRnv
      }))) : /*#__PURE__*/React.createElement("div", null)));
    }));
  }

}

export default Accordeon;