function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Suspense, lazy, Component } from "react";
import { __ } from "react-pe-utilities";
import SectionContent from "./SectionContent";
import Section, { getDefault } from "./Section";
import { Loading } from 'react-pe-useful';
import { Carousel as ElCarousel } from '@trendyol-js/react-carousel';
import Style from "style-it";

class Carousel extends SectionContent {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onEdit", (data, id) => {
      console.log("onEdit", id, data, this.state);
      const sections = [...this.state.data.sections];
      const secs = [];
      sections.forEach(e => {
        if (e.id == data.id) {
          secs.push(data);
        } else {
          secs.push(e);
        }
      });
      this.setState({
        data: { ...this.state.data,
          sections: secs
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections: secs
        }
      }, this.props.id);
    });

    _defineProperty(this, "onUp", data => {
      console.log("onUp", data, this.state);
      const sections = [...this.state.data.sections];
      const sec = { ...sections[data]
      };
      sections.splice(data, 1);
      sections.splice(data - 1, 0, sec);
      console.log(sections);
      this.setState({
        data: { ...this.state.data,
          sections
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onDn", data => {
      console.log("onDn", data, this.state);
      const sections = [...this.state.data.sections];
      const sec = { ...sections[data]
      };
      sections.splice(data, 1);
      sections.splice(data + 1, 0, sec);
      console.log(sections);
      this.setState({
        data: { ...this.state.data,
          sections
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onAdd", data => {
      console.log("onAdd", data, this.state);
      const sections = [...this.state.data.sections];
      const sec = getDefault();
      sections.splice(data + 1, 0, sec);
      console.log(sections);
      this.setState({
        data: { ...this.state.data,
          sections
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onRnv", data => {
      console.log("onRnv", data, this.state.data.sections);
      const sections = [...this.state.data.sections];
      sections.splice(data, 1);
      console.log(sections);
      this.setState({
        data: { ...this.state.data,
          sections
        }
      });
      this.props.onEdit({ ...this.state,
        data: { ...this.state.data,
          sections
        }
      }, this.props.id);
    });

    _defineProperty(this, "onHide", (id, is_hide) => {
      console.log("HIDE", id, is_hide);
    });

    _defineProperty(this, "onRemoveFloat", float_id => {});

    _defineProperty(this, "onUpdateFloat", (data, float_id, section_id) => {});
  }

  getState() {
    this.car = /*#__PURE__*/React.createRef();
    return {};
  }

  renderContent(style) {
    const {
      composition,
      is_edit
    } = this.props;
    const {
      class_name,
      items,
      dots,
      dots_type,
      dots_design,
      dots_color,
      nav,
      nav_type,
      nav_design,
      nav_color,
      autoplay,
      loop,
      sections
    } = this.props.data;

    const __sections = sections && sections.length > 0 ? sections.map((e, i) => /*#__PURE__*/React.createElement(Section, _extends({
      lasy_load_type: "none",
      background: {
        color: "#00000001"
      }
    }, e, {
      key: i,
      i: i,
      is_edit: is_edit,
      level: this.props.level + 1,
      onEdit: this.onEdit,
      onUp: this.onUp,
      onDn: this.onDn,
      onAdd: this.onAdd,
      onRnv: this.onRnv,
      onHide: this.onHide,
      onRemoveFloat: this.onRemoveFloat,
      onUpdateFloat: this.onUpdateFloat,
      is_no_waypoint: true
    }))) : null;

    const options = {
      dots: typeof dots != "undefined" ? !!dots : false,
      items: typeof composition.columns != "undefined" ? composition.columns : 1,
      nav: typeof nav != "undefined" ? !!nav : false,
      rewind: true,
      autoplay: typeof autoplay != "undefined" ? autoplay ? !is_edit : false : !is_edit,
      loop: typeof loop != "undefined" ? !!loop : false,
      mouseDrag: !is_edit,
      touchDrag: !is_edit,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: typeof composition.columns != "undefined" ? composition.columns : 1
        }
      }
    }; // const events = { }

    return /*#__PURE__*/React.createElement("div", {
      className: `landing-carousel landing-element ${class_name} ${nav_type} ${nav_design} ${nav_color} ${dots_type} ${dots_design} ${dots_color}`,
      style: this.getStyle(style)
    }, /*#__PURE__*/React.createElement(Suspense, {
      fallback: /*#__PURE__*/React.createElement(Loading, null)
    }, /*#__PURE__*/React.createElement(ElCarousel, {
      show: options.items,
      swiping: options.mouseDrag,
      swipeOn: -50,
      responsive: true,
      className: "landing-carousel-container",
      useArrowKeys: options.nav,
      infinite: options.loop,
      rightArrow: /*#__PURE__*/React.createElement(CarouselRightArrow, _extends({}, this.props.data, options)),
      leftArrow: /*#__PURE__*/React.createElement(CarouselLeftArrow, _extends({}, this.props.data, options))
    }, __sections)));
  }

  is() {
    const {
      sections
    } = this.state.data;
    return sections && sections.length > 0;
  }

}

export default Carousel;
export const CarouselRightArrow = props => {
  return /*#__PURE__*/React.createElement(CarouselArrow, _extends({}, props, {
    isLeft: false
  }));
};
export const CarouselLeftArrow = props => {
  return /*#__PURE__*/React.createElement(CarouselArrow, _extends({}, props, {
    isLeft: true
  }));
};
export const CarouselArrow = props => {
  if (!props.nav) return null;
  let dot = "";

  switch (props.nav_color) {
    case "owllight":
      dot += `
        background-color: #FFF;
        color: #111;`;
      break;

    case "owldark":
    default:
      dot += `
        background-color: #111;
        color: #EEE;`;
      break;
  }

  switch (props.nav_type) {
    case "rounded":
      dot += `
        border-Radius:100%;`;
      break;

    case "squared":
      dot += `
      border-Radius:0%;`;
      break;

    case "half_rounded":
      dot += props.isLeft ? `
        border-Radius:0% 100% 100% 0;` : `
        border-Radius:100% 0 0 100%;`;
      break;

    case "transparent":
      dot += `
        background-color: transparent;`;
      break;

    default:
      dot += "";
  }

  return /*#__PURE__*/React.createElement(React.Fragment, null, Style.it(`.landing-carousel-arrow
      {
        position: absolute;
        z-Index:  1000;
        top:      calc(50% - 20px);
        left:     ${props.isLeft ? 0 : "auto"};
        right :   ${props.isLeft ? "auto" : 0};
        height:   55px;
        width:    55px;
        font-size:20px;
        display:  flex;
        justify-content: center;
        align-items: center;
        cursor: 'pointer';
        ${dot}
      }`, /*#__PURE__*/React.createElement("div", {
    className: "landing-carousel-arrow"
  }, /*#__PURE__*/React.createElement(CarouselArrowLabel, props))));
};

const CarouselArrowLabel = props => {
  let arr;

  switch (props.nav_design) {
    case "arrowsed":
      arr = props.isLeft ? /*#__PURE__*/React.createElement("i", {
        className: "fas fa-arrow-left"
      }) : /*#__PURE__*/React.createElement("i", {
        className: "fas fa-arrow-right"
      });
      break;

    case "angle":
      arr = props.isLeft ? /*#__PURE__*/React.createElement("i", {
        className: "fas fa-angle-left"
      }) : /*#__PURE__*/React.createElement("i", {
        className: "fas fa-angle-right"
      });
      break;

    case "double-angle":
      arr = props.isLeft ? /*#__PURE__*/React.createElement("i", {
        className: "fas fa-angle-double-left"
      }) : /*#__PURE__*/React.createElement("i", {
        className: "fas fa-angle-double-right"
      });
      break;

    case "hand":
      arr = props.isLeft ? /*#__PURE__*/React.createElement("i", {
        className: "fas fa-hand-point-left"
      }) : /*#__PURE__*/React.createElement("i", {
        className: "fas fa-hand-point-right"
      });
      break;

    case "caret":
    default:
      arr = props.isLeft ? /*#__PURE__*/React.createElement("i", {
        className: "fas fa-caret-left mr-1"
      }) : /*#__PURE__*/React.createElement("i", {
        className: "fas fa-caret-right ml-1"
      });
      break;
  }

  return /*#__PURE__*/React.createElement(React.Fragment, null, arr);
};