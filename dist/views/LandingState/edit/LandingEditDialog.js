function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, ButtonGroup, Intent, Popover, Position, Drawer } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import InputForm from "./InputForm";

class LandingEditDialog extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onEdit", () => {
      //console.log(this.state.data)
      this.props.onEdit(this.state.data);
    });

    this.state = { ...props
    };
  }

  componentWillUpdate(nextProps) {
    const state = {};
    Object.keys(nextProps).forEach(e => {
      if (this.state[e] != nextProps[e]) state[e] = nextProps[e];
    }); // console.log(nextProps, this.state, state);

    if (Object.keys(state).length > 0) {
      this.setState(state);
    }
  }

  render() {
    //console.log( this.state.data );
    return /*#__PURE__*/React.createElement(Drawer, {
      isOpen: this.state.isOpen,
      onClose: this.props.onClose,
      title: __("Landing Settings"),
      position: Position.RIGHT,
      className: "little3 drawer",
      usePortal: true,
      backdropClassName: "landing-drawer-bg",
      size: 800
    }, /*#__PURE__*/React.createElement("div", {
      className: "pt-0 px-0 overflow-y-auto flex-grow-100 bg-tripp"
    }, /*#__PURE__*/React.createElement("div", {
      className: "m-4"
    }, /*#__PURE__*/React.createElement(InputForm, {
      source: "landing",
      data: this.state.data,
      on: (value, field) => this.onField(value, field, "data"),
      onAdd: this.props.onAdd,
      onDownload: this.props.onDownload,
      onLoadChange: this.props.onLoadChange,
      onClearOpen: this.props.onClearOpen
    }))), /*#__PURE__*/React.createElement(ButtonGroup, {
      className: "mx-auto py-3"
    }, /*#__PURE__*/React.createElement(Button, {
      onClick: this.onEdit
    }, __("Update"))));
  }

  onField(value, field) {
    const {
      data
    } = this.state; // { ...this.state.data}

    data[field] = value; //console.log(data)
    // this.setState( { data } );
  }

}

export default LandingEditDialog;