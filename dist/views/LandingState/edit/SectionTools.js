import { Button, Intent } from "@blueprintjs/core";
import React from "react";
import { __ } from "react-pe-utilities";

const SectionTools = ({
  sectionID,
  palette,
  level,
  dopEditTop,
  is_edit,
  is_open,
  is_hidden,
  onOpen,
  onHide,
  onRnv,
  onDialogOpen,
  onDouble,
  onAdd
}) => {
  return is_edit ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Button, {
    icon: !is_open ? "wrench" : "cross",
    className: "position-absolute right z-index-300 m-2 hidden",
    intent: Intent.NONE,
    onClick: onOpen,
    style: {
      top: dopEditTop
    }
  }), /*#__PURE__*/React.createElement("div", {
    className: "tool-palette p-2",
    style: {
      //right: is_open ? 0 : -50,
      top: level * 21 + 31 + dopEditTop
    }
  }, /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    icon: "application",
    "data-hint": __("Design template"),
    className: " section-tool-btn hint hint--right",
    "tab-bar-id": "template",
    onClick: onDialogOpen
  }), /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    icon: /*#__PURE__*/React.createElement("i", {
      className: "fas fa-coffee"
    }),
    "data-hint": __("Change content type"),
    className: " section-tool-btn hint hint--right",
    "tab-bar-id": "types",
    onClick: onDialogOpen
  }), /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    icon: /*#__PURE__*/React.createElement("i", {
      className: "fas fa-heading"
    }),
    "data-hint": __("Title"),
    className: " section-tool-btn hint hint--right",
    "tab-bar-id": "title",
    onClick: onDialogOpen
  }), /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    icon: "page-layout",
    "data-hint": __("Composition"),
    className: " section-tool-btn hint hint--right",
    "tab-bar-id": "composition",
    onClick: onDialogOpen
  }), /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    icon: /*#__PURE__*/React.createElement("i", {
      className: "fas fa-paste"
    }),
    "data-hint": __("Smart background"),
    className: " section-tool-btn hint hint--right",
    "tab-bar-id": "bg",
    onClick: onDialogOpen
  }), /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    icon: /*#__PURE__*/React.createElement("i", {
      className: "fas fa-grip-lines"
    }),
    "data-hint": __("Bottom dilimiter"),
    className: " section-tool-btn hint hint--right",
    "tab-bar-id": "dilimiter",
    onClick: onDialogOpen
  }), /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    icon: "plus",
    onClick: onAdd,
    className: " section-tool-btn hint hint--right",
    "data-hint": __("Add Secor after")
  }), /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    onClick: onDouble,
    className: " section-tool-btn hint hint--right",
    "data-hint": __("Double this Secor")
  }, /*#__PURE__*/React.createElement("i", {
    className: "far fa-clone"
  })), /*#__PURE__*/React.createElement(Button, {
    icon: is_hidden ? "eye-open" : "eye-off",
    className: " section-tool-btn hint hint--right",
    "data-hint": __(is_hidden ? "Show section" : "Hide section"),
    intent: Intent.NONE,
    onClick: onHide
  }), /*#__PURE__*/React.createElement(Button, {
    intent: Intent.DANGER,
    icon: "trash",
    "data-hint": __("Remove Section"),
    className: " section-tool-btn hint hint--right",
    onClick: onRnv
  }))) : null;
};

export default SectionTools;