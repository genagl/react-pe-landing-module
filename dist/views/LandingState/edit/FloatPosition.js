function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { __ } from "react-pe-utilities";
import { LayoutIcon } from 'react-pe-useful';
import matrix from "../data/matrix";
import FieldInput from "react-pe-scalars";
export function positions() {
  return [{
    id: "lg",
    title: "Large",
    icon: "fas fa-tv"
  }, {
    id: "xl",
    title: "Screen",
    icon: "fas fa-desktop"
  }, {
    id: "sm",
    title: "Tablet",
    icon: "fas fa-tablet-alt"
  }, {
    id: "mc",
    title: "Mobile",
    icon: "fas fa-mobile-alt"
  }];
}
export function defaultPosition() {
  return {
    x: {
      value: 0,
      ei: "px",
      dst: "L"
    },
    y: {
      value: 0,
      ei: "px",
      dst: "T"
    },
    w: {
      value: 0,
      ei: "px",
      dst: ""
    },
    h: {
      value: 0,
      ei: "px",
      dst: ""
    }
  };
}

class FloatPosition extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "updateWindowDimensions", () => {
      this.setState({
        dwidth: document.body.clientWidth,
        dheight: document.body.clientHeight,
        current: this.getScreenSize(document.body.clientWidth)
      });
    });

    _defineProperty(this, "onPosition", (field, value) => {
      const pos = Object.keys(this.state.position[this.state.current]).length > 0 ? this.state.position[this.state.current] : this.state.position.mc ? { ...this.state.position.mc
      } : defaultPosition();
      pos[field] = value;
      this.props.onPosition(pos, this.state.current);
    });

    _defineProperty(this, "onSwitch", evt => {
      const type = evt.currentTarget.getAttribute("type");
      this.setState({
        hide: true
      }, () => this.setState({
        current: type,
        hide: false
      }));
    });

    this.state = { ...props,
      current: "lg"
    };
  }

  componentWillReceiveProps(nextProps) {
    const state = {};

    if (nextProps.float_id != this.state.float_id) {
      state.float_id = nextProps.float_id;
    }

    if (nextProps.position != this.state.position) {
      state.position = nextProps.position;
    }

    if (Object.keys(state).length > 0) this.setState(state);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  getScreenSize(dwidth) {
    let p = "mc";

    if (dwidth > 940) {
      p = "lg";
    } else if (dwidth > 760) {
      p = "xl";
    } else if (dwidth > 560) {
      p = "sm";
    }

    console.log(p, dwidth);
    return p;
  }

  render() {
    const {
      current
    } = this.state;
    const pos = Object.keys(this.state.position[current]).length > 0 ? this.state.position[current] : this.state.position.mc ? { ...this.state.position.mc
    } : defaultPosition();
    const btns = []; // console.log(current, pos);

    positions().forEach((e, i) => {
      const cl = e.id == current ? "active " : " ";
      btns.push( /*#__PURE__*/React.createElement("div", {
        key: i,
        type: e.id,
        className: `l-icon ${cl}`,
        onClick: this.onSwitch
      }, /*#__PURE__*/React.createElement(LayoutIcon, {
        src: `${e.icon} fa-3x pt-3`,
        className: "layout-icon mb-1"
      }), /*#__PURE__*/React.createElement("div", {
        className: ""
      }, __(e.title))));
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "d-flex px-4 pb-2 flex-column"
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex justify-content-center pb-3 flex-wrap"
    }, btns), /*#__PURE__*/React.createElement("div", {
      className: "flex-grow-100"
    }, this.posit(pos)));
  }

  posit(pos) {
    if (this.state.hide) return;
    return /*#__PURE__*/React.createElement("div", {
      className: ""
    }, /*#__PURE__*/React.createElement(FieldInput, _extends({
      field: "x",
      key: "x",
      title: matrix.ScreenSize.x.title,
      type: "ScreenSize",
      id: this.state.float_id
    }, matrix.ScreenSize.x, {
      on: value => this.onPosition("x", value),
      onChange: value => this.onPosition("x", value),
      editable: true,
      value: pos.x,
      vertical: false
    })), /*#__PURE__*/React.createElement(FieldInput, _extends({
      field: "y",
      key: "y",
      title: matrix.ScreenSize.y.title,
      type: "ScreenSize",
      id: this.state.float_id
    }, matrix.ScreenSize.y, {
      on: value => this.onPosition("y", value),
      onChange: value => this.onPosition("y", value),
      editable: true,
      value: pos.y,
      vertical: false
    })), /*#__PURE__*/React.createElement(FieldInput, _extends({
      field: "w",
      key: "w",
      title: matrix.ScreenSize.w.title,
      type: "ScreenSize",
      id: this.state.float_id
    }, matrix.ScreenSize.w, {
      on: value => this.onPosition("w", value),
      onChange: value => this.onPosition("w", value),
      editable: true,
      value: pos.w,
      vertical: false
    })), /*#__PURE__*/React.createElement(FieldInput, _extends({
      field: "h",
      key: "h",
      title: matrix.ScreenSize.h.title,
      type: "ScreenSize",
      id: this.state.float_id,
      on: value => this.onPosition("h", value),
      onChange: value => this.onPosition("h", value)
    }, matrix.ScreenSize.h, {
      editable: true,
      value: pos.h,
      vertical: false
    })));
  }

}

export default FloatPosition;