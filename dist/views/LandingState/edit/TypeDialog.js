function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Fragment } from "react";
import { Button, ButtonGroup, Intent, Popover, Position, Tabs, Tab, Menu, MenuItem, Dialog } from "@blueprintjs/core";
import { AppToaster } from 'react-pe-useful';
import { __ } from "react-pe-utilities";
import { LayoutIcon } from 'react-pe-useful';
import InputForm from "./InputForm";
import FlaotDettingDialog from "./FlaotDettingDialog";
import matrix from "../data/matrix";
import section_groups from "../data/section_groups.json";
import FieldInput from "react-pe-scalars";
import DataContext from "../DataContext";
import PaletteSingleForm from "../../../widgets/landingObject/PaletteSingleForm";
import PalettePresets from "../data/PalettePresets";
import { components } from "../data/components";
import { Issue } from "react-pe-useful";
import $ from "jquery";

class TypeDialog extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "car", /*#__PURE__*/React.createRef());

    _defineProperty(this, "onTab", navbarTabId => this.setState({
      navbarTabId
    }));

    _defineProperty(this, "getPalette", () => {
      let palette = PalettePresets().filter(palette => palette.id == this.state.current_template_id)[0];
      if (!palette) palette = PalettePresets()[0];
      return palette;
    });

    _defineProperty(this, "onTemplateCheck", (evt, e) => {
      //console.log(this.state.current_template_id, e.id);
      const newPalette = PalettePresets().filter(palette => palette.id == e.id)[0];

      if (newPalette) {
        this.setState({
          current_template_id: e.id,
          background: { ...this.state.background,
            opacity: 1,
            color: newPalette.background_color,
            size: newPalette.background.size,
            image: newPalette.background.tile,
            tile_opacity: newPalette.background.tileOpacity,
            tile_size_px: newPalette.background.tile_size_px,
            repeat: newPalette.background.repeat
          }
        });
      } // console.log(this.state);

    });

    _defineProperty(this, "onType", evt => {
      const id = evt.currentTarget.getAttribute("id");
      console.log(id);
      this.setState({
        currentTypeGroup: id
      });
    });

    _defineProperty(this, "onStyle", val => {
      // console.log( val );
      this.setState({
        style: val
      });
    });

    _defineProperty(this, "onId", value => {
      this.setState({
        id: value
      });
    });

    _defineProperty(this, "onClassName", value => {
      this.setState({
        class_name: value
      });
    });

    _defineProperty(this, "onTypeSwitch", evt => {
      const current_type = evt.currentTarget.getAttribute("type");
      let data = {};

      if (matrix[current_type].default) {
        data = {
          data: { ...matrix[current_type].default
          },
          composition: matrix[current_type].default.composition
        };
        delete data.data.hidden;
        delete data.data.composition;
      } // console.log( data, current_type );


      this.setState({
        data,
        current_type,
        is_change_type_enbl: current_type != this.state.type
      });
    });

    _defineProperty(this, "onClick", () => {
      //console.log(this.state);
      this.props.onChange(this.state.current_type, this.state);
    });

    _defineProperty(this, "onClipboardCopy", evt => {
      console.log("TypeDialog.onClipboardCopy");
      if (this.props.onClipboardCopy) this.props.onClipboardCopy(this.state);
    });

    _defineProperty(this, "onClipboardPaste", evt => {
      if (this.props.onClipboardPaste) {
        this.props.onClipboardPaste();
        console.log("TypeDialog.onClipboardPaste");
      }
    });

    _defineProperty(this, "on", (value, field) => {// console.log( value,  field );
    });

    _defineProperty(this, "onColumnCount", (value, field, block) => {
      //console.log( value, field, block );
      const state = { ...this.state
      };
      if (!state[block]) state[block] = {};
      state[block][field] = value; //console.log( state )

      this.setState(state);
    });

    _defineProperty(this, "onField", (value, field, block, dopol) => {
      console.log(value, field, block);
      const state = { ...this.state
      };
      if (!state[block]) state[block] = {};
      state[block][field] = value; // console.log( state );

      if (this.state.type == "cards") {
        if (field == "fields" && dopol && Array.isArray(dopol)) {
          state.data.cards = dopol;
        }
      } // console.log( state.data )


      this.setState(state);
    });

    _defineProperty(this, "onUpdateFloat", (data, float_id) => {
      this.props.onUpdateFloat(data, float_id);
    });

    _defineProperty(this, "onClipboardCopyPRM", data => {
      console.log("AAAAAAAA", data);
      $("body").append(`<div style='position:absolute; z-index:-100; width:100%; top:0; left:0;'><textarea style='width:100%;' id='myInput'>${DataContext.getSectionJSON(data)}</textarea></div>`);
      const copyText = document.getElementById("myInput");
      copyText.select();
      copyText.setSelectionRange(0, 99999999999999999999);
      document.execCommand("copy");
      $("#myInput").remove();
      AppToaster.show({
        intent: Intent.SUCCESS,
        icon: "tick",
        duration: 10000,
        message: __("Section copy to clipbord")
      });
    });

    _defineProperty(this, "onClipboardPastePRM", data => {
      //console.log("::onClipboardPastePRM")
      //console.log(data)
      //console.log(this.state)
      switch (this.state.type) {
        case "cards":
          let state = { ...this.state,
            data: { ...this.state.data,
              ...data.data,
              data: { ...this.state.data.data,
                ...data.data
              }
            }
          }; //console.log( state )

          this.setState(state);
          break;
      }
    });

    this.state = { ...props,
      current_type: props.type,
      currentTypeGroup: matrix[props.type].sparams.group //navbarTabId: "prm"

    };
  }

  render() {
    // console.log( this.state );
    const hiddenButton = this.state.is_hidden ? /*#__PURE__*/React.createElement(Button, {
      className: "mb-1",
      icon: "eye-open",
      fill: true,
      onClick: () => this.props.onHide(this.state.menu.id, 0)
    }, __("Show section for users")) : /*#__PURE__*/React.createElement(Button, {
      className: "mb-1",
      icon: "eye-off",
      fill: true,
      onClick: () => this.props.onHide(this.state.menu.id, 1)
    }, __("Hide section for users"));
    return [/*#__PURE__*/React.createElement("div", {
      className: "pt-0 px-0 overflow-y-auto flex-grow-100 bg-tripp position-relative ",
      key: 1
    }, /*#__PURE__*/React.createElement(Tabs, {
      onChange: this.onTab,
      selectedTabId: this.state.navbarTabId,
      animate: false,
      id: "TabsType",
      key: "horizontal",
      vertical: false,
      className: "tab-light-head"
    }, /*#__PURE__*/React.createElement(Tab, {
      id: "prm",
      title: /*#__PURE__*/React.createElement("div", {
        className: "small"
      }, " ", __(this.state.current_title), " "),
      panel: /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(InputForm, _extends({}, this.state, {
        source: this.state.current_type,
        id: this.state.id,
        data: this.state.data,
        sourceType: "section",
        on: (value, field, dopol) => this.onField(value, field, "data", dopol),
        onClipboardPaste: this.onClipboardPastePRM,
        onClipboardCopy: this.onClipboardCopyPRM
      })))
    }), /*#__PURE__*/React.createElement(Tab, {
      id: "title",
      title: /*#__PURE__*/React.createElement("div", {
        className: "small"
      }, __("Title")),
      panel: this.title()
    }), /*#__PURE__*/React.createElement(Tab, {
      id: "composition",
      title: /*#__PURE__*/React.createElement("div", {
        className: "small"
      }, __("Composition")),
      panel: this.composition()
    }), /*#__PURE__*/React.createElement(Tab, {
      className: "tab_hidden",
      id: "floats",
      title: /*#__PURE__*/React.createElement("div", {
        className: "small"
      }, __("Floats")),
      panel: this.floats()
    }), /*#__PURE__*/React.createElement(Tab, {
      id: "html",
      title: /*#__PURE__*/React.createElement("div", {
        className: "small"
      }, __("Style")),
      panel: this.html()
    }), /*#__PURE__*/React.createElement(Tab, {
      id: "dilimiter",
      title: /*#__PURE__*/React.createElement("div", {
        className: "small"
      }, __("Dilimiter")),
      panel: this.dilimiter()
    }), /*#__PURE__*/React.createElement(Tab, {
      id: "menu",
      className: "tab_hidden",
      title: /*#__PURE__*/React.createElement("div", {
        className: "small"
      }, __("Menu")),
      panel: this.menu()
    }), /*#__PURE__*/React.createElement(Tab, {
      id: "bg",
      title: /*#__PURE__*/React.createElement("div", {
        className: "small"
      }, __("Back")),
      panel: /*#__PURE__*/React.createElement(InputForm, _extends({}, this.state.background, {
        source: "Background",
        id: this.state.id,
        data: this.state.background,
        sourceType: "Background",
        palette: this.getPalette(),
        on: (value, field, dopol) => this.onField(value, field, "background", dopol)
      }))
    }), /*#__PURE__*/React.createElement(Tab, {
      id: "template",
      className: "tab_hidden",
      title: /*#__PURE__*/React.createElement("div", {
        className: "small"
      }, __("Template")),
      panel: this.template()
    }), /*#__PURE__*/React.createElement(Tab, {
      id: "types",
      className: "tab_hidden",
      title: /*#__PURE__*/React.createElement("div", {
        className: "small"
      }, __("Type")),
      panel: this.types()
    }), /*#__PURE__*/React.createElement(Tab, {
      id: "visible",
      className: "tab_hidden",
      title: "",
      panel: /*#__PURE__*/React.createElement("div", {
        className: "p-4"
      }, /*#__PURE__*/React.createElement(FieldInput, _extends({
        field: "visible_lg",
        key: "visible_lg",
        title: "Show at large screen",
        id: this.state.id
      }, matrix.section.visible_lg, {
        on: val => this.onVisible("lg", val),
        onChange: val => this.onVisible("lg", val),
        editable: true,
        value: this.state.visible_lg,
        vertical: false
      })), /*#__PURE__*/React.createElement(FieldInput, _extends({
        field: "visible_sm",
        key: "visible_sm",
        title: "Show at laptop screen",
        id: this.state.id
      }, matrix.section.visible_sm, {
        on: val => this.onVisible("sm", val),
        onChange: val => this.onVisible("sm", val),
        editable: true,
        value: this.state.visible_sm,
        vertical: false
      })), /*#__PURE__*/React.createElement(FieldInput, _extends({
        field: "visible_ms",
        key: "visible_ms",
        title: "Show at tablet screen",
        id: this.state.id
      }, matrix.section.visible_ms, {
        on: val => this.onVisible("ms", val),
        onChange: val => this.onVisible("ms", val),
        editable: true,
        value: this.state.visible_ms,
        vertical: false
      })), /*#__PURE__*/React.createElement(FieldInput, _extends({
        field: "visible_xs",
        key: "visible_xs",
        title: "Show at mobile screen",
        id: this.state.id
      }, matrix.section.visible_xs, {
        on: val => this.onVisible("ms", val),
        onChange: val => this.onVisible("xs", val),
        editable: true,
        value: this.state.visible_xs,
        vertical: false
      })))
    })), /*#__PURE__*/React.createElement(Button, {
      className: "position-absolute right mr-4 mt-4 z-index-300",
      icon: "error",
      minimal: true,
      intent: Intent.WARNING,
      title: __("Report the common error"),
      onClick: () => this.setState({
        issueOpen: !this.state.issueOpen
      })
    }), /*#__PURE__*/React.createElement(Dialog, {
      title: __("Insert new bug issue"),
      isOpen: this.state.issueOpen,
      onClose: () => this.setState({
        issueOpen: false
      })
    }, /*#__PURE__*/React.createElement(Issue, {
      src: this.state.current_type + " tab: " + this.state.navbarTabId,
      sectionID: this.state.id
    })), /*#__PURE__*/React.createElement(Popover, {
      className: "position-absolute right",
      position: Position.LEFT_TOP,
      content: /*#__PURE__*/React.createElement("div", {
        className: "p-0 square"
      }, /*#__PURE__*/React.createElement(ButtonGroup, {
        vertical: true,
        minimal: true,
        className: "p-0 "
      }, /*#__PURE__*/React.createElement(Button, {
        alignText: "left",
        large: true,
        onClick: () => this.setState({
          navbarTabId: "types"
        })
      }, __("Type")), /*#__PURE__*/React.createElement(Button, {
        alignText: "left",
        large: true,
        onClick: () => this.setState({
          navbarTabId: "template"
        })
      }, __("Template")), /*#__PURE__*/React.createElement(Button, {
        alignText: "left",
        large: true,
        onClick: () => this.setState({
          navbarTabId: "menu"
        })
      }, __("Menu")), /*#__PURE__*/React.createElement(Button, {
        alignText: "left",
        large: true,
        onClick: () => this.setState({
          navbarTabId: "visible"
        })
      }, __("Visible"))))
    }, /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      icon: /*#__PURE__*/React.createElement("i", {
        className: "fas fa-ellipsis-v"
      }),
      style: {
        height: 70
      }
    }))), /*#__PURE__*/React.createElement("div", {
      className: "layout-simple-center pt-0 px-0 pb-0 flex-row ",
      key: 2
    }, /*#__PURE__*/React.createElement(ButtonGroup, {
      fill: true,
      vertical: false,
      large: true
    }, /*#__PURE__*/React.createElement(Button, {
      intent: Intent.SUCCESS,
      onClick: this.onClick
    }, __("Update section")), /*#__PURE__*/React.createElement(Popover, {
      content: /*#__PURE__*/React.createElement("div", {
        className: "p-3"
      }, /*#__PURE__*/React.createElement(ButtonGroup, {
        vertical: true
      }, hiddenButton, /*#__PURE__*/React.createElement(Button, {
        className: "mb-1",
        icon: /*#__PURE__*/React.createElement("i", {
          className: "fas fa-file-import"
        }),
        onClick: this.onClipboardCopy
      }, __("Copy section to clipboard")), /*#__PURE__*/React.createElement(Button, {
        onClick: this.onClipboardPaste,
        icon: /*#__PURE__*/React.createElement("i", {
          className: "fas fa-file-export"
        }),
        className: "mb-1"
      }, __("Paste section from clipboard"))))
    }, /*#__PURE__*/React.createElement(Button, {
      intent: Intent.NONE,
      large: false,
      className: "ml-1",
      icon: "chevron-down"
    }, __("Added actions"))), /*#__PURE__*/React.createElement(Button, {
      icon: "trash",
      intent: Intent.DANGER,
      className: "ml-1",
      onClick: () => this.props.onRnv(this.state.id)
    }, __("Delete Section")), /*#__PURE__*/React.createElement(Button, {
      icon: "cross",
      intent: Intent.DANGER,
      onClick: () => this.props.onClose(),
      title: __("Close"),
      className: "ml-1"
    })))];
  }

  title() {
    return /*#__PURE__*/React.createElement("div", {
      className: "p-4"
    }, /*#__PURE__*/React.createElement("div", {
      className: "layout-simple-center p-2 lead"
    }, __("Title")), /*#__PURE__*/React.createElement(InputForm, _extends({}, this.state, {
      source: "title",
      id: this.state.id,
      data: this.state.title,
      sourceType: "title",
      on: (value, field) => this.onField(value, field, "title")
    })), /*#__PURE__*/React.createElement("div", {
      className: "layout-simple-center p-2 lead"
    }, __("Undertitle")), /*#__PURE__*/React.createElement(InputForm, _extends({}, this.state, {
      source: "descriptions",
      id: this.state.id,
      data: this.state.descriptions,
      sourceType: "descriptions",
      on: (value, field) => this.onField(value, field, "descriptions")
    })));
  }

  types() {
    /**/
    const btns = [];
    const tabs = Object.entries(section_groups).map((e, i) => {
      const element = e[1];
      if (element.hidden) return;
      return /*#__PURE__*/React.createElement(Button, {
        key: i,
        minimal: true,
        small: true,
        id: element.id,
        text: __(element.title),
        active: this.state.currentTypeGroup == element.id,
        onClick: this.onType,
        rightIcon: matrix[this.state.current_type].sparams.group == element.id ? "dot" : "empty"
      });
    });

    for (const c in components()) {
      if (matrix[c].sparams && matrix[c].sparams.group != this.state.currentTypeGroup) {
        continue;
      }

      const cl = c == this.state.current_type ? "active " : " ";
      const ccl = c == this.state.type ? " text-danger " : " ";
      btns.push( /*#__PURE__*/React.createElement("div", {
        key: c,
        type: c,
        className: `l-icon ${cl}`,
        onClick: this.onTypeSwitch
      }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(LayoutIcon, {
        src: components()[c].icon,
        className: "layout-icon"
      }), /*#__PURE__*/React.createElement("div", {
        className: ccl
      }, __(components()[c].title)))));
    }

    return /*#__PURE__*/React.createElement("div", {
      className: "p-4"
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex"
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-type-menu"
    }, /*#__PURE__*/React.createElement(ButtonGroup, {
      className: "  text-right",
      vertical: true
    }, tabs)), /*#__PURE__*/React.createElement("div", {
      className: "landing-type-menu-cont"
    }, btns)));
  }

  html() {
    // console.log( this.state );
    return /*#__PURE__*/React.createElement("div", {
      className: "p-4"
    }, /*#__PURE__*/React.createElement(Tabs, {
      onChange: this.onTab,
      animate: false,
      id: `tabs_${this.state.id}`,
      key: this.state.id,
      vertical: true,
      className: "tab-params "
    }, /*#__PURE__*/React.createElement(Tab, {
      id: "style",
      title: /*#__PURE__*/React.createElement("div", {
        className: "hint hint--left",
        "data-hint": __("Basic")
      }, /*#__PURE__*/React.createElement("i", {
        className: "fas fa-palette fa-2x"
      }), /*#__PURE__*/React.createElement("div", null, __("Basic"))),
      panel: /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(FieldInput, {
        field: "class",
        key: "css-class",
        title: "css-class",
        type: "Style",
        id: this.state.id,
        on: (value, field) => this.onClassName(value),
        onChange: (value, field) => this.onClassName(value),
        editable: true,
        value: this.state.class_name,
        vertical: false
      }), /*#__PURE__*/React.createElement(FieldInput, _extends({
        field: "style",
        key: "css-style",
        title: "css-style",
        type: "landing_object",
        visualization: "landing-object",
        landing_object: "Style",
        id: this.state.id
      }, matrix.Style, {
        on: this.onStyle,
        onChange: this.onStyle,
        editable: true,
        value: this.state.style,
        vertical: false
      })), /*#__PURE__*/React.createElement(FieldInput, _extends({
        field: "lasy_load_type",
        key: "lasy_load_type",
        title: "Lazy load animation type"
      }, matrix.section.lasy_load_type, {
        id: this.state.id,
        on: (value, field) => this.onField(value, field, "lasy_load_type"),
        onChange: (value, field) => this.onField(value, field, "lasy_load_type"),
        editable: true,
        value: this.state.lasy_load_type,
        vertical: false
      })), /*#__PURE__*/React.createElement(FieldInput, _extends({
        field: "lasy_load_delay",
        key: "lasy_load_delay",
        title: "Lazy load delay",
        type: "number"
      }, matrix.section.lasy_load_delay, {
        id: this.state.id,
        on: (value, field) => this.onField(value, field),
        onChange: (value, field) => {
          this.setState({
            lasy_load_delay: value === 0 ? "" : value
          });
        },
        editable: true,
        value: this.state.lasy_load_delay ? this.state.lasy_load_delay : 0,
        vertical: false
      })))
    })));
  }

  dilimiter() {
    return /*#__PURE__*/React.createElement("div", {
      className: "p-4"
    }, /*#__PURE__*/React.createElement(InputForm, _extends({}, this.state, {
      source: "Dilimiter",
      id: this.state.id,
      data: this.state.bottom_dilimiter,
      sourceType: "Dilimiter",
      on: (value, field) => this.onField(value, field, "bottom_dilimiter")
    })));
  }

  floats() {
    return /*#__PURE__*/React.createElement("div", {
      className: "p-4"
    }, /*#__PURE__*/React.createElement("div", {
      className: "layout-simple-center p-2"
    }, /*#__PURE__*/React.createElement(FlaotDettingDialog, _extends({}, this.state, {
      onUpdate: this.onUpdateFloat
    }))));
  }

  menu() {
    const btns = Object.entries(matrix.Menu).map((e, i) => {
      // console.log( e, this.state.menu, e[1].type );
      const val = e[1] ? e[1] : {};
      return e[1].hidden ? null : /*#__PURE__*/React.createElement(FieldInput, _extends({
        field: e[0],
        key: e[0],
        title: val.title,
        type: val.type,
        id: this.state.ID,
        on: (value, field) => this.onField(value, field, "menu")
      }, matrix.Menu[e[0]], {
        onChange: (value, field) => this.onField(value, field, "menu"),
        editable: true,
        value: this.state.menu ? this.state.menu[e[0]] : e[0],
        values: val.values,
        vertical: false
      }));
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "p-4"
    }, btns, /*#__PURE__*/React.createElement(FieldInput, {
      field: "id",
      title: "id",
      type: "string",
      on: (value, field) => this.onId(value),
      onChange: (value, field) => this.onId(value),
      editable: false,
      value: this.state.id,
      vertical: false
    }));
  }

  composition() {
    return /*#__PURE__*/React.createElement("div", {
      className: "p-4"
    }, Object.entries(matrix.Composition).map((compositionField, i) => {
      const is_demand = compositionField[1].demand ? (Array.isArray(compositionField[1].demand.value) ? compositionField[1].demand.value : [compositionField[1].demand.value]).filter(ee =>
      /*
      console.log(
        Array.isArray( compositionField[1].demand.value )
          ?
          compositionField[1].demand.value
          :
          [ compositionField[1].demand.value ],
        ee,
        this.state.composition[ compositionField[1].demand.field ],
        ee == this.state.composition[ compositionField[1].demand.field ]
      );
      */
      ee == this.state.composition[compositionField[1].demand.field]).length == 0 : false;
      return compositionField[1].hidden || is_demand ? null : /*#__PURE__*/React.createElement(Fragment, {
        key: compositionField[0]
      }, /*#__PURE__*/React.createElement(FieldInput, _extends({
        field: compositionField[0],
        key: compositionField[0],
        title: compositionField[1].title,
        type: compositionField[1].type,
        id: this.state.id,
        on: (value, field) => this.onColumnCount(value, field, "composition")
      }, matrix.Composition[compositionField[0]], {
        onChange: (value, field) => this.onColumnCount(value, field, "composition"),
        editable: true,
        value: this.state.composition[compositionField[0]],
        values: compositionField[1].values,
        vertical: false
      })));
    }));
  }

  template() {
    const checkeds = Array.isArray(DataContext.data.landing.palette) && DataContext.data.landing.palette.length > 0 ? DataContext.data.landing.palette.map((e, i) => /*#__PURE__*/React.createElement("div", {
      key: i
    }, /*#__PURE__*/React.createElement(PaletteSingleForm, {
      e: { ...e,
        checked: e.id == this.state.current_template_id
      },
      i: i,
      key: i,
      isEdit: true,
      onCheck: evt => this.onTemplateCheck(evt, e)
    }))) : /*#__PURE__*/React.createElement(PaletteSingleForm, {
      e: { ...PalettePresets()[0],
        checked: true
      },
      i: 0,
      isEdit: true,
      onCheck: evt => this.onTemplateCheck(evt, PalettePresets()[0])
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "p-2"
    }, checkeds);
  }

  prm() {}

  onVisible(size, val) {
    const state = { ...this.state
    };
    state[`visible_${size}`] = val; // console.log(state);

    this.setState(state);
  }

}

export default TypeDialog;