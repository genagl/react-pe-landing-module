function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, ButtonGroup, Intent, Popover, Position, Drawer, Dialog } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import { LayoutIcon } from 'react-pe-useful';
import InputForm from "./InputForm";
import section_groups from "../data/section_groups.json";
import matrix from "../data/matrix";
import { components } from "../data/components";

class NewSectionDialog extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onType", evt => {
      const id = evt.currentTarget.getAttribute("id"); //console.log(id)

      this.setState({
        currentTypeGroup: id
      });
    });

    _defineProperty(this, "onTypeSwitch", evt => {
      const current_type = evt.currentTarget.getAttribute("type");
      let data = {}; //console.log(current_type)

      if (matrix[current_type].default) {
        data = { ...matrix[current_type].default,
          sectionType: current_type
        };
        delete data.hidden;
      } // console.log( data, current_type );


      this.setState({
        data,
        current_type,
        is_change_type_enbl: current_type != this.state.type
      });
    });

    _defineProperty(this, "onEdit", () => {
      //console.log(this.state.data)
      this.props.onEdit(this.state.data, this.state.current_type);
    });

    this.state = { ...props,
      current_type: "html",
      currentTypeGroup: matrix.html.sparams.group
    };
  }

  componentWillUpdate(nextProps) {
    const state = {};
    Object.keys(nextProps).forEach(e => {
      if (this.state[e] != nextProps[e]) state[e] = nextProps[e];
    });

    if (Object.keys(state).length > 0) {
      this.setState(state);
    }
  }

  tabs() {
    const btns = [];
    const tabs = Object.entries(section_groups).map((e, i) => {
      const element = e[1];
      if (element.hidden) return;
      return /*#__PURE__*/React.createElement(Button, {
        key: i,
        minimal: true,
        small: true,
        id: element.id,
        text: __(element.title),
        active: this.state.currentTypeGroup == element.id,
        onClick: this.onType,
        rightIcon: matrix[this.state.current_type].sparams.group == element.id ? "dot" : "empty"
      });
    });

    for (const c in components()) {
      if (matrix[c].sparams && matrix[c].sparams.group != this.state.currentTypeGroup) {
        continue;
      }

      const cl = c == this.state.current_type ? "active " : " ";
      const ccl = c == this.state.type ? " text-danger " : " ";
      btns.push( /*#__PURE__*/React.createElement("div", {
        key: c,
        type: c,
        className: `l-icon ${cl}`,
        onClick: this.onTypeSwitch
      }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(LayoutIcon, {
        src: components()[c].icon,
        className: "layout-icon"
      }), /*#__PURE__*/React.createElement("div", {
        className: ccl
      }, __(components()[c].title)))));
    }

    return /*#__PURE__*/React.createElement("div", {
      className: "p-4"
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex"
    }, /*#__PURE__*/React.createElement("div", {
      className: "landing-type-menu"
    }, /*#__PURE__*/React.createElement(ButtonGroup, {
      className: "  text-right",
      vertical: true
    }, tabs)), /*#__PURE__*/React.createElement("div", {
      className: "landing-type-menu-cont"
    }, btns)));
  }

  render() {
    //console.log( this.state.data );
    return /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isOpen,
      onClose: this.props.onClose,
      title: __("Inserting new. Choose type"),
      className: "landing-outer-container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "pt-0 px-0 overflow-y-auto flex-grow-100 bg-tripp"
    }, /*#__PURE__*/React.createElement("div", {
      className: "m-4"
    }, this.tabs())), /*#__PURE__*/React.createElement(ButtonGroup, {
      className: "mx-auto py-3"
    }, /*#__PURE__*/React.createElement(Button, {
      onClick: this.onEdit
    }, __("Insert"))));
  }

  onField(value, field) {
    const {
      data
    } = this.state; // { ...this.state.data}

    data[field] = value; //console.log(data)
    // this.setState( { data } );
  }

}

export default NewSectionDialog;