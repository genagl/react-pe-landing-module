import { Dialog, Tag, Button } from "@blueprintjs/core";
import React, { useEffect, useState } from "react";
import { __ } from "react-pe-utilities";
import DataContext from "../../DataContext";

const CardsTagsFilter = props => {
  const [isOpen, onOpen] = useState(false);
  const [allTags, setAllTags] = useState(Array.isArray(props.standartFilters) ? props.standartFilters : []);
  useEffect(() => {
    // console.log(props.standartFilters)
    setAllTags(props.standartFilters ? props.standartFilters : []);
  });
  const [filterTag, onFilterTag] = useState(allTags.map(e => false));

  const onChooseFilterTag = i => {
    let ft = [...filterTag];
    ft[i] = !ft[i];
    onFilterTag(ft);
    if (props.onFilterParams) props.onFilterParams({
      tags: allTags.filter((e, i) => ft[i])
    });
  };

  const tagFilterList = allTags.map((tag, i) => {
    const filter = filterTag[i];
    return /*#__PURE__*/React.createElement(Tag, {
      key: i,
      style: {
        backgroundColor: filter ? tag.color : "#CCC",
        textTransform: "uppercase",
        padding: "10px 40px",
        cursor: "pointer"
      },
      onClick: evt => onChooseFilterTag(i)
    }, tag.title);
  });
  return /*#__PURE__*/React.createElement("div", {
    className: " position-relative w-100 "
  }, /*#__PURE__*/React.createElement(Button, {
    fill: true,
    large: true,
    minimal: true,
    onClick: evt => onOpen(true)
  }, /*#__PURE__*/React.createElement("b", null, __("Filters")), ":", /*#__PURE__*/React.createElement("span", {
    className: "text-secondary"
  }, allTags.filter((e, i) => filterTag[i]).map(e => e.title).join(", ")), /*#__PURE__*/React.createElement("span", {
    className: "px-2 text-danger"
  }, "( ", props.col, " )")), /*#__PURE__*/React.createElement(Dialog, {
    isOpen: isOpen,
    onClose: () => onOpen(false),
    title: __("Filters")
  }, /*#__PURE__*/React.createElement("div", {
    className: "p-4"
  }, /*#__PURE__*/React.createElement("div", {
    className: "pb-4"
  }, /*#__PURE__*/React.createElement("div", {
    className: "title mb-2"
  }, __("Select tags")), tagFilterList), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onOpen(false)
  }, __("Close"))))));
};

export default CardsTagsFilter;