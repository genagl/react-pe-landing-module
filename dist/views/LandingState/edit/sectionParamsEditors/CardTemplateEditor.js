function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import { Button, ButtonGroup, Dialog, Intent } from "@blueprintjs/core";
import React from "react";
import { useState } from "react";
import { AppToaster } from 'react-pe-useful';
import { __ } from "react-pe-utilities";
import DataContext from "../../DataContext";
import InputForm from "../InputForm";
import $ from "jquery";

const CardTemplateEditor = props => {
  const [landing, setLanding] = useState({ ...DataContext.data.landing
  });
  const [title, onTitle] = useState(props.title && props.title.text ? props.title.text : null);
  const [preTitle, onPreTitle] = useState(title);
  const [isOpen, onOpen] = useState(false);

  const setTitle = () => {
    onTitle(preTitle);
    onOpen(null);
    let cardData = { ...props.data
    }; //delete cardData.cards

    cardData.title = preTitle;
    if (!Array.isArray(DataContext.data.landing.card_templates)) DataContext.data.landing.card_templates = [];
    DataContext.data.landing.card_templates.unshift(cardData);
    setLanding({ ...DataContext.data.landing
    });
    props.onEdit();
  };

  const onCopy = () => {
    onOpen(true);
  };

  const onClearAll = () => {
    DataContext.data.landing.card_templates = [];
    setLanding({ ...DataContext.data.landing
    });
    props.onEdit();
  };

  const on = (val, field, dopol) => {
    console.log(val, field, dopol);
  };

  const onApply = val => {
    props.onApply(val);
  };

  const onTry = val => {
    props.onTry(val);
  };

  const onSingleRemove = val => {
    DataContext.data.landing.card_templates = DataContext.data.landing.card_templates.filter((e, i) => {
      return val !== i;
    });
    setLanding({ ...DataContext.data.landing
    });
    props.onEdit();
  };

  const onClipboardCopy = evt => {
    const data = DataContext.data.landing.card_templates;
    $("body").append(`<div style='position:absolute; z-index:-100; width:100%; top:0; left:0;'><textarea style='width:100%;' id='myInput'>${JSON.stringify(data)}</textarea></div>`);
    const copyText = document.getElementById("myInput");
    copyText.select();
    copyText.setSelectionRange(0, 99999999999999999999);
    document.execCommand("copy");
    $("#myInput").remove();
    AppToaster.show({
      intent: Intent.SUCCESS,
      icon: "tick",
      duration: 10000,
      message: __("Copy to clipbord")
    });
  };

  const onClipboardPaste = evt => {
    navigator.clipboard.readText().then(clipText => {
      try {
        const temp = DataContext.data.landing.card_templates || [];
        DataContext.data.landing.card_templates = JSON.parse(clipText).concat(temp);
        setLanding({ ...DataContext.data.landing
        });
        props.onEdit();
      } catch (e) {
        AppToaster.show({
          intent: Intent.DANGER,
          icon: "tick",
          message: __("Error read clipboard data")
        });
      }
    });
  }; // console.log( props )


  return /*#__PURE__*/React.createElement("div", {
    className: "p-3"
  }, /*#__PURE__*/React.createElement(ButtonGroup, {
    className: "d-flex flex-wrap"
  }, /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    minimal: true,
    large: true,
    className: "m-0 w-50",
    onClick: onCopy
  }, __("Copy current card template to local library")), /*#__PURE__*/React.createElement(Button, {
    large: true,
    minimal: true,
    "data-hint": __("Copy library to clipboard"),
    onClick: onClipboardCopy,
    className: "hint hint--bottom"
  }, /*#__PURE__*/React.createElement("i", {
    className: "fas fa-file-import"
  })), /*#__PURE__*/React.createElement(Button, {
    large: true,
    minimal: true,
    "data-hint": __("Paste library from clipboard"),
    onClick: onClipboardPaste,
    className: "hint hint--bottom "
  }, /*#__PURE__*/React.createElement("i", {
    className: "fas fa-file-export"
  })), /*#__PURE__*/React.createElement(Button, {
    "data-hint": __("Clear local library"),
    minimal: true,
    large: true,
    className: "hint hint--bottom",
    onClick: onClearAll,
    icon: "cross"
  })), /*#__PURE__*/React.createElement(InputForm, _extends({
    data: landing,
    source: "landing",
    include_tab: ["Card templates"],
    id: props.section_id,
    vertical: true,
    palette: props.palette
  }, props.data, {
    on: on,
    onApply: onApply,
    onTry: onTry,
    onSingleRemove: onSingleRemove,
    onUpdate: props.onUpdate
  })), /*#__PURE__*/React.createElement(Dialog, {
    isOpen: isOpen,
    onClose: () => onOpen(true),
    title: __("Set new template title")
  }, /*#__PURE__*/React.createElement("div", {
    className: "p-5"
  }, /*#__PURE__*/React.createElement("input", {
    type: "text",
    value: preTitle,
    onChange: evt => onPreTitle(evt.currentTarget.value),
    className: "form-control input dark my-4"
  }), /*#__PURE__*/React.createElement(ButtonGroup, null, /*#__PURE__*/React.createElement(Button, {
    intent: Intent.DANGER,
    onClick: setTitle
  }, __("Set title")), /*#__PURE__*/React.createElement(Button, {
    intent: Intent.NONE,
    onClick: () => onOpen(false)
  }, __("Close"))))));
};

export default CardTemplateEditor;