function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, ButtonGroup, Intent, Icon, Dialog } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import FloatSetting from "./FloatSetting";

class FlaotDettingDialog extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", { ...this.props,
      current_type: 0,
      isDialog: false
    });

    _defineProperty(this, "onSwitch", evt => {
      // this.setState({ current_type: evt.currentTarget.getAttribute("i") });
      this.setState({
        isDialog: true,
        float_id: evt.currentTarget.getAttribute("float_id")
      });
    });

    _defineProperty(this, "onComponentSelect", evt => {});

    _defineProperty(this, "onDialog", () => {
      this.setState({
        isDialog: !this.state.isDialog
      });
    });

    _defineProperty(this, "onUpdate", (data, float_id) => {
      console.log(data);
      this.props.onUpdate(data, float_id);
      this.onDialog();
    });
  }

  render() {
    // console.log( this.state );
    const btns = this.state.floats.map((e, i) => {
      if (!e) return null; //const cl = i == this.state.current_type ? " btn-danger " : " btn-light "

      return /*#__PURE__*/React.createElement(Button, {
        key: i,
        i: i,
        float_id: e.float_id,
        small: true,
        onClick: this.onSwitch
      }, /*#__PURE__*/React.createElement("div", {
        className: ""
      }, e.type));
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "p-0"
    }, /*#__PURE__*/React.createElement(ButtonGroup, null, btns, /*#__PURE__*/React.createElement(Button, {
      intent: Intent.SUCCESS
    }, /*#__PURE__*/React.createElement(Icon, {
      icon: "plus"
    }))), /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isDialog,
      onClose: this.onDialog,
      title: __("Float Settings")
    }, /*#__PURE__*/React.createElement(FloatSetting, {
      float_id: this.state.float_id,
      onChange: this.onUpdate
    })));
  }

}

export default FlaotDettingDialog;