function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import matrix from "../data/matrix";
import FieldInput from "react-pe-scalars";

class InputPosition extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", { ...this.props
    });

    _defineProperty(this, "on", (field, value) => {});
  }

  render() {
    const {
      x
    } = this.state.position.mc;
    return /*#__PURE__*/React.createElement("div", {
      className: "landing-position"
    }, /*#__PURE__*/React.createElement(FieldInput, _extends({
      field: "x",
      key: "x"
    }, matrix.Position, {
      visualization: "landing_object",
      type: "landing_object",
      landing_object: "Position",
      on: this.on,
      onChange: this.on,
      editable: true,
      value: x,
      vertical: false
    })));
  }

}

export default InputPosition;