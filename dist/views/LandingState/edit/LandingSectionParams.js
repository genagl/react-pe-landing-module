function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { ButtonGroup, Button, Drawer, Position } from "@blueprintjs/core";
import React, { Component } from "react";
import { __ } from "react-pe-utilities";
import matrix from "../data/matrix.json";
import CardTemplateEditor from "./sectionParamsEditors/CardTemplateEditor";

class LandingSectionParams extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onClick", (evt, data) => {
      this.setState({
        isDialogOpen: true,
        DialogTitle: __(data.title) + "  " + __(data.hint),
        matrixSrc: data
      });
    });

    _defineProperty(this, "onDialogClose", () => {
      this.setState({
        isDialogOpen: false
      });
    });

    this.state = { ...this.props,
      matrixSrc: {}
    };
  }

  render() {
    // console.log(matrix[this.state.type].sparams.sparam_id);
    const btns = /*#__PURE__*/React.createElement(ButtonGroup, null, Array.isArray(matrix[this.state.type].sparams.sparam_id) ? matrix[this.state.type].sparams.sparam_id.map((e, i) => /*#__PURE__*/React.createElement(Button, {
      key: i,
      onClick: evt => this.onClick(evt, e),
      className: "py-1 hint hint--left",
      "data-hint": __(e.hint)
    }, /*#__PURE__*/React.createElement("span", {
      dangerouslySetInnerHTML: {
        __html: __(e.title)
      }
    }))) : null);
    let content;

    switch (this.state.matrixSrc.editor) {
      case "CardTemplateEditor":
        content = /*#__PURE__*/React.createElement(CardTemplateEditor, _extends({}, this.state, {
          onEdit: this.props.onEdit,
          onApply: this.props.onApply,
          onTry: this.props.onTry,
          onUpdate: this.props.onUpdate
        }));
        break;
    }

    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "landing-sector__params mr-5",
      style: {
        top: this.state.dopEditTop
      }
    }, btns), /*#__PURE__*/React.createElement(Drawer, {
      isOpen: this.state.isDialogOpen,
      onClose: this.onDialogClose,
      title: /*#__PURE__*/React.createElement("span", {
        dangerouslySetInnerHTML: {
          __html: this.state.DialogTitle
        }
      }),
      position: Position.RIGHT,
      usePortal: true,
      backdropClassName: "landing-drawer-bg",
      size: 700
    }, /*#__PURE__*/React.createElement("div", {
      className: "pt-0 px-0 overflow-y-auto flex-grow-100 bg-tripp"
    }, content)));
  }

}

export default LandingSectionParams;