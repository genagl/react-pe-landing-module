function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* ==================================
 * 	Форма регулировки значений элемента, имеющего зматрицу в matrix.json
 ====================================*/
import React, { Component } from "react";
import { Intent, Tabs, Tab, Callout } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import matrix from "../data/matrix";
import FieldInput from "react-pe-scalars";

class InputForm extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "on", (value, field, dopol) => {
      let val = value;

      if (typeof val == "string") {
        val = val.replace(/"([^"]+)"/g, '«$1»');
      } // console.log(val, field, dopol);


      if (this.props.on) this.props.on(val, field, dopol);
      const state = { ...this.state
      };
      state[field] = val;
      this.setState(state);
    });

    _defineProperty(this, "tab", (tabil, i, e) => /*#__PURE__*/React.createElement("div", {
      key: i
    }, /*#__PURE__*/React.createElement("div", {
      className: "title mb-1 text-center text-secondary"
    }, __(e)), /*#__PURE__*/React.createElement("div", null, tabil)));

    this.state = { ...props,
      data: props.data ? props.data : {}
    }; //console.log( this.state )
  }

  componentDidUpdate(nextProps) {
    if (nextProps.source !== this.state.source && typeof nextProps.source !== "undefined") {
      this.setState({
        source: nextProps.source
      });
    }

    if (nextProps.id !== this.state.id && typeof nextProps.id !== "undefined") {
      this.setState({
        id: nextProps.id
      });
    }

    if (nextProps.data !== this.state.data && typeof nextProps.data !== "undefined") {
      this.setState({
        data: nextProps.data
      });
    }

    if (nextProps.palette !== this.state.palette && typeof nextProps.palette !== "undefined") {
      this.setState({
        palette: nextProps.palette
      });
    }
  }

  render() {
    const ttabs = {};
    const tabTitles = {}; // определяем матрицу полей текущего типа данных

    const cellData = this.state.cellData ? this.state.cellData : matrix[this.state.source]; //определяем список разрешённых к публикации tab-групп

    const include_tab = Array.isArray(this.state.include_tab) ? this.state.include_tab : this.state.include_tab ? [this.state.include_tab] : []; // console.log( this.state )
    // перебираем все зарегистрированные поля и отрисовываем скаляры (регуляторы)

    for (const m in cellData) {
      const mtrxCell = cellData[m];
      let ttb = mtrxCell.tab;
      let ttbIcon = mtrxCell.tab_icon;
      let ttbCommentary;

      if (include_tab.length > 0 && include_tab.filter(e => e === ttb).length === 0 || ttb === "Lasy") {
        continue;
      }

      const isincl = include_tab.length > 0 && include_tab.filter(e => e == ttb).length > 0;

      if (!ttb || isincl) {
        ttb = "BasicSettings";
        ttbIcon = "fas fa-cog";
      }

      if (!ttabs[ttb]) {
        ttabs[ttb] = [];
      } //


      if (!ttbCommentary && mtrxCell.tab_commentary) {
        ttbCommentary = mtrxCell.tab_commentary;
        ttabs[ttb].push( /*#__PURE__*/React.createElement(Callout, {
          className: "p-5 my-3",
          i: "callout",
          key: `callout${ttbCommentary}`,
          intent: Intent.WARNING
        }, /*#__PURE__*/React.createElement("div", {
          dangerouslySetInnerHTML: {
            __html: __(ttbCommentary)
          }
        })));
      }

      if (!tabTitles[ttb]) {
        tabTitles[ttb] = ttbIcon;
      } // скрытое поле - пропускаем


      if (mtrxCell.hidden && !isincl) continue; // определяем поле - визуальный заголовок

      const visibled_value = mtrxCell.landing_object && matrix[mtrxCell.landing_object].visible_value ? matrix[mtrxCell.landing_object].visible_value.value : "title"; // есть ли у этого поля разрешаемое (demand)?
      // Если есть, то его значение соответствует 
      // хотя бы одному из разрешаемых?

      const notDemand = mtrxCell.demand && mtrxCell.demand.value.filter(e => {
        /*
         *	this.state.data - значения полей данного инстанса
         *	mtrxCell.demand - название разрешаемого поля
         */
        return this.state.data[mtrxCell.demand.field] == e;
      }).length == 0; // У поля емть родитель

      const hasParent = mtrxCell.parent; // разрешаемое поле имеет одно из разрешаемых значений. Или у этого поля нет разрешаемого...  

      if (!notDemand && !hasParent && m !== "lasy_load_type" && m !== "lasy_load_delay") {
        //search children
        const childCells = Object.keys(cellData).filter(cell => cellData[cell].parent && cellData[cell].parent == m).map((cell, icell) => {
          return { ...cellData[cell],
            field: cell,
            i: icell,
            value: this.state.data ? this.state.data[cell] : "",
            origin: this.state
          };
        }); // console.log(mtrxCell)

        ttabs[ttb].push( /*#__PURE__*/React.createElement(FieldInput, _extends({
          field: m,
          key: m,
          id: this.state.ID,
          section_id: this.state.id,
          on: (value, dopol) => this.on(value, m, dopol),
          onChange: (value, dopol) => this.on(value, m, dopol),
          visibled_value: visibled_value,
          vertical: this.state.vertical
        }, mtrxCell, {
          origin: this.state,
          sourceData: this.state.data ? this.state.data[m] : "",
          editable: true,
          value: this.state.data ? this.state.data[m] : "",
          palette: this.state.palette,
          sub_menus: childCells,
          onSubMenu: (value, dopol) => this.on(value, dopol),
          onAdd: this.props.onAdd,
          onDownload: this.props.onDownload,
          onLoadChange: this.props.onLoadChange,
          onClearOpen: this.props.onClearOpen,
          palette: this.props.palette,
          onApply: this.props.onApply // CardTemplateEditor
          ,
          onTry: this.props.onTry // CardTemplateEditor
          ,
          onSingleRemove: this.props.onSingleRemove // CardTemplateEditor
          ,
          onUpdate: this.props.onUpdate,
          onClipboardPaste: data => {
            console.log("InputForm.onClipboardPaste");
            if (this.props.onClipboardPaste) this.props.onClipboardPaste(data);
          },
          onClipboardCopy: data => {
            console.log("InputForm.onClipboardCopy", this.props.onClipboardCopy);
            if (this.props.onClipboardCopy) this.props.onClipboardCopy(data);
          }
        })));
      }
    } // распределяем все скаляры по группам ( поле tab в матрице )
    // если в матрице нет ни одного поля с tab - игнорируем распределение.


    const tabArray = Object.keys(ttabs);
    return tabArray.length > 1 ? /*#__PURE__*/React.createElement("div", {
      className: "px-4"
    }, /*#__PURE__*/React.createElement(Tabs, {
      onChange: this.onTab,
      animate: false,
      id: `tabs_${this.state.id}`,
      key: this.state.id,
      vertical: true,
      className: "tab-params "
    }, tabArray.map((e, i) =>
    /*#__PURE__*/
    // console.log( ttabs[ e ] );
    React.createElement(Tab, {
      key: i,
      id: e,
      title: tabTitles[e] ? /*#__PURE__*/React.createElement("div", {
        className: "hint hint--left",
        "data-hint": __(e)
      }, /*#__PURE__*/React.createElement("i", {
        className: `${tabTitles[e]} fa-2x `
      }), /*#__PURE__*/React.createElement("div", null, __(e))) : __(e),
      panel: this.tab(ttabs[e], i, e)
    })))) : /*#__PURE__*/React.createElement("div", {
      className: "px-4"
    }, ttabs.BasicSettings);
  }
  /*
   * 	Обновление всех скаляров и передача обновленных данных наверх
   */


}

export default InputForm;